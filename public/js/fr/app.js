
var app = angular.module('app', ['ui.validate', 'ui.calendar', 'ui.bootstrap'], function($httpProvider) {

  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */ 
   var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj) {
      value = obj[name];

      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
}).config(
[          '$interpolateProvider',
function ($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
}]);

app.controller("indexMainCtrl", function($timeout, $scope, $http, API_URL){

$scope.search = function (keywords) {

    window.location.href="/search/index/"+keywords.name+"/1";

  }

});
// console.log("THIS is a test");
//Anchor
app.controller('anchorMainCtrl', function($scope, $location, $anchorScroll) {
  $scope.scrollTo = function(id) {
    $location.hash(id);
    console.log($location.hash());
    $anchorScroll();
  };
});

// Calendar
app.controller("CalenCtrl", function($scope, $http, $compile, uiCalendarConfig, API_URL){

  $scope.lisview1 = false;
  $scope.calview1 = true;
  $scope.lisview = function(){
    $scope.lisview1 = true;
    $scope.calview1 = false;   
  }
  $scope.calview = function(){
    $scope.calview1 = true;
    $scope.lisview1 = false;
  }

  // begin code here
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  /* event source that pulls from google.com */
  $scope.eventSource = {
    url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
    className: 'gcal-event',           // an option!
    currentTimezone: 'America/Chicago' // an option!
  };  
  
  $scope.events = [];
  
  $http.get(API_URL + '/calendar/manageactivity').success(function(listview3){
    var data = JSON.parse(JSON.stringify(listview3));
    if(listview3){
      var arrevents = [];
      for(var d in data){
        if(data[d].datef == data[d].datet){
          $scope.events.push({
            title: data[d].title,
            start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
            location: data[d].loc,
            info: data[d].info,
            dstart: data[d].fyea2+'-'+data[d].fmon2+'-'+data[d].fday2,
            dend: 'One Day Activity'
          });
        }else{
          $scope.events.push({
            title: data[d].title,
            start: new Date(data[d].fyea, data[d].fmon-1, data[d].fday),
            end: new Date(data[d].tyea, data[d].tmon-1, data[d].tday, data[d].hhr, data[d].hmin),
            allDay: false,
            location: data[d].loc,
            info: data[d].info,
            dstart: data[d].fyea2+'-'+data[d].fmon2+'-'+data[d].fday2,
            dend: '<i class="icon-time text-muted m-r-xs"></i>&nbsp&nbsp&nbsp'
            +data[d].tyea2+'-'+data[d].tmon2+'-'+data[d].tday2+', '+data[d].hhr+':'+data[d].hmin+' '+data[d].hap
          });
        }
      }
    }
    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: false,
        header:{
          left: 'title',
          center: 'Activity',
          right: 'today, prev, next'
        },
        eventClick: $scope.alertOnEventClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: function(event, element) {
          element.qtip({
              content: '<div class="h4 font-thin m-b-sm">'
              +event.title+'</div><div class="line b-b b-light"></div><div><i class="icon-calendar text-muted m-r-xs"></i>&nbsp&nbsp&nbsp'
              +event.dstart+'</div><div>'
              +event.dend+'</div><div><i class="icon-location-arrow text-muted m-r-xs"></i>&nbsp&nbsp&nbsp'
              +event.location+'</div><div class="m-t-sm"> '
              +event.info+'</div>'
          });
        }
      }
    };

  });

/* event sources array*/
$scope.eventSources = [$scope.events];
  // end code here

});

app.controller("TestimonialCtrl", function($scope, $http,API_URL){
  ////////////////////////////////////////////////////////////
  var oriUser = angular.copy($scope.user);
  $scope.submit = function(user){
    console.log(user);
    $http({
      url: API_URL + "/utility/submit",
      method: "POST",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: $.param(user)
    }).success(function(data, status, headers, config) {
      $scope.isSaving = false;
      $scope.user = angular.copy(oriUser);
      $scope.form.$setPristine();

      // $scope.alerts.push({
      //   type: 'success',
      //   msg: 'News successfully saved!'
      // });

  })
  }
  ////////////////////////////////////////////////////////////////
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 5;
  $scope.show = function(){
    $scope.showloading = true;
    $http({
      url: API_URL + '/utility/testimonial/' + offset,
      method: "GET"

    }).success(function(response) {
     var obj = JSON.parse(JSON.stringify(response));
     console.log(obj);

     if(response){

      for (var data in obj){

        var disp = $('#display').append("<div  class='media box effect3' style='background-color:#eee;padding:20px;color:#fff;border-radius:10px;'><a href=''  style='color:5386c1;font-size:1.5em;'>"+ obj[data].id+"-"+ obj[data].name+"</a><div class='media-body'><div class='pull-right text-center text-muted'></div><div class='block' style='color:#333'><em class='text-xs'>Company/Organization: <span class='text-danger'>ob"+ obj[data].comporg+"</span></em><i class='fa fa-envelope fa-fw m-r-xs'></i>:"+ obj[data].email+"</div><blockquote ><small class='block m-t-sm' style='color:#333'>"+ obj[data].message+"</small></blockquote></div></div> </div>");
        
      }
    }


    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if(obj.length < 5)
    {
     console.log('end');
     $scope.notendresult = false;
     $scope.endresult = true;
   }
   else
   {
    console.log('nah');
    $scope.notendresult = true;
    $scope.endresult = false;
  }

})
  //   .success(function(response) {

  //   //   if(response){
  //   //     for (var data in obj){
  //   //       var bodyreplace = obj[data].feat_content.replace('/<img[^>]+\>/i', '');                    
  //   //       var body = bodyreplace.substring(0, 180);                    
  //   //       $('#projiklist').append("<table><tr height='140'><td><div class='fillmyimage'><img src=" 
  //   //         + API_URL + "/images/featurebanner/" 
  //   //         + obj[data].feat_picpath + "></div><b><u style=font-size:18px>" 
  //   //         + obj[data].feat_title + "</u></b><p style=color:#5386c1>Date Posted: " 
  //   //         + obj[data].reupdate + "</p>" 
  //   //         + body + "<a href=http://ecosite/projects/viewer/" 
  //   //         + obj[data].feat_id + " style=font-size:12px>read more...</a></td></tr></table>");
  //   //     }
  // }
}

});



app.controller("featuredproject", function($scope, $http, API_URL){

  $scope.data = {};
  var num = 10;
  var off = 1;
  var keyword = null;

  var paginate = function (off, keyword) {
    $http({
      url: API_URL+"/featuredprojects/indexmanagefeature/" + num + '/' + off + '/' + keyword,
      method: "GET",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      console.log(data);
    }).error(function (data, status, headers, config) {
      $scope.status = status;
    });
  }

  $scope.search = function (keyword) {
    var off = 0;
    paginate(off, keyword);
  }

  $scope.paging = function (off, keyword) {
    paginate(off, keyword);
  }

  paginate(off, keyword);
});


app.controller("albumctrl", function ($scope, $http) {
  console.log('asdasd')
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 6;
  $scope.getalbum = function (data) {


    var pageid = data;
    $scope.showloading = true;
    $http.get(API_URL + '/album/album/' + offset).success(function (response) {


     var obj = JSON.parse(JSON.stringify(response));

     if (response) {
      for (var data in obj) {
        var bodyreplace = obj[data].description.replace('/<img[^>]+\>/i', '');
        var body = bodyreplace.substring(0, 250);
        var styles = "style='background-image: url(&quot;" +API_URL + "/images/"+ obj[data].cats.foldername +"/"+obj[data].cats.path+"&quot;)'"

        $('#albumlist').append('<div class="list-image bar2" '+ styles +' ><a href="/gallery/folder/'+obj[data].cats.folderid +'" class="mosaic-overlay"><div class="simple-caption details"><br><br><strong>'+obj[data].album_name+'</strong><p> <i class="icon-calendar"></i>'+obj[data].date +'</p><br><br><br><strong><p>' + body + '</p></strong></div> </a></div>');


          $('.bar2').mosaic({
            animation : 'slide' 
          });

      }
    }
    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if (obj.length < 5)
    {
      console.log('end');
      $scope.notendresult = false;
      $scope.endresult = true;
    }
    else
    {
      console.log('nah');
      $scope.notendresult = true;
      $scope.endresult = false;
    }
  });
}
});


app.controller("newsletterload", function ($scope, $http,BASE_URL) {
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 5;
  $scope.getlatest = function (data) {

    $scope.showloading = true;
    $http.get(API_URL + '/newsletter/newsletter/' + offset).success(function (response) {


     var obj = JSON.parse(JSON.stringify(response));

     if (response) {
      for (var data in obj) {
        var newsletterid = obj[data].newsletterid;
        var title = obj[data].title;
        var body = obj[data].body;
        var banner = obj[data].banner;
        var year = obj[data].date.substr(0, 4);
        var day =  obj[data].date.substr(-2);
        
        if(obj[data].date.substr(5, 2) == 01)
        {
          var month = 'Jan';
        }
        else if(obj[data].date.substr(5, 2) == 02)
        {
          var month = 'Feb';
        }
        else if(obj[data].date.substr(5, 2) == 03)
        {
          var month = 'Mar';
        }
        else if(obj[data].date.substr(5, 2) == 04)
        {
          var month = 'Apr';
        }
        else if(obj[data].date.substr(5, 2) == 05)
        {
          var month = 'May';
        }
        else if(obj[data].date.substr(5, 2) == 06)
        {
          var month = 'Jun';
        }
        else if(obj[data].date.substr(5, 2) == 07)
        {
          var month = 'Jul';
        }
        else if(obj[data].date.substr(5, 2) == 08)
        {
          var month = 'Aug';
        }
        else if(obj[data].date.substr(5, 2) == 09)
        {
          var month = 'Sep';
        }
        else if(obj[data].date.substr(5, 2) == 10)
        {
          var month = 'Oct';
        }
        else if(obj[data].date.substr(5, 2) == 11)
        {
          var month = 'Nov';
        }
        else if(obj[data].date.substr(5, 2) == 12)
        {
          var month = 'Dec';
        }

  
        $('#latestlist').append("<table style='margin-top:20px; margin-bottom:30px;width:100%;'><tr><td>" +
          "<span><div class='fillmyimage'><img src='"+ API_URL +"/images/featurebanner/"+banner+"' style='width:150px;height:150px;'></div></span>"+
          "<a href='/newsletter/view/" + newsletterid + "'><h4>" + title + "</h4></a>"+
          "<p style='color: #999;'><b><i>"+ month + " " + day + ", " + year +
          "</i></b></p>"+
          "<br>"+
          "</td></tr></table>");
      }
    }
    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if (obj.length < 5)
    {
      console.log('end');
      $scope.notendresult = false;
      $scope.endresult = true;
    }
    else
    {
      console.log('nah');
      $scope.notendresult = true;
      $scope.endresult = false;
    }
  });
}
});





app.controller("postlatestctrl", function ($scope, $http,BASE_URL) {
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 5;
  $scope.getlatest = function (data) {

    var disqus_shortname = 'ilchilee';

  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = '//' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
  }());


    var pageid = data;
    $scope.showloading = true;
    $http.get(API_URL + '/post/post/' + offset).success(function (response) {


     var obj = JSON.parse(JSON.stringify(response));

     if (response) {
      for (var data in obj) {
        var newsslugs = obj[data].newsslugs;
        var newsid = obj[data].newsid;
        var category = obj[data].category;
        var title = obj[data].title;
        var body = obj[data].body;
        var year = obj[data].date.substr(0, 4);
        var day =  obj[data].date.substr(-2);
        
        if(obj[data].date.substr(5, 2) == 01)
        {
          var month = 'Jan';
        }
        else if(obj[data].date.substr(5, 2) == 02)
        {
          var month = 'Feb';
        }
        else if(obj[data].date.substr(5, 2) == 03)
        {
          var month = 'Mar';
        }
        else if(obj[data].date.substr(5, 2) == 04)
        {
          var month = 'Apr';
        }
        else if(obj[data].date.substr(5, 2) == 05)
        {
          var month = 'May';
        }
        else if(obj[data].date.substr(5, 2) == 06)
        {
          var month = 'Jun';
        }
        else if(obj[data].date.substr(5, 2) == 07)
        {
          var month = 'Jul';
        }
        else if(obj[data].date.substr(5, 2) == 08)
        {
          var month = 'Aug';
        }
        else if(obj[data].date.substr(5, 2) == 09)
        {
          var month = 'Sep';
        }
        else if(obj[data].date.substr(5, 2) == 10)
        {
          var month = 'Oct';
        }
        else if(obj[data].date.substr(5, 2) == 11)
        {
          var month = 'Nov';
        }
        else if(obj[data].date.substr(5, 2) == 12)
        {
          var month = 'Dec';
        }

  
          $('#latestlist').append('<div class="articlediv">' +
            '<div class="articlediv">'+
              '<div class="datebox">'+
                '<div style="margin-top:10px;">'+
                  '<div style="font-size:35px;">' + day + '</div>'+
                  month+
                  year +
                '</div>'+
              '</div>'+
              '<div class="titlebox">'+
                '<a href="/post/view/' +category+ '/' +newsslugs+ '">'+
                  '<h3>' + title + '</h3>'+
                '</a>'+
                '<i class="icon-user"></i> By ' + obj[data].author +
                ' <i class="icon-comment"></i> <span class="disqus-comment-count" data-disqus-url="' + BASE_URL + '/post/view/' +category+ '/' +newsslugs+ '">0 Comment</span>' +
              '</div>'+
            '</div>'+
            '<div style="margin-top:20px;" class="content2">'+
              body+
            '</div>'+
            '<div class="sepera2r"> </div>'+
            '<br>'+
            '<br>'+
          '</div>');


      }
    }
    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if (obj.length < 5)
    {
      console.log('end');
      $scope.notendresult = false;
      $scope.endresult = true;
    }
    else
    {
      console.log('nah');
      $scope.notendresult = true;
      $scope.endresult = false;
    }
  });
}
});

app.controller("postcategoryctrl", function ($scope, $http,BASE_URL) {
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 5;
  $scope.getlatest = function (data) {
      
      var disqus_shortname = 'ilchilee';

  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = '//' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
  }());



    var pageid = data;
    $scope.showloading = true;
    $http.get(API_URL + '/post/category/' + offset + '/' + data).success(function (response) {


     var obj = JSON.parse(JSON.stringify(response));

     if (response) {
      for (var data in obj) {
        var newsslugs = obj[data].newsslugs;
        var newsid = obj[data].newsid;
        var category = obj[data].category;
        var title = obj[data].title;
        var body = obj[data].body;
        var year = obj[data].date.substr(0, 4);
        var day =  obj[data].date.substr(-2);
        
        if(obj[data].date.substr(5, 2) == 01)
        {
          var month = 'Jan';
        }
        else if(obj[data].date.substr(5, 2) == 02)
        {
          var month = 'Feb';
        }
        else if(obj[data].date.substr(5, 2) == 03)
        {
          var month = 'Mar';
        }
        else if(obj[data].date.substr(5, 2) == 04)
        {
          var month = 'Apr';
        }
        else if(obj[data].date.substr(5, 2) == 05)
        {
          var month = 'May';
        }
        else if(obj[data].date.substr(5, 2) == 06)
        {
          var month = 'Jun';
        }
        else if(obj[data].date.substr(5, 2) == 07)
        {
          var month = 'Jul';
        }
        else if(obj[data].date.substr(5, 2) == 08)
        {
          var month = 'Aug';
        }
        else if(obj[data].date.substr(5, 2) == 09)
        {
          var month = 'Sep';
        }
        else if(obj[data].date.substr(5, 2) == 10)
        {
          var month = 'Oct';
        }
        else if(obj[data].date.substr(5, 2) == 11)
        {
          var month = 'Nov';
        }
        else if(obj[data].date.substr(5, 2) == 12)
        {
          var month = 'Dec';
        }

  
          $('#latestlist').append('<div class="articlediv">' +
            '<div class="articlediv">'+
              '<div class="datebox">'+
                '<div style="margin-top:10px;">'+
                  '<div style="font-size:35px;">' + day + '</div>'+
                  month+
                  year +
                '</div>'+
              '</div>'+
              '<div class="titlebox">'+
                '<a href="/post/view/' +category+ '/' +newsslugs+ '">'+
                  '<h3>' + title + '</h3>'+
                '</a>'+
                '<i class="icon-user"></i> By ' + obj[data].author +
                 ' <i class="icon-comment"></i> <span class="disqus-comment-count" data-disqus-url="' + BASE_URL + '/post/view/' +category+ '/' +newsslugs+ '">0 Comment</span>' +
              '</div>'+
            '</div>'+
            '<div style="margin-top:20px;" class="content2">'+
              body+
            '</div>'+
            '<div class="sepera2r"> </div>'+
            '<br>'+
            '<br>'+
          '</div>');


      }
    }
    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if (obj.length < 5)
    {
      console.log('end');
      $scope.notendresult = false;
      $scope.endresult = true;
    }
    else
    {
      console.log('nah');
      $scope.notendresult = true;
      $scope.endresult = false;
    }
  });
}
});



app.controller("posttagsctrl", function ($scope, $http,BASE_URL) {
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 5;
  $scope.getlatest = function (data) {
    
    var disqus_shortname = 'ilchilee';

  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = '//' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
  }());

    var pageid = data;
    $scope.showloading = true;
    $http.get(API_URL + '/post/category/' + offset + '/' + data).success(function (response) {


     var obj = JSON.parse(JSON.stringify(response));

     if (response) {
      for (var data in obj) {
        var newsslugs = obj[data].newsslugs;
        var newsid = obj[data].newsid;
        var category = obj[data].category;
        var title = obj[data].title;
        var body = obj[data].body;
        var year = obj[data].date.substr(0, 4);
        var day =  obj[data].date.substr(-2);
        
        if(obj[data].date.substr(5, 2) == 01)
        {
          var month = 'Jan';
        }
        else if(obj[data].date.substr(5, 2) == 02)
        {
          var month = 'Feb';
        }
        else if(obj[data].date.substr(5, 2) == 03)
        {
          var month = 'Mar';
        }
        else if(obj[data].date.substr(5, 2) == 04)
        {
          var month = 'Apr';
        }
        else if(obj[data].date.substr(5, 2) == 05)
        {
          var month = 'May';
        }
        else if(obj[data].date.substr(5, 2) == 06)
        {
          var month = 'Jun';
        }
        else if(obj[data].date.substr(5, 2) == 07)
        {
          var month = 'Jul';
        }
        else if(obj[data].date.substr(5, 2) == 08)
        {
          var month = 'Aug';
        }
        else if(obj[data].date.substr(5, 2) == 09)
        {
          var month = 'Sep';
        }
        else if(obj[data].date.substr(5, 2) == 10)
        {
          var month = 'Oct';
        }
        else if(obj[data].date.substr(5, 2) == 11)
        {
          var month = 'Nov';
        }
        else if(obj[data].date.substr(5, 2) == 12)
        {
          var month = 'Dec';
        }

  
          $('#latestlist').append('<div class="articlediv">' +
            '<div class="articlediv">'+
              '<div class="datebox">'+
                '<div style="margin-top:10px;">'+
                  '<div style="font-size:35px;">' + day + '</div>'+
                  month+
                  year +
                '</div>'+
              '</div>'+
              '<div class="titlebox">'+
                '<a href="/post/view/' +category+ '/' +newsslugs+ '">'+
                  '<h3>' + title + '</h3>'+
                '</a>'+
                '<i class="icon-user"></i> By ' + obj[data].author +
                 ' <i class="icon-comment"></i> <span class="disqus-comment-count" data-disqus-url="' + BASE_URL + '/post/view/' +category+ '/' +newsslugs+ '">0 Comment</span>' +
              '</div>'+
            '</div>'+
            '<div style="margin-top:20px;" class="content2">'+
              body+
            '</div>'+
            '<div class="sepera2r"> </div>'+
            '<br>'+
            '<br>'+
          '</div>');


      }
    }
    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if (obj.length < 5)
    {
      console.log('end');
      $scope.notendresult = false;
      $scope.endresult = true;
    }
    else
    {
      console.log('nah');
      $scope.notendresult = true;
      $scope.endresult = false;
    }
  });
}
});



app.controller("postrecentctrl", function ($scope, $http) {
  $scope.showloading = false;
  $scope.notendresult = true;
  $scope.endresult = false;
  var offset = 5;
  $scope.getrecent = function (data) {


    var pageid = data;
    $scope.showloading = true;
    $http.get(API_URL + '/post/post/' + offset).success(function (response) {


     var obj = JSON.parse(JSON.stringify(response));

     if (response) {
      for (var data in obj) {
        var newsslugs = obj[data].newsslugs;
        var newsid = obj[data].newsid;
        var category = obj[data].category;
        var title = '';
        var date = obj[data].date;
        var banner = obj[data].banner;

       

        if( obj[data].title.length < 30)
        {
          title = obj[data].title;
        }
        else
        {
           title = obj[data].title.substr(0, 45)+'...';
        }
      
        
     

  
        $('#listrecent').append('<div style="clear:both;margin-bottom:30px;">'+
          '<a href="/post/view/' + category + '/' + newsslugs + '">'+
          '<div class="sidebarimage" style="background-image: url(&quot;' + API_URL +'/images/sliderimages/' + banner + '&quot;);">'+
          '</div>'+
          '</a>'+
          '<div style="height:74px;">'+
          '<a href="/post/view/' + category + '/' + newsslugs + '">'+
          '<h6>' + title + '</h6>'+
          '</a>'+
          date+
          '</div>'+
          '</div>');


      }
    }
    offset = offset + obj.length;
    $scope.showloading = false;
    console.log(offset);
    if (obj.length < 5)
    {
      console.log('end');
      $scope.notendresult = false;
      $scope.endresult = true;
    }
    else
    {
      console.log('nah');
      $scope.notendresult = true;
      $scope.endresult = false;
    }
  });
}
});




app.controller("contacts", function($scope, $http,API_URL){
  $scope.showalert = false;
  $scope.isSaving = false;
  $scope.news = {};
  $scope.master = {};
  var orig = angular.copy($scope.master);
  $scope.saveMessage = function(contacts) {
            //Execute Here
            $scope.alerts = [];
            $scope.closeAlert = function(index) {
              $scope.alerts.splice(index, 1);
            };
            $scope.isSaving = true;
            $http({
              url: API_URL + "/contacts/send",
              method: "POST",
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param(contacts)
            }).success(function(data, status, headers, config) {
              console.log(API_URL);
              console.log(data);
              $scope.isSaving = false;
              $scope.alertsmsg ='Your message was sent successfully. Thanks.';
              $scope.showalert = true;
              $scope.contacts.name=null;
              $scope.contacts.email=null;  
              $scope.contacts.message=null;    
              $scope.formNews.$setPristine(true) 
            }).error(function(data, status, headers, config) {
              scope.alerts.push({
                type: 'danger',
                msg: 'Something went wrong please check your fields'
              });
            });





          }


        });


// jimmy


app.controller("NMSubcribersCtrl", function ($scope, $http) {
  
    $scope.blanko = '';
    $scope.emailtaken = '';
    $scope.emailregister = '';
    $scope.success = false;
    var oriUser = angular.copy($scope.nms);
    $scope.SaveNMS = function (nms) {
        console.log(nms);
        var mailtest = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(nms.NMSemail);
        if (mailtest == false) {
            $scope.blanko = 'Ok';
            $scope.emailtaken = '';
            $scope.emailregister = '';
            return           
        } else {
            $scope.process = true;
            $http.post(API_URL + '/subscribers/addNMS', nms).success(function (response) {
                if (response.hasOwnProperty('emailtaken')) {
                    $scope.emailtaken = response.emailtaken;
                    $scope.emailregister = '';
                    $scope.blanko = '';
                    $scope.success = false;
                    return
                } else {
                    $scope.emailregister = response.emailregister;
                    $scope.emailtaken = '';
                    $scope.blanko = '';
                    $scope.success = true;
                    //$scope.process = false;
                    $('html, body').animate({
                        scrollTop: $("#donat").offset().top
                    }, 2000);
                    $scope.nms = angular.copy(oriUser);
                    $scope.formnms.$setPristine(true);
                }
                $scope.response = response;
            });
        }
    }
});

app.factory('MDY', function(){
  return {
    month: function(){
      return [
      {name:'January', val:1},
      {name:'February', val:2},
      {name:'March', val:3},
      {name:'April', val:4},
      {name:'May', val:5},
      {name:'June', val:6},
      {name:'July', val:7},
      {name:'August', val:8},
      {name:'September', val:9},
      {name:'October', val:10},
      {name:'November', val:11},
      {name:'December', val:12}
      ]
    },
    day: function(){
      var day = [];
      for(var x=1; x<=31; x++){
        day.push({'val': x })
      }
      return day;
    },
    year: function(){
      var year = [];
      for(var y=2013; y>=1950; y--){
        year.push({'val': y })
      }
      return year;
    }
  }
});
app.factory('Countries', function(){

  return {
    list: function(){
      return [
      {
        "name": "Afghanistan",
        "code": "AF"
      },
      {
        "name": "Åland Islands",
        "code": "AX"
      },
      {
        "name": "Albania",
        "code": "AL"
      },
      {
        "name": "Algeria",
        "code": "DZ"
      },
      {
        "name": "American Samoa",
        "code": "AS"
      },
      {
        "name": "AndorrA",
        "code": "AD"
      },
      {
        "name": "Angola",
        "code": "AO"
      },
      {
        "name": "Anguilla",
        "code": "AI"
      },
      {
        "name": "Antarctica",
        "code": "AQ"
      },
      {
        "name": "Antigua and Barbuda",
        "code": "AG"
      },
      {
        "name": "Argentina",
        "code": "AR"
      },
      {
        "name": "Armenia",
        "code": "AM"
      },
      {
        "name": "Aruba",
        "code": "AW"
      },
      {
        "name": "Australia",
        "code": "AU"
      },
      {
        "name": "Austria",
        "code": "AT"
      },
      {
        "name": "Azerbaijan",
        "code": "AZ"
      },
      {
        "name": "Bahamas",
        "code": "BS"
      },
      {
        "name": "Bahrain",
        "code": "BH"
      },
      {
        "name": "Bangladesh",
        "code": "BD"
      },
      {
        "name": "Barbados",
        "code": "BB"
      },
      {
        "name": "Belarus",
        "code": "BY"
      },
      {
        "name": "Belgium",
        "code": "BE"
      },
      {
        "name": "Belize",
        "code": "BZ"
      },
      {
        "name": "Benin",
        "code": "BJ"
      },
      {
        "name": "Bermuda",
        "code": "BM"
      },
      {
        "name": "Bhutan",
        "code": "BT"
      },
      {
        "name": "Bolivia",
        "code": "BO"
      },
      {
        "name": "Bosnia and Herzegovina",
        "code": "BA"
      },
      {
        "name": "Botswana",
        "code": "BW"
      },
      {
        "name": "Bouvet Island",
        "code": "BV"
      },
      {
        "name": "Brazil",
        "code": "BR"
      },
      {
        "name": "British Indian Ocean Territory",
        "code": "IO"
      },
      {
        "name": "Brunei Darussalam",
        "code": "BN"
      },
      {
        "name": "Bulgaria",
        "code": "BG"
      },
      {
        "name": "Burkina Faso",
        "code": "BF"
      },
      {
        "name": "Burundi",
        "code": "BI"
      },
      {
        "name": "Cambodia",
        "code": "KH"
      },
      {
        "name": "Cameroon",
        "code": "CM"
      },
      {
        "name": "Canada",
        "code": "CA"
      },
      {
        "name": "Cape Verde",
        "code": "CV"
      },
      {
        "name": "Cayman Islands",
        "code": "KY"
      },
      {
        "name": "Central African Republic",
        "code": "CF"
      },
      {
        "name": "Chad",
        "code": "TD"
      },
      {
        "name": "Chile",
        "code": "CL"
      },
      {
        "name": "China",
        "code": "CN"
      },
      {
        "name": "Christmas Island",
        "code": "CX"
      },
      {
        "name": "Cocos (Keeling) Islands",
        "code": "CC"
      },
      {
        "name": "Colombia",
        "code": "CO"
      },
      {
        "name": "Comoros",
        "code": "KM"
      },
      {
        "name": "Congo",
        "code": "CG"
      },
      {
        "name": "Congo, The Democratic Republic of the",
        "code": "CD"
      },
      {
        "name": "Cook Islands",
        "code": "CK"
      },
      {
        "name": "Costa Rica",
        "code": "CR"
      },
      {
        "name": "Cote D\"Ivoire",
        "code": "CI"
      },
      {
        "name": "Croatia",
        "code": "HR"
      },
      {
        "name": "Cuba",
        "code": "CU"
      },
      {
        "name": "Cyprus",
        "code": "CY"
      },
      {
        "name": "Czech Republic",
        "code": "CZ"
      },
      {
        "name": "Denmark",
        "code": "DK"
      },
      {
        "name": "Djibouti",
        "code": "DJ"
      },
      {
        "name": "Dominica",
        "code": "DM"
      },
      {
        "name": "Dominican Republic",
        "code": "DO"
      },
      {
        "name": "Ecuador",
        "code": "EC"
      },
      {
        "name": "Egypt",
        "code": "EG"
      },
      {
        "name": "El Salvador",
        "code": "SV"
      },
      {
        "name": "Equatorial Guinea",
        "code": "GQ"
      },
      {
        "name": "Eritrea",
        "code": "ER"
      },
      {
        "name": "Estonia",
        "code": "EE"
      },
      {
        "name": "Ethiopia",
        "code": "ET"
      },
      {
        "name": "Falkland Islands (Malvinas)",
        "code": "FK"
      },
      {
        "name": "Faroe Islands",
        "code": "FO"
      },
      {
        "name": "Fiji",
        "code": "FJ"
      },
      {
        "name": "Finland",
        "code": "FI"
      },
      {
        "name": "France",
        "code": "FR"
      },
      {
        "name": "French Guiana",
        "code": "GF"
      },
      {
        "name": "French Polynesia",
        "code": "PF"
      },
      {
        "name": "French Southern Territories",
        "code": "TF"
      },
      {
        "name": "Gabon",
        "code": "GA"
      },
      {
        "name": "Gambia",
        "code": "GM"
      },
      {
        "name": "Georgia",
        "code": "GE"
      },
      {
        "name": "Germany",
        "code": "DE"
      },
      {
        "name": "Ghana",
        "code": "GH"
      },
      {
        "name": "Gibraltar",
        "code": "GI"
      },
      {
        "name": "Greece",
        "code": "GR"
      },
      {
        "name": "Greenland",
        "code": "GL"
      },
      {
        "name": "Grenada",
        "code": "GD"
      },
      {
        "name": "Guadeloupe",
        "code": "GP"
      },
      {
        "name": "Guam",
        "code": "GU"
      },
      {
        "name": "Guatemala",
        "code": "GT"
      },
      {
        "name": "Guernsey",
        "code": "GG"
      },
      {
        "name": "Guinea",
        "code": "GN"
      },
      {
        "name": "Guinea-Bissau",
        "code": "GW"
      },
      {
        "name": "Guyana",
        "code": "GY"
      },
      {
        "name": "Haiti",
        "code": "HT"
      },
      {
        "name": "Heard Island and Mcdonald Islands",
        "code": "HM"
      },
      {
        "name": "Holy See (Vatican City State)",
        "code": "VA"
      },
      {
        "name": "Honduras",
        "code": "HN"
      },
      {
        "name": "Hong Kong",
        "code": "HK"
      },
      {
        "name": "Hungary",
        "code": "HU"
      },
      {
        "name": "Iceland",
        "code": "IS"
      },
      {
        "name": "India",
        "code": "IN"
      },
      {
        "name": "Indonesia",
        "code": "ID"
      },
      {
        "name": "Iran, Islamic Republic Of",
        "code": "IR"
      },
      {
        "name": "Iraq",
        "code": "IQ"
      },
      {
        "name": "Ireland",
        "code": "IE"
      },
      {
        "name": "Isle of Man",
        "code": "IM"
      },
      {
        "name": "Israel",
        "code": "IL"
      },
      {
        "name": "Italy",
        "code": "IT"
      },
      {
        "name": "Jamaica",
        "code": "JM"
      },
      {
        "name": "Japan",
        "code": "JP"
      },
      {
        "name": "Jersey",
        "code": "JE"
      },
      {
        "name": "Jordan",
        "code": "JO"
      },
      {
        "name": "Kazakhstan",
        "code": "KZ"
      },
      {
        "name": "Kenya",
        "code": "KE"
      },
      {
        "name": "Kiribati",
        "code": "KI"
      },
      {
        "name": "Korea, Democratic People\"S Republic of",
        "code": "KP"
      },
      {
        "name": "Korea, Republic of",
        "code": "KR"
      },
      {
        "name": "Kuwait",
        "code": "KW"
      },
      {
        "name": "Kyrgyzstan",
        "code": "KG"
      },
      {
        "name": "Lao People\"S Democratic Republic",
        "code": "LA"
      },
      {
        "name": "Latvia",
        "code": "LV"
      },
      {
        "name": "Lebanon",
        "code": "LB"
      },
      {
        "name": "Lesotho",
        "code": "LS"
      },
      {
        "name": "Liberia",
        "code": "LR"
      },
      {
        "name": "Libyan Arab Jamahiriya",
        "code": "LY"
      },
      {
        "name": "Liechtenstein",
        "code": "LI"
      },
      {
        "name": "Lithuania",
        "code": "LT"
      },
      {
        "name": "Luxembourg",
        "code": "LU"
      },
      {
        "name": "Macao",
        "code": "MO"
      },
      {
        "name": "Macedonia, The Former Yugoslav Republic of",
        "code": "MK"
      },
      {
        "name": "Madagascar",
        "code": "MG"
      },
      {
        "name": "Malawi",
        "code": "MW"
      },
      {
        "name": "Malaysia",
        "code": "MY"
      },
      {
        "name": "Maldives",
        "code": "MV"
      },
      {
        "name": "Mali",
        "code": "ML"
      },
      {
        "name": "Malta",
        "code": "MT"
      },
      {
        "name": "Marshall Islands",
        "code": "MH"
      },
      {
        "name": "Martinique",
        "code": "MQ"
      },
      {
        "name": "Mauritania",
        "code": "MR"
      },
      {
        "name": "Mauritius",
        "code": "MU"
      },
      {
        "name": "Mayotte",
        "code": "YT"
      },
      {
        "name": "Mexico",
        "code": "MX"
      },
      {
        "name": "Micronesia, Federated States of",
        "code": "FM"
      },
      {
        "name": "Moldova, Republic of",
        "code": "MD"
      },
      {
        "name": "Monaco",
        "code": "MC"
      },
      {
        "name": "Mongolia",
        "code": "MN"
      },
      {
        "name": "Montserrat",
        "code": "MS"
      },
      {
        "name": "Morocco",
        "code": "MA"
      },
      {
        "name": "Mozambique",
        "code": "MZ"
      },
      {
        "name": "Myanmar",
        "code": "MM"
      },
      {
        "name": "Namibia",
        "code": "NA"
      },
      {
        "name": "Nauru",
        "code": "NR"
      },
      {
        "name": "Nepal",
        "code": "NP"
      },
      {
        "name": "Netherlands",
        "code": "NL"
      },
      {
        "name": "Netherlands Antilles",
        "code": "AN"
      },
      {
        "name": "New Caledonia",
        "code": "NC"
      },
      {
        "name": "New Zealand",
        "code": "NZ"
      },
      {
        "name": "Nicaragua",
        "code": "NI"
      },
      {
        "name": "Niger",
        "code": "NE"
      },
      {
        "name": "Nigeria",
        "code": "NG"
      },
      {
        "name": "Niue",
        "code": "NU"
      },
      {
        "name": "Norfolk Island",
        "code": "NF"
      },
      {
        "name": "Northern Mariana Islands",
        "code": "MP"
      },
      {
        "name": "Norway",
        "code": "NO"
      },
      {
        "name": "Oman",
        "code": "OM"
      },
      {
        "name": "Pakistan",
        "code": "PK"
      },
      {
        "name": "Palau",
        "code": "PW"
      },
      {
        "name": "Palestinian Territory, Occupied",
        "code": "PS"
      },
      {
        "name": "Panama",
        "code": "PA"
      },
      {
        "name": "Papua New Guinea",
        "code": "PG"
      },
      {
        "name": "Paraguay",
        "code": "PY"
      },
      {
        "name": "Peru",
        "code": "PE"
      },
      {
        "name": "Philippines",
        "code": "PH"
      },
      {
        "name": "Pitcairn",
        "code": "PN"
      },
      {
        "name": "Poland",
        "code": "PL"
      },
      {
        "name": "Portugal",
        "code": "PT"
      },
      {
        "name": "Puerto Rico",
        "code": "PR"
      },
      {
        "name": "Qatar",
        "code": "QA"
      },
      {
        "name": "Reunion",
        "code": "RE"
      },
      {
        "name": "Romania",
        "code": "RO"
      },
      {
        "name": "Russian Federation",
        "code": "RU"
      },
      {
        "name": "RWANDA",
        "code": "RW"
      },
      {
        "name": "Saint Helena",
        "code": "SH"
      },
      {
        "name": "Saint Kitts and Nevis",
        "code": "KN"
      },
      {
        "name": "Saint Lucia",
        "code": "LC"
      },
      {
        "name": "Saint Pierre and Miquelon",
        "code": "PM"
      },
      {
        "name": "Saint Vincent and the Grenadines",
        "code": "VC"
      },
      {
        "name": "Samoa",
        "code": "WS"
      },
      {
        "name": "San Marino",
        "code": "SM"
      },
      {
        "name": "Sao Tome and Principe",
        "code": "ST"
      },
      {
        "name": "Saudi Arabia",
        "code": "SA"
      },
      {
        "name": "Senegal",
        "code": "SN"
      },
      {
        "name": "Serbia and Montenegro",
        "code": "CS"
      },
      {
        "name": "Seychelles",
        "code": "SC"
      },
      {
        "name": "Sierra Leone",
        "code": "SL"
      },
      {
        "name": "Singapore",
        "code": "SG"
      },
      {
        "name": "Slovakia",
        "code": "SK"
      },
      {
        "name": "Slovenia",
        "code": "SI"
      },
      {
        "name": "Solomon Islands",
        "code": "SB"
      },
      {
        "name": "Somalia",
        "code": "SO"
      },
      {
        "name": "South Africa",
        "code": "ZA"
      },
      {
        "name": "South Georgia and the South Sandwich Islands",
        "code": "GS"
      },
      {
        "name": "Spain",
        "code": "ES"
      },
      {
        "name": "Sri Lanka",
        "code": "LK"
      },
      {
        "name": "Sudan",
        "code": "SD"
      },
      {
        "name": "Suriname",
        "code": "SR"
      },
      {
        "name": "Svalbard and Jan Mayen",
        "code": "SJ"
      },
      {
        "name": "Swaziland",
        "code": "SZ"
      },
      {
        "name": "Sweden",
        "code": "SE"
      },
      {
        "name": "Switzerland",
        "code": "CH"
      },
      {
        "name": "Syrian Arab Republic",
        "code": "SY"
      },
      {
        "name": "Taiwan, Province of China",
        "code": "TW"
      },
      {
        "name": "Tajikistan",
        "code": "TJ"
      },
      {
        "name": "Tanzania, United Republic of",
        "code": "TZ"
      },
      {
        "name": "Thailand",
        "code": "TH"
      },
      {
        "name": "Timor-Leste",
        "code": "TL"
      },
      {
        "name": "Togo",
        "code": "TG"
      },
      {
        "name": "Tokelau",
        "code": "TK"
      },
      {
        "name": "Tonga",
        "code": "TO"
      },
      {
        "name": "Trinidad and Tobago",
        "code": "TT"
      },
      {
        "name": "Tunisia",
        "code": "TN"
      },
      {
        "name": "Turkey",
        "code": "TR"
      },
      {
        "name": "Turkmenistan",
        "code": "TM"
      },
      {
        "name": "Turks and Caicos Islands",
        "code": "TC"
      },
      {
        "name": "Tuvalu",
        "code": "TV"
      },
      {
        "name": "Uganda",
        "code": "UG"
      },
      {
        "name": "Ukraine",
        "code": "UA"
      },
      {
        "name": "United Arab Emirates",
        "code": "AE"
      },
      {
        "name": "United Kingdom",
        "code": "GB"
      },
      {
        "name": "United States",
        "code": "US"
      },
      {
        "name": "United States Minor Outlying Islands",
        "code": "UM"
      },
      {
        "name": "Uruguay",
        "code": "UY"
      },
      {
        "name": "Uzbekistan",
        "code": "UZ"
      },
      {
        "name": "Vanuatu",
        "code": "VU"
      },
      {
        "name": "Venezuela",
        "code": "VE"
      },
      {
        "name": "Viet Nam",
        "code": "VN"
      },
      {
        "name": "Virgin Islands, British",
        "code": "VG"
      },
      {
        "name": "Virgin Islands, U.S.",
        "code": "VI"
      },
      {
        "name": "Wallis and Futuna",
        "code": "WF"
      },
      {
        "name": "Western Sahara",
        "code": "EH"
      },
      {
        "name": "Yemen",
        "code": "YE"
      },
      {
        "name": "Zambia",
        "code": "ZM"
      },
      {
        "name": "Zimbabwe",
        "code": "ZW"
      }
      ];
    }

  }
})
.constant('API_URL', 'http://ilchiapi')
.constant('BASE_URL', 'http://ilchisite.com');
