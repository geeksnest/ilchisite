'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('CreateNewsCtrl', function($scope, $state, $window, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){

	//Convert TEXT TO SLUG
	$scope.onnewstitle = function convertToSlug(Text){
		$scope.news.slugs = angular.lowercase(Text.replace(/[^\w ]+/g,'').replace(/ +/g,'-'));
	}

	//LOAD CATEGORY
	var loadTags =function(){
		dataFactory.getData({
			apiUrl: "/news/listcategory"
		},
		function(_rdata){
			$scope.category = _rdata;
		});
	}
	loadTags();

	//LIST CATEGORY
	var listCategory = function(){
		dataFactory.getData({
			apiUrl: "/news/categorylist"
		},
		function(_rdata){
			$scope.datalist = _rdata;
		});
	}
	listCategory();


	//RESET FORM
	$scope.reset = function(){
		$scope.news = " ";
		$scope.news.body ='';
		$scope.news.tags = [];
		$('.chosen-choices').html('<li class="search-field"><input type="text" value="Select Some Options" class="default" autocomplete="off" style="width: 138px;"></li>');
		$('.chosen-results').addClass("active-result"); 
		$("#tagoption").trigger("liszt:updated"); 
	};

    //ADD CATEGORY
    $scope.addcategory = function(){
    	var modalInstance = $modal.open({
    		templateUrl: 'addcategory.html',
    		controller:function($scope, $modalInstance) {
    			$scope.ok =function(category) {
    				dataFactory.saveData({
    					apiUrl:"/news/savecategory"
    				},
    				category,
    				function(_rdata){
    					loadcategory();
    					$modalInstance.close();
    					$scope.success = true;
    				});
    			};
    			$scope.cancel =function(){
    				$modalInstance.dismiss('cancel');
    			};
    		}
    	});
    }

    //ADD TAGS
    $scope.addtags = function() {
    	var modalInstance = $modal.open({
    		templateUrl: 'addcategory.html',
    		controller:function($scope, $modalInstance) {
    			$scope.ok = function(category) {
    				dataFactory.saveData({
    					apiUrl:"/news/savetag"
    				},
    				category,
    				function(_rdata){
    					loadTags();
    					$modalInstance.close();
    					$scope.success = true;
    				});
    			};
    			$scope.cancel = function() {
    				$modalInstance.dismiss('cancel');
    			};
    		}
    	});
    }


    //IMAGE UPLOAD LOADER
    var _imgLoader =function(imageloader, imagecontent){
    	$scope.imageloader  = imageloader;
    	$scope.imagecontent = imagecontent;
    }
    _imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			dataFactory.getData({
				apiUrl: '/news/ajaxfileuploader/'+ rdata.config.file.name + '/New Image'
			},
			function(_rdata){
				$scope.projImg = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
			});
			
		})
	}

	var selctimg = function(img){
		$scope.projImg = img;
		console.log($scope.projImg);
	}

	//MODAL MEDIA GALLERY
	$scope.mediaGallery =function(){
		var modalInstance = $modal.open({
			templateUrl: 'mediagallery.html',
			size:'lg',
			controller:function($scope, $modalInstance, Upload){

    			//LOAD UPLOADED IMAGES
    			var loadImages = function() {
    				dataFactory.getData({
    					apiUrl: "/news/listnewsimages"
    				},
    				function(_rdata){
    					angular.forEach(_rdata, function(value, key) {
							_rdata[key].imgpath = Config.amazonlink+"/uploads/image/"+_rdata[key].imgpath; //Change C3 Full Path iMAge
						});
    					console.log(_rdata);
    					$scope.data = _rdata;
    				});
    			}
    			loadImages();

    			//DELETE IMAGE
    			$scope.dltnewsimageClick = function(imageid) {
    				var modalInstance = $modal.open({
    					templateUrl: 'editMemberModal.html',
    					controller: function($scope, $modalInstance) {
    						$scope.imageid = imageid;
    						$scope.ok = function(imageid) {
    							dataFactory.saveData({
    								apiUrl:"/news/dltpagephoto"
    							},
    							imageid,
    							function(_rdata){
    								loadImages();
    								$modalInstance.dismiss('cancel');
    							});
    						};
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			$scope.path = function(path) {
    				var modalInstance = $modal.open({
    					templateUrl: 'imgurl.html',
    					controller: function($scope, $modalInstance) {
    						$scope.imgpath = path;
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			//IMAGE UPLOAD LOADER
    			var _imgLoader =function(imageloader, imagecontent){
    				$scope.imageloader  = imageloader;
    				$scope.imagecontent = imagecontent;
    			}
    			_imgLoader(false, true);

				//Upload image
				$scope.prepare=function(file){
					dataFactory.imageUpload({
					    dataFile:file, //Image or File to be upload
					    apiUrl:null, //Your API Url
					    amazonS3:"uploads/image/" //Amazon S3 File path
					},
					function(loader){
					   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
					},
					function(rdata){
						dataFactory.getData({
							apiUrl: '/news/ajaxfileuploader/'+ rdata.config.file.name + '/New Image'
						},
						function(_rdata){
							loadImages();
						});
					})
				}

				$scope.slctimage = function(image){
					selctimg(image);
					$modalInstance.dismiss('cancel');
				}

				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');
				};
			}
		});
	}

	//SAVE NEWS
	$scope.saveNews = function(news) {
		$scope.alerts = [];
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};
		$scope.isSaving = true;

		dataFactory.saveData({
			apiUrl:"/news/create"
		},
		news,
		function(_rdata){
			$scope.isSaving = false;
			$scope.alerts.push({ type: 'success', msg: 'News successfully saved!' });
			$scope.reset();   
			$scope.formNews.$setPristine(true) 
			$scope.projImg ='';
			$scope.news.tags = [];
			$window.open("http://ilchi.com/post/view/" + news.category + "/" + news.slugs, "_blank");
		});
	}

})




