'use strict';

/*
* 
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
* Readble as possible Rock in Roll to the World
* hang on and play the code 
*/
app.controller('sliderEditCtrl', function($scope, $state, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){


	$scope.closeAlert = function (index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.editbtn = true;
	$scope.savebtn = false;
	$scope.setmebtn=true;
	$scope.setmebtn1=false;
	document.getElementById("name").disabled = true;
	$scope.edit = function(name){
		$scope.editbtn = false;
		$scope.savebtn = true;
		document.getElementById("name").disabled = false;
	}

	//UPDATE ALBUM NAME
	$scope.savename=function(){
		$scope.alerts=[];
		$scope.editbtn = true;
		$scope.savebtn = false;
		document.getElementById("name").disabled = true;
		var albbumname=document.getElementById("name").value;

		dataFactory.getData({
			apiUrl:"/utility/editlabumname/"+$stateParams.id+"/"+albbumname
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			loadImage($stateParams.id);
			$scope.alerts.push({type: 'success', msg: 'Album Name successfully updated!'});
			
		});	
	}

	//GET FOLDER INFORMATION
	var getfolderinfo = function(id){
		dataFactory.getData({
			apiUrl:"/utility/editslider/"+id
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			$scope.album_info = _rdata;
			if(_rdata.active==1){
				$scope.setmebtn=false;
				$scope.setmebtn1=true;
			}
		});
	}
	getfolderinfo($stateParams.id);

	//LIST UPLOADED IMAGES
	var loadImage = function(id){
		dataFactory.getData({
			apiUrl:"/utility/sliderimagelist/"+id
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			angular.forEach(_rdata, function(value, key) {
				_rdata[key].imgpath = Config.amazonlink+"/uploads/image/"+_rdata[key].imgpath; //Change C3 Full Path iMAge
			});
			$scope.data = _rdata;
		});
	}
	loadImage($stateParams.id);

	//SAVE UPLOADED IMAGES
	var imageSave=function(filename, albumtitle, albumid){
		dataFactory.getData({
			apiUrl:'/utility/slider/'+ filename+'/'+albumtitle+'/'+albumid
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			loadImage(albumid);
		});
	}

	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	// UPLOAD IMAGE
	$scope.upload = function (files, gdata){
		dataFactory.imageUpload({
            dataFile:files, //Image or File to be upload
            apiUrl:"/pagestatic/saveimage", //Your API Url
            amazonS3:"uploads/image/" //Amazon S3 File path
        },
        function(loader){
            _imgLoader(loader.imageloader, loader.imagecontent) // Loader
        },
        function(rdata){
        	//Beyond here Do Something about the result you passed
        	//Console.log(rdata);
        	if(rdata.status == 204){
        		imageSave(rdata.config.file.name, gdata.title, $stateParams.id); //Save Image
        	}else{
        		//Something went Wrong when uploading image
        	}
        })
	}

	//Update Uploaded Image Information 
	$scope.albumid=$stateParams.id;
	$scope.page = {};
	$scope.saveboxmessage = '';
	$scope.imgInfo = function(info){
		$('.buttonPanel'+info.id).hide();
		dataFactory.saveData({
			apiUrl:"/utility/sliderphotoupdate"
		},
		info,
		function(_rdata){
			if(_rdata==0){
				loadImage($stateParams.id);
				$scope.saveboxmessage = info.id;
				$scope.notification = false;
			}else{
				$scope.notification = info.id;
				$scope.saveboxmessage = false ;
			}
			$('.buttonPanel'+info.id).show();
		});
	};	

	//MESSAGE ALERT
	$scope.closeAlert1 = function (index) {$scope.alert.splice(index, 1);};
	$scope.alert=[];
	var alertme1 = function(){ $scope.alert.push({type: 'success', msg: 'Photo has been successfully Deleted!'})}
	var error1   = function(){ $scope.alerterror.push({type: 'success', msg: 'Inputed Number already Exist!'})}

	//DELETE Uploaded Image
	$scope.dltimg = function(imageid, folderid){
		var modalInstance = $modal.open({
			templateUrl: 'deleteSliderPHoto.html',
			controller: function ($scope, $modalInstance) {
				$scope.imageid = imageid;
				$scope.ok = function (imageid) {
					dataFactory.saveData({
						apiUrl:"/utility/dltsliderphoto"
					},
					imageid,
					function(_rdata){
						// Beyond here Do Something about the return data
						loadImage($stateParams.id);
						$modalInstance.dismiss('cancel');
						alertme1();
					});
				};
				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};
			}
		});
	}

	//SET PRIMARY SLIDER
	$scope.set = function(){
		var modalInstance = $modal.open({
			templateUrl: 'setasslider.html',
			controller: function ($scope, $modalInstance) {
				$scope.ok = function(){
					dataFactory.getData({
						apiUrl:":/utility/setslider/"+$stateParams.id
					},
					function(_rdata){
						//Beyond here Do Something about the result you passed
						//console.log(_rdata);
						$modalInstance.dismiss('cancel');
					});	
				};
				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};
			},

		});
	}
})
