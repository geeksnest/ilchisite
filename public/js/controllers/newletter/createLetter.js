'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('CreateNewsLetterCtrl', function($scope, $state, $window, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){


	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			dataFactory.getData({
				apiUrl: '/newsletter/ajaxfileuploader/'+ rdata.config.file.name + '/'
			},
			function(_rdata){
				$scope.projImg = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
			});
			
		})
	}

	var selctimg = function(img){
		$scope.projImg = img;
		console.log($scope.projImg);
	}

	//MODAL MEDIA GALLERY
	$scope.mediaGallery =function(){
		var modalInstance = $modal.open({
			templateUrl: 'mediagallery.html',
			size:'lg',
			controller:function($scope, $modalInstance, Upload){

    			//LOAD UPLOADED IMAGES
    			var loadImages = function() {
    				dataFactory.getData({
    					apiUrl: "/newsletter/lstphoto"
    				},
    				function(_rdata){
    					angular.forEach(_rdata, function(value, key) {
							_rdata[key].imgpath = Config.amazonlink+"/uploads/image/"+_rdata[key].imgpath; //Change C3 Full Path iMAge
						});
    					$scope.data = _rdata;
    				});
    			}
    			loadImages();

    			//DELETE IMAGE
    			$scope.dltnewsimageClick = function(imageid){
    				var modalInstance = $modal.open({
    					templateUrl: 'editMemberModal.html',
    					controller: function($scope, $modalInstance){
    						$scope.imageid = imageid;
    						$scope.ok = function(imageid) {
    							console.log(imageid);

    							dataFactory.saveData({
    								apiUrl:"/newsletter/dltphoto"
    							},
    							imageid,
    							function(_rdata){
    								loadImages();
    								$modalInstance.dismiss('cancel');
    							});
    						};
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			$scope.path = function(path){
    				var modalInstance = $modal.open({
    					templateUrl: 'imgurl.html',
    					controller: function($scope, $modalInstance) {
    						$scope.imgpath = path;
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			//IMAGE UPLOAD LOADER
    			var _imgLoader =function(imageloader, imagecontent){
    				$scope.imageloader  = imageloader;
    				$scope.imagecontent = imagecontent;
    			}
    			_imgLoader(false, true);

				//Upload image
				$scope.prepare=function(file){
					dataFactory.imageUpload({
					    dataFile:file, //Image or File to be upload
					    apiUrl:null, //Your API Url
					    amazonS3:"uploads/image/" //Amazon S3 File path
					},
					function(loader){
					   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
					},
					function(rdata){
						dataFactory.getData({
							apiUrl: '/newsletter/ajaxfileuploader/'+ rdata.config.file.name + '/'
						},
						function(_rdata){
							loadImages();
						});
					})
				}

				$scope.slctimage = function(image){
					selctimg(image);
					$modalInstance.dismiss('cancel');
				}

				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');
				};
			}
		});
	}

	//PRVIEW NEW LETTER
	$scope.previewletter = function(letterbody) {
		var modalInstance = $modal.open({
			templateUrl: 'previewletter.html',
			controller:function($scope, $modalInstance, $sce) {
				$scope.htmlbody = $sce.trustAsHtml(letterbody);
				$scope.close = function() {
					$modalInstance.dismiss('cancel');
				};
			},

		});
	}

	//SAVE NEWS LETTER
	$scope.saveNewsLetter = function(newsletter) {
        //Execute Here
        $scope.alerts = [];
        $scope.closeAler =function(index){$scope.alerts.splice(index, 1);};
        $scope.isSaving = true;

        dataFactory.saveData({
        	apiUrl:"/newsletter/create"
        },
        newsletter,
        function(_rdata){
        	if(_rdata.success){
        		$scope.isSaving = false;
        		$scope.alerts.push({type: 'success',msg: 'News Letter successfully saved!'});
        		$scope.newsletter="";
        		$scope.form.$setPristine(true);
        	}else{
        		$scope.alerts.push({type: 'danger',msg: 'Something went wrong please check your fields'});
        	}
        });
    }

})




