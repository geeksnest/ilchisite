app.controller('HomeCtrl',  function ($http, $scope, $stateParams, $modal, $state) {


	//var titles = '{"_mediaTitle" : ['+'{ "title":"Weekly Insight"},'+'{ "title":"Weekly Video"},' +'{ "title":"Latest News"},' +'{ "title":"Featured Projects"} ]}';
	var _loadTitle = function(){
		$http({
			url: API_URL + "/pagestatic/medialTitleGet",
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			
			var titles = data.body;
			var objTitle = JSON.parse(titles);
			var ListTitle = objTitle._mediaTitle;
			$scope.data = ListTitle;
			$scope.datas = ListTitle;


		})

	}
	_loadTitle();

	$scope.updatealbumname = function(_index,_title, _gdata) {

		$scope.alerts = [];
		$scope.closeAlert = function (index) {
			$scope.alerts.splice(index, 1);
		};
		_gdata[_index].title = _title;

		var json = JSON.stringify( _gdata, function( key, value ) {
			if( key === "$$hashKey" ) {
				return undefined;
			}
			return value;
		});

		$http({
			url: API_URL + "/pagestatic/medialTitleSave",
			method: "POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $.param({data:'{"_mediaTitle" :'+json+'}'})
		}).success(function (data, status, headers, config) {
				if(data.success){
					$scope.alerts.push({type: 'success', msg: 'About Ilchi Succesfully Updates'});
				}else{
					$scope.alerts.push({type: 'warning', msg: 'Something went Wrong!!!'});
				}
				document.body.scrollTop = document.documentElement.scrollTop = 0;
			}).error(function (data, status, headers, config){
				console.log(datas);
		
			});
		};


		$http({
			url: API_URL + "/pagestatic/aboutGet",
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.about = data.body;
		})

		$scope.saveAbout= function(_gdata){
			$scope.alerts = [];
			$scope.closeAlert = function (index) {
				$scope.alerts.splice(index, 1);
			};
			$http({
				url: API_URL + "/pagestatic/aboutSave",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({about:_gdata})
			}).success(function (data, status, headers, config) {

				if(data.success){
					$scope.alerts.push({type: 'success', msg: 'About Ilchi Succesfully Updates'});
				}else{
					$scope.alerts.push({type: 'warning', msg: 'Something went Wrong!!!'});
				}
				document.body.scrollTop = document.documentElement.scrollTop = 0;
			}).error(function (data, status, headers, config){
			//Code etc.. Here 
		});
		}

	})