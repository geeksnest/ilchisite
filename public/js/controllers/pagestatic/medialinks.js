app.controller('MediaLinksCtrl',  function ($http, $scope, $stateParams, $modal, $state) {


	var _loadTitle = function(){
		$http({
			url: API_URL + "/pagestatic/socialLinksGet",
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			
			var titles = data.body;
			var objTitle = JSON.parse(titles);
			var ListTitle = objTitle._socialLinks;

			$scope.data = ListTitle;
			$scope.datas = ListTitle;
		})

	}
	_loadTitle();

	$scope.updatealbumname = function(_index,_title, _gdata) {
		$scope.alerts = [];
		$scope.closeAlert = function (index) {
			$scope.alerts.splice(index, 1);
		};
		_gdata[_index].title = _title;

		var json = JSON.stringify( _gdata, function( key, value ) {
			if( key === "$$hashKey" ) {
				return undefined;
			}
			return value;
		});
		$http({
			url: API_URL + "/pagestatic/socialLinksSave",
			method: "POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $.param({data:'{"_socialLinks" :'+json+'}'})
		}).success(function (data, status, headers, config) {
			if(data.success){
				$scope.alerts.push({type: 'success', msg: 'About Ilchi Succesfully Updates'});
			}else{
				$scope.alerts.push({type: 'warning', msg: 'Something went Wrong!!!'});
			}
			document.body.scrollTop = document.documentElement.scrollTop = 0;
		}).error(function (data, status, headers, config){
			console.log(datas);

		});
	};
})