app.controller('ContactCtrl',  function ($http, $scope, $stateParams, $modal, $state) {

	$http({
		url: API_URL + "/pagestatic/contactGet",
		method: "GET",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function (data, status, headers, config) {
		$scope.term = data.body;
	})
	
	$scope.save= function(_gdata){
		$scope.alerts = [];
		$scope.closeAlert = function (index) {
			$scope.alerts.splice(index, 1);
		};

		$http({
			url: API_URL + "/pagestatic/contactSave",
			method: "POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $.param({term:_gdata})
		}).success(function (data, status, headers, config) {

			if(data.success){
				$scope.alerts.push({type: 'success', msg: 'Contact Us Succesfully Updates'});
			}else{
				$scope.alerts.push({type: 'warning', msg: 'Something went Wrong!!!'});
			}
			document.body.scrollTop = document.documentElement.scrollTop = 0;
		}).error(function (data, status, headers, config){
			//Code etc.. Here 
		});
	}
})