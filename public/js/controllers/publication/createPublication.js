'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('createpubCtrl', function($scope, $state, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){

	$scope.imageselected = false;
	// $scope.loadingg = false;
	$scope.loadingg = false;
	$scope.imgAlerts = [];
	$scope.closeAlert = function(index) {
		$scope.imgAlerts.splice(index, 1);
	};
	$scope.imageselected = false;

	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			$scope.projImg = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
		})
	}

	$scope.isSaving = false;
	$scope.save = function(info){
		$scope.alerts = [];
		$scope.closeAlert = function (index) {
			$scope.alerts.splice(index, 1);
		};
		$scope.isSaving = true;

		dataFactory.saveData({
			apiUrl:"/utility/create" //Api Url
		},
		info,
		function(_rdata){
			// Beyond here Do Something about the return data
			if(_rdata==0) {
				$scope.alerts.push({type: 'danger', msg: 'Please Upload a photo for the submitted Publication'});
				$scope.isSaving = false;
			}
			else{
				$scope.info ="";
				$scope.isSaving = false;
				$scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
				$scope.form.$setPristine(true)
			}
		});
		
		
	}   
})




