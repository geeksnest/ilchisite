'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('editpubCtrl', function($scope, $state, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){

	$scope.stat="false";
	//Get Data
	var getInfoData= function(id){
		dataFactory.getData({
			apiUrl:"/utility/listeditpub/"+id
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			$scope.info = _rdata[0];
			$scope.stat = (_rdata[0].active==1 ? "Publish" : "Unpublish");
		});
	}
	getInfoData($stateParams.id);

	$scope.active= function(){
		$scope.stat = ($scope.info.active==1 ? "Publish" : "Unpublish");
	}

	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file , _gdata){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			_gdata.banner = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
		})
	}

	$scope.save = function(info){
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
        	$scope.alerts.splice(index, 1);
        };
        $scope.isSaving = true;

        dataFactory.saveData({
        	apiUrl:"/utility/updatepub"
        },
        info,
        function(_rdata){
			// Beyond here Do Something about the return data
			console.log(_rdata);
			$scope.isSaving = false;
			$scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
			$scope.form.$setPristine(true)
		});
    }   
})




