'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('CreatefeaturedprojectCtrl', function($scope, $state, $window, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){

	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			dataFactory.getData({
				apiUrl: '/featuredprojects/ajaxfileuploader/'+ rdata.config.file.name + '/'
			},
			function(_rdata){
				$scope.projImg = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
			});
			
		})
	}

	var selctimg = function(img){
		$scope.projImg = img;
		console.log($scope.projImg);
	}

	//MODAL MEDIA GALLERY
	$scope.mediaGallery =function(){
		var modalInstance = $modal.open({
			templateUrl: 'mediagallery.html',
			size:'lg',
			controller:function($scope, $modalInstance, Upload){

    			//LOAD UPLOADED IMAGES
    			var loadImages = function() {
    				dataFactory.getData({
    					apiUrl: "/featuredprojects/listsimg"
    				},
    				function(_rdata){
    					angular.forEach(_rdata, function(value, key) {
							_rdata[key].imgpath = Config.amazonlink+"/uploads/image/"+_rdata[key].imgpath; //Change C3 Full Path iMAge
						});
    					$scope.data = _rdata;
    				});
    			}
    			loadImages();

    			//DELETE IMAGE
    			$scope.dltnewsimageClick = function(imageid){
    				var modalInstance = $modal.open({
    					templateUrl: 'editMemberModal.html',
    					controller: function($scope, $modalInstance){
    						$scope.imageid = imageid;
    						$scope.ok = function(imageid) {
    							dataFactory.saveData({
    								apiUrl:"/featuredprojects/dltpagephoto"
    							},
    							imageid,
    							function(_rdata){
    								loadImages();
    								$modalInstance.dismiss('cancel');
    							});
    						};
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			$scope.path = function(path){
    				var modalInstance = $modal.open({
    					templateUrl: 'imgurl.html',
    					controller: function($scope, $modalInstance) {
    						$scope.imgpath = path;
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			//IMAGE UPLOAD LOADER
    			var _imgLoader =function(imageloader, imagecontent){
    				$scope.imageloader  = imageloader;
    				$scope.imagecontent = imagecontent;
    			}
    			_imgLoader(false, true);

				//Upload image
				$scope.prepare=function(file){
					dataFactory.imageUpload({
					    dataFile:file, //Image or File to be upload
					    apiUrl:null, //Your API Url
					    amazonS3:"uploads/image/" //Amazon S3 File path
					},
					function(loader){
					   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
					},
					function(rdata){
						dataFactory.getData({
							apiUrl: '/featuredprojects/ajaxfileuploader/'+ rdata.config.file.name + '/'
						},
						function(_rdata){
							loadImages();
						});
					})
				}

				$scope.slctimage = function(image){
					selctimg(image);
					$modalInstance.dismiss('cancel');
				}

				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');
				};
			}
		});
	}
	
	//SAVE DATA
	$scope.savePage = function(page){
        $scope.alerts = [];
        $scope.closeAlert = function(index){$scope.alerts.splice(index, 1)};
        $scope.isSaving = true;

        dataFactory.saveData({
        	apiUrl:"/featuredprojects/insdata"
        },
        page,
        function(_rdata){
        	if(_rdata.success){
        		$scope.isSaving = false;
        		$scope.alerts.push({type: 'success', msg: 'New Featured Project Added!'});
        		$scope.page="";
        		$scope.form.$setPristine(true)
        	}else{
        		$scope.alerts.push({type: 'error', msg: 'All fields is required.'});
        	}
        });
    }


    




})




