'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('settingsCtrl', function($scope, $state, $window, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){

	$scope.alerts = [];
	$scope.closeAlert = function(index){$scope.alerts.splice(index, 1)};

	// LOADIMAGE
	var loadImages = function(){
		dataFactory.getData({
			apiUrl: "/settings/sitelogo"
		},
		function(_rdata){

			$scope.projImg =_rdata.replace(/\\/g,'').replace(/"/g,'');
			console.log($scope.projImg)
		});
	}
	loadImages();

	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			dataFactory.getData({
				apiUrl: '/utility/publicationUploadImage/'+ rdata.config.file.name + '/Sample description'
			},
			function(_rdata){
				$scope.projImg = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
			});
		})
	}

	//Save New Logo
	$scope.savelogo=function(data){

		dataFactory.saveData({
			apiUrl:"/settings/savelogo"
		},{
			img:data
		},
		function(_rdata){
			if(_rdata.success){
				$scope.alerts.push({type: 'success', msg: 'New Logo Added!'});
				loadImages();
			}else{
				$scope.alerts.push({type: 'danger', msg: 'All fields is required.'});
			}
		});
	};







	$scope.data = {};
	var maintenance = function(data, status, headers, config) {
		$http({
			url: API_URL+"/settings/managesettings",
			method: "POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function (data, status, headers, config) {
			$scope.id = data.id;
			$scope.value1 = data.value1;
			$scope.value2 = data.value2;
			$scope.value3 = data.value3;

		}).error(function (data, status, headers, config) {

		});
	};
	maintenance();





	$scope.savesettings = function (on){
		$http({
			url: API_URL + "/settings/maintenanceon",
			method: "POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $.param(on)
		}).success(function (data, status, headers, config) {
			console.log('success');
			window.location.reload();
		}).error(function (data, status, headers, config) {

		});
	};

	$scope.savesettingsoff =function(){
		console.log("save");
		$http({
			url: API_URL + "/settings/maintenanceoff",
			method: "POST",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}

		}).success(function (data, status, headers, config) {
			console.log('success');
			window.location.reload();
		}).error(function (data, status, headers, config) {

		});
	};  


	//LOAD GOOGLE Analytics
	var loadscript = function(){
		$http({
			url: API_URL +"/settings/loadscript", 
			method: "GET",
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function (data) {
			console.log(data[0].script);
			$scope.dbscripts=data[0].script;
		}).error(function (data) {

		});
	}
	loadscript();

    //Google Analytics
    $scope.save= function(info){
    	$scope.alerts = [];
    	$scope.closeAlert = function (index) {
    		$scope.alerts.splice(index, 1);
    	};
    	$scope.isSaving = true;

    	$http({
    		url: API_URL + "/settings/googleanalytics",
    		method: "POST",
    		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    		data: $.param(info)
    	}).success(function (data, status, headers, config) {

    		if(data==0) {
    			$scope.alerts.push({type: 'danger', msg: 'Please Upload a photo for the submitted Publication'});
    			$scope.isSaving = false;
    		}
    		else{
    			$scope.isSaving = false;
    			$scope.alerts.push({type: 'success', msg: 'Google Analytics Script successfully saved!'});
    			$scope.form.$setPristine(true)
    		}

    	}).error(function (data, status, headers, config) {
    		scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
    	});
    }

    

})




