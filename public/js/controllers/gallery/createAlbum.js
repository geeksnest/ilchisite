'use strict';

 /*
 * 
 *▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
 * █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
 * █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
 *
 * Readble as possible Rock in Roll to the World
 * hang on and play the code 
 */
app.controller('imagegalleryCTRL', function($scope, $modal, Config, dataFactory){

	//GET THE  GENEREATED ALBUM ID
	var folderid=function(){
		dataFactory.getData({
			apiUrl: "/utility/generateid"
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			$scope.genId = _rdata;
		});
	}
	folderid();

	//LIST UPLOADED IMAGES
	var loadImage = function(id){
		dataFactory.getData({
			apiUrl:"/utility/listsliderimages/"+id
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			angular.forEach(_rdata, function(value, key) {
				_rdata[key].imgpath = Config.amazonlink+"/uploads/image/"+_rdata[key].imgpath; //Change C3 Full Path iMAge
			});
			$scope.data = _rdata;
		});
	}

	//CREATE NEW FOLDER
	var foldersave=function(albumName,albumId,description,date){
		dataFactory.getData({
			apiUrl:"/utility/createalbum/"+albumName+'/'+albumId+'/'+description+'/'+date
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
		});
	}

	//SAVE UPLOADED IMAGES
	var imageSave=function(filename, albumtitle, albumid, date){
		dataFactory.getData({
			apiUrl:'/utility/ajaxfileuploader/'+ filename+'/'+albumtitle+'/'+albumid+'/'+date
		},
		function(_rdata){
			//Beyond here Do Something about the result you passed
			//console.log(_rdata);
			loadImage(albumid);
		});
	}

	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	// UPLOAD IMAGE
	$scope.upload = function (files, gdata){
		dataFactory.imageUpload({
            dataFile:files, //Image or File to be upload
            apiUrl:"/pagestatic/saveimage", //Your API Url
            amazonS3:"uploads/image/" //Amazon S3 File path
        },
        function(loader){
            _imgLoader(loader.imageloader, loader.imagecontent) // Loader
        },
        function(rdata){
        	//Beyond here Do Something about the result you passed
        	//Console.log(rdata);
        	if(rdata.status == 204){
        		foldersave(gdata.title,gdata.folderid,gdata.desc,$('#date').val());	//Create new Album

        		imageSave(rdata.config.file.name, gdata.title, gdata.folderid, $('#date').val()); //Save Image
        	}else{
        		//Something went Wrong when uploading image
        	}

        })
	}

	//Update Uploaded Image Information 
	$scope.page = {};
	$scope.saveboxmessage = '';
	$scope.imgInfo = function (info){
		$('.buttonPanel'+info.id).hide();
		dataFactory.saveData({
			apiUrl:"/utility/imginfoupdate"
		},
		info,
		function(_rdata){
      		// Beyond here Do Something about the return data
      		$('.buttonPanel'+info.id).show();
      		$scope.saveboxmessage = info.id;
      	});
	};

	//DELETE Uploaded Image
	$scope.dltimg = function(imageid, folderid){
		var modalInstance = $modal.open({
			templateUrl: 'editMemberModal.html',
			controller: function ($scope, $modalInstance) {
				$scope.imageid = imageid;
				$scope.ok = function (imageid) {
					dataFactory.saveData({
						apiUrl:"/utility/dltphoto"
					},{
						id:imageid
					},
					function(_rdata){
						// Beyond here Do Something about the return data
						loadImage(folderid);
					});
				};
				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};
			}
		});
	}
})
