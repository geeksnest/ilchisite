'use strict';
/*
*▒█░▄▀ █░░█ █▀▀▄ █▀▀ █▀▀▄ 　 █▀▀▄ █▀▀ 　 █▀▀▀ █░░█ ░▀░ █▀▀█ 
* █▀▄░ █▄▄█ █▀▀▄ █▀▀ █░░█ 　 █░░█ █▀▀ 　 █░▀█ █░░█ ▀█▀ █▄▄█ 
* █░▒█ ▄▄▄█ ▀▀▀░ ▀▀▀ ▀░░▀ 　 ▀▀▀░ ▀▀▀ 　 ▀▀▀▀ ░▀▀▀ ▀▀▀ ▀░░▀ 	
*
* Readble as possible Rock in Roll to the World \m/(0_$)\m/
* hang on and play the code 
*/

app.controller('editPageCtrl', function($scope, $state, $window, Upload ,$q, $http,$stateParams , $modal, Config, dataFactory){


	//Convert TEXT TO SLUG
	$scope.onpagetitle = function convertToSlug(Text){
		$scope.page.slugs = angular.lowercase(Text.replace(/[^\w ]+/g,'').replace(/ +/g,'-'));
	}

	// LOAD GET DATA
	var loadGetData = function(id){

		dataFactory.getData({
			apiUrl: "/pages/editpage/"+id
		},
		function(_rdata){
			$scope.projImg = _rdata.pagebanner;
			$scope.page = _rdata;
		});

	}
	loadGetData($stateParams.pageid );


	//IMAGE UPLOAD LOADER
	var _imgLoader =function(imageloader, imagecontent){
		$scope.imageloader  = imageloader;
		$scope.imagecontent = imagecontent;
	}
	_imgLoader(false, true);

	//Upload image
	$scope.prepare=function(file){
		dataFactory.imageUpload({
		    dataFile:file, //Image or File to be upload
		    apiUrl:null, //Your API Url
		    amazonS3:"uploads/image/" //Amazon S3 File path
		},
		function(loader){
		   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
		},
		function(rdata){
			dataFactory.getData({
				apiUrl: '/pages/ajaxfileuploader/'+ rdata.config.file.name + '/New Image'
			},
			function(_rdata){
				$scope.projImg = Config.amazonlink+"/uploads/image/"+rdata.config.file.name;
			});
			
		})
	}

	var selctimg = function(img){
		$scope.projImg = img;
	}

	//MODAL MEDIA GALLERY
	$scope.mediaGallery =function(){
		var modalInstance = $modal.open({
			templateUrl: 'mediagallery.html',
			size:'lg',
			controller:function($scope, $modalInstance, Upload){

    			//LOAD UPLOADED IMAGES
    			var loadImages = function() {
    				dataFactory.getData({
    					apiUrl: "/pages/listpageimages"
    				},
    				function(_rdata){
    					angular.forEach(_rdata, function(value, key) {
							_rdata[key].imgpath = Config.amazonlink+"/uploads/image/"+_rdata[key].imgpath; //Change C3 Full Path iMAge
						});
    					console.log(_rdata);
    					$scope.data = _rdata;
    				});
    			}
    			loadImages();

    			//DELETE IMAGE
    			$scope.dltnewsimageClick = function(imageid) {
    				var modalInstance = $modal.open({
    					templateUrl: 'editMemberModal.html',
    					controller: function($scope, $modalInstance) {
    						$scope.imageid = imageid;
    						$scope.ok = function(imageid) {
    							dataFactory.saveData({
    								apiUrl:"/pages/dltpagephoto"
    							},
    							imageid,
    							function(_rdata){
    								loadImages();
    								$modalInstance.dismiss('cancel');
    							});
    						};
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			$scope.path = function(path) {
    				var modalInstance = $modal.open({
    					templateUrl: 'imgurl.html',
    					controller: function($scope, $modalInstance) {
    						$scope.imgpath = path;
    						$scope.cancel = function() {
    							$modalInstance.dismiss('cancel');
    						};
    					}
    				});
    			}

    			//IMAGE UPLOAD LOADER
    			var _imgLoader =function(imageloader, imagecontent){
    				$scope.imageloader  = imageloader;
    				$scope.imagecontent = imagecontent;
    			}
    			_imgLoader(false, true);

				//Upload image
				$scope.prepare=function(file){
					dataFactory.imageUpload({
					    dataFile:file, //Image or File to be upload
					    apiUrl:null, //Your API Url
					    amazonS3:"uploads/image/" //Amazon S3 File path
					},
					function(loader){
					   _imgLoader(loader.imageloader, loader.imagecontent) // Loader
					},
					function(rdata){
						dataFactory.getData({
							apiUrl: '/pages/ajaxfileuploader/'+ rdata.config.file.name + '/New Image'
						},
						function(_rdata){
							loadImages();
						});
					})
				}

				$scope.slctimage = function(image){
					selctimg(image);
					$modalInstance.dismiss('cancel');
				}

				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');
				};
			}
		});
	}
	



	// UPDATE DATA
	$scope.updatepage = function (page){
		$scope.alerts = [];
		$scope.closeAlert = function(index){$scope.alerts.splice(index, 1)};

		dataFactory.saveData({
			apiUrl:"/pages/updatepage"
		},
		page,
		function(_rdata){
			console.log(_rdata);
			if(_rdata.success){
				$scope.alerts.push({type: 'success', msg: 'Page successfully saved!'});
			}else{
				$scope.alerts.push({type: 'danger', msg: 'Something went wrong please check your fields'});
			}
		});
	};
})




