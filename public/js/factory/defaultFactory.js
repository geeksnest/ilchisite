app.factory('dataFactory', function($http, $q, Config, Upload){
	return {
		saveData: function(url, data, callback){
			$http({
				url: Config.ApiURL+""+url.apiUrl,
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			}).success(function (data, status, headers, config){
				callback(data);
			}).error(function (data, status, headers, config){
				callback(data);
			});
		},
		getData: function(url, callback){
			$http({
				url: Config.ApiURL+""+url.apiUrl,
				method: "GET",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function (data, status, headers, config){
				callback(data);
			});
		},
		imageUpload: function(data, imgLoader, callback){
			  var files = data.dataFile; 
			  var apiUrl = data.apiUrl;
			  var filename;
	          var filecount = 0;
	          if(files && files.length){
	          	imgLoader({imageloader:true,imagecontent:false});
	            for(var i=0;i < files.length;i++){
	              var file = files[i];
	              if(file.size >= 2000000){
	                $scope.alerts.push({type: 'danger', msg: 'File ' + file.name + ' is too big'});
	                filecount = filecount + 1;
	                if(filecount == files.length){
	                  imgLoader({imageloader:false,imagecontent:true});
	                }
	              }else{
	                var promises;
	                 promises = Upload.upload({
	                    url: Config.amazonlink, //S3 upload url including bucket name
	                    method: 'POST',
	                    transformRequest: function (data, headersGetter) {
	                      var headers = headersGetter();
	                      delete headers['Authorization'];
	                      return data;
	                    },
	                    fields : {
	                      key: data.amazonS3+''+ file.name, // the key to store the file on S3, could be file name or customized
	                      AWSAccessKeyId: Config.AWSAccessKeyId,
	                      acl: 'private', // sets the access to the uploaded file in the bucket: private or public
	                      policy: Config.policy, // base64-encoded json policy (see article below)
	                      signature: Config.signature, // base64-encoded signature based on policy string (see article below)
	                      "Content-Type": file.type != '' ? file.type : 'application/octet-stream' // content type of the file (NotEmpty)
	                    },
	                    file: file
	                })
	                promises.then(function(data){

	                	imgLoader({imageloader:false,imagecontent:true});
	                	callback(data);
	      //           	if(apiUrl!=null){
	      //           		$http({
							// url: Config.ApiURL+""+apiUrl,
							// method: "POST",
							// headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							// data: $.param({'imgfilename' : data.config.file.name})
							// }).success(function (data, status, headers, config){
							// 	imgLoader({imageloader:false,imagecontent:true});
							// 	callback(data);
							// }).error(function (data, status, headers, config){
							// 	imgLoader({imageloader:false,imagecontent:true});
							// 	callback(data);
							// });
	      //           	}else{
	      //           		callback(data);
	      //           	}
	                });
	              }
	            }
	        }
		}
	}
})