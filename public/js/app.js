'use strict';
// Declare app level module which depends on filters, and services

var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'app.filters',
    'app.services',
    'app.directives',
    'app.controllers',
    'angularFileUpload',
    'ngRoute',
    'ngSanitize',
    'checklist-model',
    'localytics.directives',
    'ngFileUpload'
    
    ])
.run(
  [          '$rootScope', '$state', '$stateParams','editableOptions',
  function ($rootScope,   $state,   $stateParams, editableOptions) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;    
    editableOptions.theme = 'bs3';
    $rootScope.count = 0;

}
]
)
.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$interpolateProvider','$parseProvider',
  function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide, $interpolateProvider, $parseProvider ) {

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $urlRouterProvider
        .otherwise('/dashboard');
        $stateProvider     
        .state('dashboard', {
            url: '/dashboard',
            controller: 'dashboardCtrl',
            templateUrl: '/ilchiadmin/admin/dashboard'
        })            
        .state('editprofile', {
            url: '/editprofile/:userid',
            controller: 'editprofileCtrl',
            templateUrl: '/ilchiadmin/admin/editprofile'
        })
        .state('viewuser', {
            url: '/viewuser/:userid',
                // controller: 'ViewUserCtrl',
                // templateUrl: '/ilchiadmin/admin/viewuser'
            })
        .state('userscreate', {
            url: '/userscreate',
            controller: 'CreateUserCtrl',
            templateUrl: '/ilchiadmin/users/create',
            resolve: {
                deps: ['uiLoad',
                function( uiLoad ){
                    return uiLoad.load( [
                        '/js/controllers/user/createUser.js',
                        '/js/jquery/jstz.js',
                        '/js/angular/moment.js',
                        '/js/angular/moment-timezone-all-years.js'
                        ]);
                }]
            }
        })
        .state('userlist', {
            url: '/userlist',
            controller: 'UserCtrl',
            templateUrl: '/ilchiadmin/users/list'
        })
        .state('edituser', {
            url: '/edituser/:userid',
            controller: 'edituserCtrl',
            templateUrl: '/ilchiadmin/users/edituser'
        })
        .state('members', {
            url: '/members',
            controller: 'MembersCtrl',
            templateUrl: '/ilchiadmin/users/members'
        })
        .state('donationlist', {
            url: '/donationlist',
            controller: 'DonationsCtrl',
            templateUrl: '/ilchiadmin/donation/donationlist'
        })

        .state('peacemarks', {
            url: '/peacemarks/:pageid',
            controller: 'GoogleMapCtrl',
            templateUrl: '/ilchiadmin/peacemark/create',
            resolve: {
                deps: ['uiLoad',
                function (uiLoad) {
                    return uiLoad.load(['/js/app/map/load-google-maps.js',
                        '/js/modules/ui-map.js',
                        '/js/app/map/map.js']).then(function () {
                            return loadGoogleMaps();
                        });
                    }]
                }
            })
        .state('editpeacemap', {
            url: '/peacemarks/edit/:id',
            templateUrl: '/ilchiadmin/peacemark/edit',
            resolve: {
                deps: ['uiLoad',
                function (uiLoad) {
                    return uiLoad.load(['/js/app/map/load-google-maps.js',
                        '/js/modules/ui-map.js',
                        '/js/app/map/map.js']).then(function () {
                            return loadGoogleMaps();
                        });
                    }]
                }
            })
        .state('managepeacemarks', {
            url: '/managepeacemarks',
            controller: 'GoogleMapCtrl',
            templateUrl: '/ilchiadmin/peacemark/managepeacemarks',
            resolve: {
                deps: ['uiLoad',
                function (uiLoad) {
                    return uiLoad.load(['/js/app/map/load-google-maps.js',
                        '/js/modules/ui-map.js',
                        '/js/app/map/map.js']).then(function () {
                            return loadGoogleMaps();
                        });
                    }]
                }
            })

            .state('createpage',{
                url: '/createpage',
                controller: 'CreatePageCtrl',
                templateUrl: '/ilchiadmin/pages/createpage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/pages/ceatePage.js',
                            ]);
                    }]
                }
            })

            .state('editpage',{
                url: '/editpage/:pageid',
                controller: 'editPageCtrl',
                templateUrl: '/ilchiadmin/pages/editpage',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/pages/editPage.js',
                            ]);
                    }]
                }
            })

            .state('managepages', {
                url: '/managepages',
                controller: 'ManageCtrl',
                templateUrl: '/ilchiadmin/pages/managepages'
            })
            .state('statpage', {
                url: '/statpage',
                // controller: 'ManageCtrl',
                templateUrl: '/ilchiadmin/pages/statlist'
            }) 
            .state('statpage.home', {
                url: '/home',
                 controller: 'HomeCtrl',
                templateUrl: '/ilchiadmin/pages/home',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/pagestatic/home.js',
                            ]);
                    }]
                }
            })
            .state('statpage.contact', {
                url: '/contact',
                controller: 'ContactCtrl',
                templateUrl: '/ilchiadmin/pages/contact',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/pagestatic/contact.js',
                            ]);
                    }]
                }
            })
            .state('statpage.terms', {
                url: '/terms',
                templateUrl: '/ilchiadmin/pages/terms',
                controller: 'TermCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/pagestatic/terms.js',
                            ]);
                    }]
                }
            })
            .state('statpage.medialinks', {
                url: '/medialinks',
                templateUrl: '/ilchiadmin/pages/medialinks',
                controller: 'MediaLinksCtrl',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/pagestatic/medialinks.js',
                            ]);
                    }]
                }

            })


           

            .state('createnews', {
                url: '/createnews',
                controller: 'CreateNewsCtrl',
                templateUrl: '/ilchiadmin/news/createnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/post/ceatePost.js'
                        ]);
                    }]
                }
            })



            

            .state('managenews', {
                url: '/managenews',
                controller: 'ManageNewsCtrl',
                templateUrl: '/ilchiadmin/news/managenews'
            })

            .state('editnews', {
                url: '/editnews/:newsid',
                controller: 'editNewsCtrl',
                templateUrl: '/ilchiadmin/news/editnews',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/post/editPost.js'
                        ]);
                    }]
                }
            })




            .state('tags', {
                url: '/tags/',
                controller: 'NewsTagsCtrl',
                templateUrl: '/ilchiadmin/news/tags'
            })
            .state('category', {
                url: '/category/',
                controller: 'NewscategoryCtrl',
                templateUrl: '/ilchiadmin/news/category'
            })
            .state('createnewsletter', {
                url: '/createnewsletter',
                controller: 'CreateNewsLetterCtrl',
                templateUrl: '/ilchiadmin/newsletter/createnewsletter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/newletter/createLetter.js'
                        ]);
                    }]
                }
            })
            .state('managenewsletter', {
                url: '/managenewsletter',
                controller: 'manageNewsLetterCtrl',
                templateUrl: '/ilchiadmin/newsletter/managenewsletter'
            })
            .state('editnewsletter', {
                url: '/editnewsletter/:newsletterid',
                controller: 'editNewsletterCtrl',
                templateUrl: '/ilchiadmin/newsletter/editnewsletter',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/newletter/editLetter.js'
                        ]);
                    }]
                }
            })
            .state('sendnewsletter', {
                url: '/sendnewsletter/:newsletterid',
                controller: 'sendNewsletterCtrl',
                templateUrl: '/ilchiadmin/newsletter/sendnewsletter'
            })
            .state('image', {
                url: '/image/',
                controller: 'imagegalleryCTRL',
                templateUrl: '/ilchiadmin/gallery/image',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/gallery/createAlbum.js'
                        ]);
                    }]
                }
            })
            .state('manage_album', {
                url: '/manage_album',
                controller: 'Manage_albumCtrl',
                templateUrl: '/ilchiadmin/gallery/manage_album'
            })
            .state('edit_album1', {
                url: '/edit_album/:id',
                controller: 'Edit_albumCtrl',
                templateUrl: '/ilchiadmin/gallery/edit_album',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/gallery/manageAlbum.js'
                        ]);
                    }]
                }
              

            })
            .state('forgotpassword', {
                url: '/forgotpassword',
                templateUrl: '/ilchiadmin/forgotpassword/index'

            })

            //end rainier state

            //jimmy state

            .state('addcalendar', {
                url: '/addcalendar',
                controller: 'addcalendarCtrl',
                templateUrl: '/ilchiadmin/calendar/addcalendar'
            })
            
            .state('viewcalendar', {
                url: '/viewcalendar',
                controller: 'viewcalendarCtrl',
                templateUrl: '/ilchiadmin/calendar/viewcalendar',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( ['/js/jquery/fullcalendar/fullcalendar.css',
                         '/js/jquery/jquery-ui-1.10.3.custom.min.js',
                         '/js/jquery/fullcalendar/fullcalendar.min.js',
                         '/js/modules/ui-calendar.js',
                         '/js/app/calendar/calendar.js']);
                    }]
                }
            })
            .state('editcalendar', {
                url: '/editcalendar/:pageid',
                controller: 'editcalendarCtrl',
                templateUrl: '/ilchiadmin/calendar/editcalendar'
            })

            .state('createfeaturedproject', {
                url: '/createfeaturedproject',
                controller: 'CreatefeaturedprojectCtrl',
                templateUrl: '/ilchiadmin/featuredprojects/createfeaturedproject',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/featured/createFeatured.js'
                        ]);
                    }]
                }
            })
            
            .state('managefeaturedproject', {
                url: '/managefeaturedproject',
                controller: 'ManagefeaturedprojectCtrl',
                templateUrl: '/ilchiadmin/featuredprojects/managefeaturedproject'
            })
            .state('editfeaturedproject', {
                url: '/editfeaturedproject/:pageid',
                controller: 'editfeatureCtrl',
                templateUrl: '/ilchiadmin/featuredprojects/editfeaturedproject',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/featured/editFeatured.js'
                        ]);
                    }]
                }
            })

            .state('addsubscriber', {
                url: '/addsubscriber',
                controller: 'AddsubscriberCtrl',
                templateUrl: '/ilchiadmin/subscribers/addsubscriber'
            })

            .state('subscriberslist', {
                url: '/subscriberslist',
                controller: 'SubscribersListCtrl',
                templateUrl: '/ilchiadmin/subscribers/subscriberslist'
            })

            .state('editsubscriber', {
                url: '/editsubscriber/:pageid',
                controller: 'EditsubscriberCtrl',
                templateUrl: '/ilchiadmin/subscribers/editsubscriber'
            })

            //end jimmy state

            //uson
            .state('createproposals', {
                url: '/createproposals',
                controller: 'ProposalsCtrl',
                templateUrl: '/ilchiadmin/proposals/createproposals'
            })

            .state('managecontacts', {
                url: '/managecontacts',
                controller: 'manageproposalsCtrl',
                templateUrl: '/ilchiadmin/contacts/managecontacts'
            })

            .state('managesettings', {
                url: '/managesettings',
                controller: 'settingsCtrl',
                templateUrl: '/ilchiadmin/settings/managesettings',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/setting/setting.js'
                        ]);
                    }]
                }
            })

            //end uson

            

            .state('sliderupload', {
                url: '/sliderupload',
                controller: 'SlideruploadCtrl',
                templateUrl: '/ilchiadmin/sliderimage/sliderupload'

            })



             ///////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////SLIDER HERE
            .state('menucreator', {
                url: '/menucreator',
                controller: 'menucreatorCtrl',
                templateUrl: '/ilchiadmin/menucreator/createMenu'

            })
            .state('createpublication', {
                url: '/createpublication',
                controller: 'createpubCtrl',
                templateUrl: '/ilchiadmin/publication/publication',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/publication/createPublication.js'
                        ]);
                    }]
                }

            })
            .state('managepublication', {
                url: '/managepublication',
                controller: 'managepubCtrl',
                templateUrl: '/ilchiadmin/publication/manage'

            })
            .state('editpub', {
                url: '/editpub/:id',
                controller: 'editpubCtrl',
                templateUrl: '/ilchiadmin/publication/editpub',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/publication/editPublication.js'
                        ]);
                    }]
                }

            })


            .state('sliderimage', {
                url: '/slider',
                controller: 'sliderCtrl',
                templateUrl: '/ilchiadmin/slider/slider',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/slider/createSlider.js'
                        ]);
                    }]
                }

            })
            .state('slider_list', {
                url: '/slider_list',
                controller: 'slider_listCtrl',
                templateUrl: '/ilchiadmin/slider/slider_lists'

            })
            .state('slideralbum', {
                url: '/slideralbum/:id',
                controller: 'sliderEditCtrl',
                templateUrl: '/ilchiadmin/slider/editalbum',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( [
                            '/js/controllers/slider/manageSlider.js'
                        ]);
                    }]
                }

            })


            .state('video', {
                url: '/video',
                controller: 'videoCtrl',
                templateUrl: '/ilchiadmin/video/videos'

            })
            .state('video_list', {
                url: '/video_list',
                controller: 'video_listCtrl',
                templateUrl: '/ilchiadmin/video/video_lists'

            })
            .state('edit', {
                url: '/edit/:id',
                controller: 'video_editCtrl',
                templateUrl: '/ilchiadmin/video/edit'

            })

            ///////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////            

            .state('app.ui.grid', {
                url: '/grid',
                templateUrl: 'tpl/ui_grid.html'
            })            
            .state('app.ui.bootstrap', {
                url: '/bootstrap',
                templateUrl: 'tpl/ui_bootstrap.html'
            })
            .state('app.ui.sortable', {
                url: '/sortable',
                templateUrl: 'tpl/ui_sortable.html'
            })
            .state('app.ui.portlet', {
                url: '/portlet',
                templateUrl: 'tpl/ui_portlet.html'
            })
            .state('app.ui.timeline', {
                url: '/timeline',
                templateUrl: 'tpl/ui_timeline.html'
            })
            .state('app.ui.jvectormap', {
                url: '/jvectormap',
                templateUrl: 'tpl/ui_jvectormap.html'
            })
            .state('app.ui.googlemap', {
                url: '/googlemap',
                templateUrl: 'tpl/ui_googlemap.html',
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( ['js/app/map/load-google-maps.js',
                            'js/modules/ui-map.js',
                            'js/app/map/map.js'] ).then(function(){ return loadGoogleMaps(); });
                    }]
                }
            })
            .state('app.widgets', {
                url: '/widgets',
                templateUrl: 'tpl/ui_widgets.html'
            })
            .state('app.chart', {
                url: '/chart',
                templateUrl: 'tpl/ui_chart.html'
            })
            // table
            .state('app.table', {
                url: '/table',
                template: '<div ui-view></div>'
            })
            .state('app.table.static', {
                url: '/static',
                templateUrl: 'tpl/table_static.html'
            })
            .state('app.table.datatable', {
                url: '/datatable',
                templateUrl: 'tpl/table_datatable.html'
            })
            .state('app.table.footable', {
                url: '/footable',
                templateUrl: 'tpl/table_footable.html'
            })
            // form
            .state('app.form', {
                url: '/form',
                template: '<div ui-view class="fade-in"></div>'
            })
            .state('app.form.elements', {
                url: '/elements',
                templateUrl: 'tpl/form_elements.html'
            })
            .state('app.form.validation', {
                url: '/validation',
                templateUrl: 'tpl/form_validation.html'
            })
            .state('app.form.wizard', {
                url: '/wizard',
                templateUrl: 'tpl/form_wizard.html'
            })
            // pages
            .state('app.page', {
                url: '/page',
                template: '<div ui-view class="fade-in-down"></div>'
            })
            .state('app.page.profile', {
                url: '/profile',
                templateUrl: 'tpl/page_profile.html'
            })
            .state('app.page.post', {
                url: '/post',
                templateUrl: 'tpl/page_post.html'
            })
            .state('app.page.search', {
                url: '/search',
                templateUrl: 'tpl/page_search.html'
            })
            .state('app.page.invoice', {
                url: '/invoice',
                templateUrl: 'tpl/page_invoice.html'
            })
            .state('app.page.price', {
                url: '/price',
                templateUrl: 'tpl/page_price.html'
            })
            .state('app.docs', {
                url: '/docs',
                templateUrl: 'tpl/docs.html'
            })
            // others
            .state('lockme', {
                url: '/lockme',
                templateUrl: 'tpl/page_lockme.html'
            })
            .state('access', {
                url: '/access',
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
            })
            .state('access.signin', {
                url: '/signin',
                templateUrl: 'tpl/page_signin.html'
            })
            .state('access.signup', {
                url: '/signup',
                templateUrl: 'tpl/page_signup.html'
            })
            .state('access.forgotpwd', {
                url: '/forgotpwd',
                templateUrl: 'tpl/page_forgotpwd.html'
            })
            .state('access.404', {
                url: '/404',
                templateUrl: 'tpl/page_404.html'
            })            

            // fullCalendar
            .state('app.calendar', {
                url: '/calendar',
                templateUrl: 'tpl/app_calendar.html',
                // use resolve to load other dependences
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( ['js/jquery/fullcalendar/fullcalendar.css',
                         'js/jquery/jquery-ui-1.10.3.custom.min.js',
                         'js/jquery/fullcalendar/fullcalendar.min.js',
                         'js/modules/ui-calendar.js',
                         'js/app/calendar/calendar.js']);
                    }]
                }
            })

            // mail
            .state('app.mail', {
                abstract: true,
                url: '/mail',
                templateUrl: 'tpl/mail.html',
                // use resolve to load other dependences
                resolve: {
                    deps: ['uiLoad',
                    function( uiLoad ){
                        return uiLoad.load( ['/js/app/mail/mail.js',
                         '/js/app/mail/mail-service.js',
                         '/js/libs/moment.min.js']);
                    }]
                }
            })
            .state('app.mail.list', {
                url: '/inbox/{fold}',
                templateUrl: 'tpl/mail.list.html'
            })
            .state('app.mail.detail', {
                url: '/{mailId:[0-9]{1,4}}',
                templateUrl: 'tpl/mail.detail.html'
            })
            .state('app.mail.compose', {
                url: '/compose',
                templateUrl: 'tpl/mail.new.html'
            })
        }
        ]
        )

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: '/js/jsons/',
    suffix: '.json'
});

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
 .constant('JQ_CONFIG', {
    easyPieChart:   ['../js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['../js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['../js/jquery/charts/flot/jquery.flot.min.js', 
    '../js/jquery/charts/flot/jquery.flot.resize.js',
    '../js/jquery/charts/flot/jquery.flot.tooltip.min.js',
    '../js/jquery/charts/flot/jquery.flot.spline.js',
    '../js/jquery/charts/flot/jquery.flot.orderBars.js',
    '../js/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['../js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['../js/jquery/sortable/jquery.sortable.js'],
    nestable:       ['../js/jquery/nestable/jquery.nestable.js',
    '../js/jquery/nestable/nestable.css'],
    filestyle:      ['../js/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['../js/jquery/slider/bootstrap-slider.js',
    '../js/jquery/slider/slider.css'],
    chosen:         ['../js/jquery/chosen/chosen.jquery.min.js',
    '../js/jquery/chosen/chosen.css'],
    TouchSpin:      ['../js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
    '../js/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['../js/jquery/wysiwyg/bootstrap-wysiwyg.js',
    '../js/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['../js/jquery/datatables/jquery.dataTables.min.js',
    '../js/jquery/datatables/dataTables.bootstrap.js',
    '../js/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['../js/jquery/jvectormap/jquery-jvectormap.min.js', 
    '../js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
    '../js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
    '../js/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['../js/jquery/footable/footable.all.min.js',
    '../js/jquery/footable/footable.core.css']
}
)


.constant('MODULE_CONFIG', {
    select2:        ['../js/jquery/select2/select2.css',
    '../js/jquery/select2/select2-bootstrap.css',
    '../js/jquery/select2/select2.min.js',
    '../js/modules/ui-select2.js']
}

)

