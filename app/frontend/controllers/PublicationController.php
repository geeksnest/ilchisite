<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;
// use Modules\Frontend\Models\Publication as Publication;

class PublicationController extends ControllerBase {


    public function indexAction($pubcont) {  

        // $pub = Publication::find("category = '". $pubcont ."'");
        // $countpub = count($pub);
        // if ($countpub == 0) {
        //     return $this->response->redirect($this->config->application->baseURL . '/page/index/404-page');
        // }  

        // featured project info
      $service_url = $this->config->application->apiURL.'/utility/publication';

      $curl = curl_init($service_url);

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $curl_response = curl_exec($curl);

      if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additional info: ' . var_export($info));
    }

    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->viewinfo = $decoded;
    $this->view->pubcont = $pubcont;
    $this->view->pageid = 301;



    if($pubcont == 1)
    {
        $type = "Books";
        $this->view->activeid = 14;
    }
    else if($pubcont == 2)
    {
        $type = "CDs";
        $this->view->activeid = 15;

    }
    else if($pubcont == 3)
    {
        $type = "Related Products";
        $this->view->activeid = 16;

    }

    $this->view->metatitle   ="Ilchi Lee  - Publication ".$type;
    $this->view->metadesc    ="Ilchi Lee  - Publication ".$type;
    $this->view->metakeyword ="Ilchi Lee  - Publication ".$type;


}
public function pubAction() {    

        // featured project info
   $service_url = $this->config->application->apiURL.'/utility/publication';

   $curl = curl_init($service_url);

   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

   $curl_response = curl_exec($curl);

   if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additional info: ' . var_export($info));
}

curl_close($curl);
$decoded = json_decode($curl_response);
$this->view->viewinfo = $decoded;
$this->view->pageid = 301;

$this->view->activeid = 17;

}






}
