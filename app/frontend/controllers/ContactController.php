<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;
// use Modules\Frontend\Models\Featuredprojects as Featuredprojects;


class ContactController extends ControllerBase {

	public function indexAction() {

	}

	public function contactusAction() {

		$this->view->pageid 			= 501;
		$this->view->activeid 			= 22;

		$this->view->contact 			= $this->curl('pagestatic/contactGet')->body;

		$this->view->metatitle   		= "Ilchi Lee  - Contact Us ";
		$this->view->metadesc    		= "Ilchi Lee  - Contact Us ";
		$this->view->metakeyword 		= "Ilchi Lee  - Contact Us ";
	}

	public function termsandconditionsAction() {
		$this->view->pageid 			= 501;
		$this->view->activeid 			= 24;
		$this->view->terms 				= $this->curl('pagestatic/termGet')->body;

		$this->view->metatitle   		= "Ilchi Lee  - Terms & Condition";
		$this->view->metadesc    		= "Ilchi Lee  - Terms & Condition";
		$this->view->metakeyword 		= "Ilchi Lee  - Terms & Condition";
	}

	public function curl($url){
		$service_url 					= $this->config->application->apiURL.'/'.$url;
		$curl 							= curl_init($service_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response 					= curl_exec($curl);
		if($curl_response === false) {
			$info 						= curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additional info: ' . var_export($info));
		}
		curl_close($curl);
		return $decoded 				= json_decode($curl_response);
	} 
}