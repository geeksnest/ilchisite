<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;



class GalleryController extends ControllerBase {

    public function indexAction() {

       $service_url = $this->config->application->apiURL.'/album/album/0';

       $curl = curl_init($service_url);

       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

       $curl_response = curl_exec($curl);

       if ($curl_response === false) 
       {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additional info: ' . var_export($info));
    }

    curl_close($curl);
    $decoded = json_decode($curl_response);
    $this->view->albuminfo = $decoded;
    $this->view->pageid = 1;
    $this->view->activeid = 2;


    $this->view->metatitle   ="Meet Ilchi  - Gallery";
    $this->view->metadesc    ="Meet Ilchi  - Gallery";
    $this->view->metakeyword ="Meet Ilchi  - Gallery"; 

    


}

public function folderAction($id) {

    if($this->curl('/validate/album/'.$id)){
        return $this->response->redirect($this->config->application->baseURL . '/page/index/404-page');
    }

    $decoded = $this->curl('/gallery/folder/'. $id);
    $this->view->folderinfo = $decoded;
    $this->view->pageid = 1;
    $this->view->activeid = 2;

    $this->view->metatitle   ="Meet Ilchi  - Gallery";
    $this->view->metadesc    ="Meet Ilchi  - Gallery";
    $this->view->metakeyword ="Meet Ilchi  - Gallery"; 
}
}