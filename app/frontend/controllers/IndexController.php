<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;

class IndexController extends ControllerBase {

  
    public function indexAction() { 

        $this->view->aboutIlchi = $this->curl('pagestatic/aboutGet')->body;
        $this->view->socialLink = json_decode($this->curl('pagestatic/socialLinksGet')->body)->_socialLinks;
        $this->view->mediaTitle = json_decode($this->curl('pagestatic/medialTitleGet')->body)->_mediaTitle;

        if ($this->curl('/site/maintenace')) {
           return $this->response->redirect($this->config->application->baseURL . '/maintenance');
           $this->view->message = $msg ;
        }
        $this->view->path = $this->curl('/site/logo');


        

        $this->view->msginfo =$this->curl('/utility/sliderbanner');
        $this->view->projectinfo = $this->curl('/project/project/0');
        $data = $this->curl('/project/featuredproject');
        $this->view->feat_id = $data->feat_id;
        $this->view->feat_title = $data->feat_title;
        $this->view->feat_content = $data->feat_content;
        $this->view->feat_picpath = $data->feat_picpath;
        $this->view->feat_date = $data->feat_date;    
        $this->view->postinfo = $this->curl('/post/latestpost/0'); 
        $this->view->latestvid = $this->curl('/utility/latestvid');



         $service_url_news = $this->config->application->apiURL. '/post/journal/0';

        
        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->postjournal = $decoded; 


       
        $service_url_news = $this->config->application->apiURL. '/settings/script';

        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->script = $decoded[0]->script;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}




    public function errorAction() { 
        return $this->response->redirect($this->config->application->baseURL . '/page/index/404-page');

     }
}
