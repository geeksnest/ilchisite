<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;

class PostController extends ControllerBase{

    public function indexAction(){

        $this->view->popularpost        = $this->curl('post/popularpost/0');
        $this->view->postinfo           = $this->curl('post/post/0');
        $this->view->taginfo            = $this->curl('post/showtags');

        $this->view->pageid             = 401;
        $this->view->activeid           = 17;
        $this->view->metatitle          = "Ilchi Lee  - Latest Posts ";
        $this->view->metadesc           = "Ilchi Lee  - Latest Posts ";
        $this->view->metakeyword        = "Ilchi Lee  - Latest Posts ";
    }

    public function categoryAction($category){

        $this->view->popularpost        = $this->curl('post/popularpost/0');
        $this->view->postinfo           = $this->curl('post/category/0/'.$category);
        $this->view->taginfo            = $this->curl('post/showtags');

        $this->view->categorytitle      = $category;

        $this->view->pageid             = 401;
        $this->view->activeid           = array('Journal' => 18 , 'News' => 19, 'Video' => 20)[$category];

        $this->view->metatitle          = "Ilchi Lee  -  Posts ".$category;
        $this->view->metadesc           = "Ilchi Lee  -  Posts ".$category;
        $this->view->metakeyword        = "Ilchi Lee  -  Posts ".$category;
    }

    public function viewAction($category,$newsslugs){

        if($this->curl('/newsslug/check/'.$newsslugs)){
            return $this->response->redirect($this->config->application->baseURL .'/page/index/404-page');
        }

        $this->view->popularpost        = $this->curl('post/popularpost/0');
        $this->view->postinfo           = $this->curl('post/post/0');
        $this->view->taginfo            = $this->curl('post/showtags');

        $this->view->postinforelated    = $this->curl('post/categoryrelated/0/'.$category.'/'.$newsslugs);

        $decoded                        = $this->curl('news/fullnews/'. $newsslugs);
        $this->view->newsid             = $decoded->newsid;
        $this->view->title              = $decoded->title;
        $this->view->newsslugs          = $decoded->newsslugs;
        $this->view->author             = $decoded->author;
        $this->view->body               = $decoded->body;
        $this->view->banner             = $decoded->banner;
        $this->view->category           = $decoded->category;
        $this->view->date               = $decoded->date;
        $this->view->catdata            = $decoded->catdata;

        $this->view->metatitle          = $decoded->metatitle;
        $this->view->metadesc           = $decoded->metadesc;
        $this->view->metakeyword        = @$decoded->metakeyword;

        $this->view->pageid             = 401; 
        $this->view->activeid           = 401;

    }

    public function tagsAction($tagsname,$tags){
        $this->view->relatedTags        = $this->curl( 'post/relatedtags/'.$tags);

        $this->view->postinfo           = $this->curl('post/post/0');
        $this->view->taginfo            = $this->curl('post/showtags');

        $this->view->categorytitle      = $tagsname;

        $this->view->pageid             = 401;
        $this->view->activeid           = 401;

        $this->view->metatitle          = "Ilchi Lee  -  Posts Tags ".$tagsname;
        $this->view->metadesc           = "Ilchi Lee  -  Posts Tags ".$tagsname;
        $this->view->metakeyword        = "Ilchi Lee  -  Posts Tags ".$tagsname;
    }
}