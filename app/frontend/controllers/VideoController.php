<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;
// use Modules\Frontend\Models\Video as Video;

class VideoController extends ControllerBase {

  
    public function indexAction() {    

        // featured project info
    	 $service_url = $this->config->application->apiURL.'/utility/videodisp/0';
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->msginfo = $decoded;


         // featured project info
         $service_url = $this->config->application->apiURL.'/utility/featuredvid';
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->featuredvid = $decoded;
        $this->view->pageid =1;
        $this->view->activeid = 5;


        $this->view->metatitle   ="Meet Ilchi  - Featured Videos";
        $this->view->metadesc    ="Meet Ilchi  - Featured Videos";
        $this->view->metakeyword ="Meet Ilchi  - Featured Videos"; 

	}
	 public function readaboutAction($slugs) {  

        // $video = Video::find("slugs = '". $slugs. "'");
        //  $countvideo = count($video);
        //  if ($countvideo == 0) {
        //     return $this->response->redirect($this->config->application->baseURL . '/page/index/404-page');
        //  }

        // featured project info
        $service_url = $this->config->application->apiURL.'/utility/videoshow/'.$slugs;
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);

        // var_dump($service_url);
        // die();

        $this->view->msginfo = $decoded;
        // var_dump($decoded);
        $this->view->metatitle   ="Meet Ilchi  - Featured Videos";
        $this->view->metadesc    ="Meet Ilchi  - Featured Videos";
        $this->view->metakeyword ="Meet Ilchi  - Featured Videos"; 




        $service_url1 = $this->config->application->apiURL.'/utility/videodisp/0';

         $curl1 = curl_init($service_url1);

         curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);

         $curl_response1 = curl_exec($curl1);

         if ($curl_response1 === false) {
            $info1 = curl_getinfo($curl);
            curl_close($curl1);
            die('error occured during curl exec. Additional info: ' . var_export($info1));
        }
        
        curl_close($curl1);
        $decoded1 = json_decode($curl_response1);
        // var_dump($decoded1);
        $this->view->msginfo1 = $decoded1;
        $this->view->pageid =1;
        $this->view->activeid = 5;


        $this->view->metatitle   ="Meet Ilchi  - Featured Videos";
        $this->view->metadesc    ="Meet Ilchi  - Featured Videos";
        $this->view->metakeyword ="Meet Ilchi  - Featured Videos"; 

	}
     public function showmoreAction($album) {   
        $service_url = $this->config->application->apiURL.'/utility/showmore/'.$album;
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
         $this->view->showmore = $decoded;
         $this->view->album = $album;

         
         $this->view->pageid =1;
        $this->view->activeid = 5;


        $this->view->metatitle   ="Meet Ilchi  - Featured Videos";
        $this->view->metadesc    ="Meet Ilchi  - Featured Videos";
        $this->view->metakeyword ="Meet Ilchi  - Featured Videos"; 
        
    }
     public function morefeaturedAction($album) { 

         $service_url = $this->config->application->apiURL.'/utility/featuredvid';
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->featuredvid = $decoded;
        $this->view->pageid =1;
        $this->view->activeid = 5;


        $this->view->metatitle   ="Meet Ilchi  - Featured Videos";
        $this->view->metadesc    ="Meet Ilchi  - Featured Videos";
        $this->view->metakeyword ="Meet Ilchi  - Featured Videos";  
    }
}
