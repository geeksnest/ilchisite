<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;
// use Modules\Frontend\Models\Featuredprojects as Featuredprojects;


class NewsletterController extends ControllerBase {

	public function indexAction() {

		$service_url_news = $this->config->application->apiURL. '/newsletter/newsletter/0';


		$curl = curl_init($service_url_news);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$curl_response = curl_exec($curl);

		if ($curl_response === false) 
		{
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additional info: ' . var_export($info));
		}

		curl_close($curl);
		$decoded = json_decode($curl_response);
		$this->view->newsletterinfo = $decoded;

		$this->view->pageid = 404;
		$this->view->activeid = 404;


	}

	public function viewAction($newsletterid) {

		$service_url = $this->config->application->apiURL. '/newsletter/fullletter/' . $newsletterid;

		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		curl_close($curl);
		$decoded = json_decode($curl_response);


        //var_dump($decoded);
		$this->view->newsletterid = $decoded->newsletterid;
		$this->view->title = $decoded->title;
		$this->view->body = $decoded->body;
		$this->view->banner = $decoded->banner;
		$this->view->date = $decoded->date;

		$this->view->pageid = 404;
		$this->view->activeid = 404;


	}



}