<?php


namespace Modules\Frontend\Controllers;
use \Phalcon\Mvc\View;


class SearchController extends ControllerBase {

    public function indexAction($keyword,$offset) {

        $service_url = $this->config->application->apiURL.'/search/search/'.$keyword.'/'.$offset;

        $curl = curl_init($service_url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->searchresult = $decoded;


        $service_url = $this->config->application->apiURL.'/page/page/'.$keyword.'/'.$offset;

        $curl = curl_init($service_url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->pageresult = $decoded;


        $this->view->pageid = 404;
        $this->view->keyword = $keyword;
        $this->view->offset = $offset;

         $this->view->activeid = 404;


    }

    
}