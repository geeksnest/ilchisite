<?php

namespace Modules\Frontend\Controllers;

use Phalcon\Mvc\View;


class ControllerBase extends \Phalcon\Mvc\Controller
{


    public function curl($url){
        $service_url = $this->config->application->apiURL.'/'.$url;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
    }




    protected function initialize(){
        $this->view->base_url = $this->config->application->baseURL;
        $this->view->api_url = $this->config->application->apiURL;

        $clientip= $this->get_client_ip_server();   

        $countvisit =$this->config->application->apiURL.'/getip/visit/'. $clientip; 
        $curl = curl_init($countvisit);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            var_dump(die('error occured during curl exec. Additional info: ' . var_export($info))) ;
        }




          $this->view->path = $this->curl('/site/logo');
          $this->view->script_google = @$this->curl('/settings/script')[0]->script;
          $this->view->activeid = 0;



     //      $logo = Sitelogo::findFirst("id=1");
     //    if ($logo->id == 1) {

     //     $this->view->path = $logo->path;
     //    // $this->view->message = $msg ;


     //    $service_url_news = $this->config->application->apiURL. '/settings/script';

     //    $curl = curl_init($service_url_news);
        
     //    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

     //    $curl_response = curl_exec($curl);
        
     //    if ($curl_response === false) 
     //    {
     //        $info = curl_getinfo($curl);
     //        curl_close($curl);
     //        die('error occured during curl exec. Additional info: ' . var_export($info));
     //    }
        
     //    curl_close($curl);
     //    $decoded = json_decode($curl_response);
      
     //    $this->view->script_google = $decoded[0]->script;
     //     $this->view->activeid = 0;



     // }


    }
    public  function get_client_ip_server() {

    	if (isset($_SERVER)) {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];

        return $_SERVER["REMOTE_ADDR"];
    }

    if (getenv('HTTP_X_FORWARDED_FOR'))
        return getenv('HTTP_X_FORWARDED_FOR');

    if (getenv('HTTP_CLIENT_IP'))
        return getenv('HTTP_CLIENT_IP');

    return getenv('REMOTE_ADDR');
    }

    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }
    
    public function getConfig(){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->config;
    }
}