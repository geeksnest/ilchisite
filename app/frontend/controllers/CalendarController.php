<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;


class CalendarController extends ControllerBase {

    public function indexAction() {
        // activity lists -------------------------------------------------------------
    	$service_url = $this->config->application->apiURL.'/calendar/listview';

        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->viewcalendar = $decoded;
        $this->view->disonli = 1;
        $this->view->pageid = 1;

        $this->view->activeid = 4;


        $this->view->metatitle   ="Meet Ilchi  - Callender of Activity";
        $this->view->metadesc    ="Meet Ilchi  - Callender of Activity";
        $this->view->metakeyword ="Meet Ilchi  - Callender of Activity"; 


    }

    public function activityinfoAction($actid) {

        // $calendar = Calendar::find("actid = ". $actid );
        // $countcalendar = count($calendar);
        // if ($countcalendar == 0) {
        //     return $this->response->redirect($this->config->application->baseURL . '/page/index/404-page');
        // }

        $service_url = $this->config->application->apiURL.'/calendar/editactivity/' . $actid;
        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        
        $this->view->actid = $decoded->actid;
        $this->view->acttitle = $decoded->title;
        $this->view->actloc = $decoded->loc;
        $this->view->df = $decoded->dfrom;
        $this->view->dt = $decoded->dto;
        $this->view->mt = $decoded->mytime2;
        $this->view->actdesc = $decoded->body;
        $this->view->actinfo = $decoded->shortdesc;
        $this->view->actbanner = $decoded->banner;
        $this->view->disonli = 2;
        $this->view->pageid = 1;
        $this->view->activeid = 4;



        $this->view->metatitle   ="Meet Ilchi  - Callender of Activity";
        $this->view->metadesc    ="Meet Ilchi  - Callender of Activity";
        $this->view->metakeyword ="Meet Ilchi  - Callender of Activity"; 

    }
}