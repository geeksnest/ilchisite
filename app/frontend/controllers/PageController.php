<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;
// use Modules\Frontend\Models\Featuredprojects as Featuredprojects;


class PageController extends ControllerBase {

    public function indexAction($pageslugs) {

         if ($this->curl('/site/maintenace')) {
           return $this->response->redirect($this->config->application->baseURL . '/maintenance');
           $this->view->message = $msg ;
    }

    if ($this->curl('/pages/page404/'.$pageslugs)) {
        return $this->response->redirect($this->config->application->baseURL . '/page/index/404-page');
    }



     $service_url = $this->config->application->apiURL.'/pages/getpage/' . $pageslugs;
     $curl = curl_init($service_url);
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     $curl_response = curl_exec($curl);
     if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additioanl info: ' . var_export($info));
    }
    curl_close($curl);
    $decoded = json_decode($curl_response);

    $this->view->title = $decoded->title;
    $this->view->pageid = $decoded->pageid;
    $this->view->pageslugs = $decoded->pageslugs;
    $this->view->pagefeatures = $decoded->pagefeatures;
    $this->view->body = $decoded->body;

    $this->view->metatitle = $decoded->metatitle;
    $this->view->metadesc = $decoded->metadesc;
    $this->view->metakeyword = $decoded->metakeyword;

    

    if($pageslugs == 'Who-is-Ilchi-Lee')
    {
        $this->view->activeid = 1;
    }
    else if($pageslugs == 'Ilchi-Lees-Timeline')
    {
        $this->view->activeid = 3;

    }
    else if($pageslugs == 'Self-Development-Guidelines')
    {
        $this->view->activeid = 6;

    }
    else if($pageslugs == 'Awakened-Brain')
    {
        $this->view->activeid = 7;

    }
    else if($pageslugs == 'Energy-Principle')
    {
        $this->view->activeid = 8;

    }
    else if($pageslugs == 'Earth-Citizenship')
    {
        $this->view->activeid = 9;

    }
    else if($pageslugs == 'Korean-Heritage')
    {
        $this->view->activeid = 10;

    }
    else if($pageslugs == 'Projects')
    {
        $this->view->activeid = 122222;

    }
    else if($pageslugs == 'Media')
    {
        $this->view->activeid = 11;

    }
    else if($pageslugs == 'Movements')
    {
        $this->view->activeid = 12;

    }
    else if($pageslugs == 'Education')
    {
        $this->view->activeid = 13;

    }
    else if($pageslugs == 'The-Solar-Body-for-Natural-Health')
    {
        $this->view->activeid = 11;

    }
    else if($pageslugs == 'Bird-of-the-Soul')
    {
        $this->view->activeid = 11;

    }
    else if($pageslugs == 'The-Change-Project')
    {
        $this->view->activeid = 11;

    }
    else if($pageslugs == 'Earth-Citizens-Movement')
    {
        $this->view->activeid = 12;

    }
    else if($pageslugs == 'The-Call-of-Sedona')
    {
        $this->view->activeid = 12;

    }
    else if($pageslugs == 'Reviving-Ancient-Wisdom')
    {
        $this->view->activeid = 12;

    }
    else if($pageslugs == 'Prayer-of-Peace')
    {
        $this->view->activeid = 12;

    }
    else if($pageslugs == 'Educating-the-Brain')
    {
        $this->view->activeid = 13;

    }
    else if($pageslugs == 'Body-and-Brain-Programs-and-Centers')
    {
        $this->view->activeid = 13;

    }
    else if($pageslugs == 'Principles')
    {
        $this->view->activeid = 404;

    }
    
    else if($pageslugs == '404-page')
    {
        $this->view->activeid = 404;

    }



      /////////////////////////////////////////////////////////////////////////////////


        $service_url_news = $this->config->application->apiURL. '/project/project/0';

        
        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->projectinfo = $decoded;

        /////////////////////////////////////////////////////////////////////////////////

        $service_url = $this->config->application->apiURL. '/proj/featproject';
        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

		// $this->view->id = $decoded->id;
		// $this->view->title = $decoded->title;
		// $this->view->picpath = $decoded->picpath;
		// $this->view->date = $decoded->date;


        $service_url_news = $this->config->application->apiURL. '/post/latestpost/0';

        
        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->postinfo = $decoded; 

        /////////////////////////////////////////////////////////////////////////////////

        $service_url_news = $this->config->application->apiURL. '/post/journal/0';
        $curl = curl_init($service_url_news);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) 
        {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        //var_dump($decoded);
        $decoded = json_decode($curl_response);
        $this->view->postjournal = $decoded;    

    
    
}
}