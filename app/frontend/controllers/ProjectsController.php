<?php

use \Phalcon\Mvc\View;

namespace Modules\Frontend\Controllers;
// use Modules\Frontend\Models\Featuredprojects as Featuredprojects;


class ProjectsController extends ControllerBase {

    public function indexAction($feat_id) {



        $service_url = $this->config->application->apiURL. '/project/fullproject/' .$feat_id;
        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        

        //var_dump($decoded);
        $this->view->feat_id = $decoded->feat_id;
        $this->view->feat_title = $decoded->feat_title;
        $this->view->feat_content = $decoded->feat_content;
        $this->view->feat_picpath = $decoded->feat_picpath;
        $this->view->feat_date = $decoded->feat_date;

        $this->view->pageid = 222;
        $this->view->activeid = 222;

    }


}