<?php

namespace Modules\Frontend\Controllers;
use Phalcon\Mvc\View;


class PagenotfoundController extends ControllerBase {


    public function indexAction() {


        if ($this->curl('/site/maintenace')) {
           return $this->response->redirect( $this->view->base_url );
        }


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    } 



}