<?php

namespace Modules\Frontend;

use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory;


            use \Phalcon\Mvc\Dispatcher as PhDispatcher;


class Module {

    public function registerAutoloaders() {

        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces(array(
            'Modules\Frontend\Controllers' => __DIR__ . '/controllers/',
            'Modules\Frontend\Models' => __DIR__ . '/models/',
            'Modules\Frontend\Compoments' => __DIR__ . '/components/',
        ));

        $loader->register();
    }

    public function registerServices($di) {
        /**
         * Read configuration
         */
        $config = include __DIR__ . "/config/config.php";

        $di['dispatcher'] = function() use ($di) {

           $evManager = $di->getShared('eventsManager');

           $evManager->attach(
            "dispatch:beforeException",
            function($event, $dispatcher, $exception)
            {
                switch ($exception->getCode()) {
                    case PhDispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case PhDispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward(
                        array(
                            'controller' => 'index',
                            'action'     => 'error',
                            )
                        );
                    return false;
                }
            }
            );
           $dispatcher = new PhDispatcher();
           $dispatcher->setEventsManager($evManager);



           $dispatcher->setDefaultNamespace("Modules\Frontend\Controllers");
           return $dispatcher;
       };

        /**
         * Setting up the view component
         */
        $di['view'] = function() {
            $view = new \Phalcon\Mvc\View();

            $view->setViewsDir(__DIR__ . '/views/');
            $view->setLayoutsDir('layouts/');
            $view->setTemplateAfter('main');

            $view->registerEngines(array(
                '.volt' => function ($view, $di) {

                    $volt = new VoltEngine($view, $di);

                    $volt->setOptions(array(
                        'compiledPath' => __DIR__ . '/cache/',
                        'compiledSeparator' => '_'
                    ));

                    return $volt;
                },
                        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
                    ));

                    return $view;
                };

                /**
                 * Database connection is created based in the parameters defined in the configuration file
                 */
                $di['db'] = function() use ($config) {
                    return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
                        "host" => $config->database->host,
                        "username" => $config->database->username,
                        "password" => $config->database->password,
                        "dbname" => $config->database->name
                    ));
                };
                /**
                 * If the configuration specify the use of metadata adapter use it or use memory otherwise
                 */
                $di['modelsMetadata'] = function () {
                    return new \Phalcon\Mvc\Model\Metadata\Memory();
                };
                /**
                 * ModelsManager
                 */
                $di['modelsManager'] = function() {
                    return new \Phalcon\Mvc\Model\Manager();
                };

        }
}