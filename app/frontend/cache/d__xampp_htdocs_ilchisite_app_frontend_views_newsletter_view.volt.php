<?php echo $this->getContent()?>
<div style="overflow:hidden;">
<div class="content contact" ng-controller="contacts">
  <div class="container">
    <div class="row">
     <div class="col-md-8">
       <div class="h1title">E-Newsletter Archive</div>
       <br>
       <br>

                    <div style="overflow:hidden;">
       
                    <h1><?php echo $title ?></h1>
                    <p style="color: #999;"><b><i><?php

                    $dates = '';
                    if (substr($date, 5, -3) == 01)
                    {
                      $dates = 'Jan';
                    }
                    elseif (substr($date, 5, -3) == 02)
                    {
                      $dates = 'Feb';
                    }
                    elseif (substr($date, 5, -3) == 03)
                    {
                      $dates = 'Mar';
                    }
                    elseif (substr($date, 5, -3) == 04)
                    {
                      $dates = 'Apr';
                    }
                    elseif (substr($date, 5, -3) == 05)
                    {
                      $dates = 'May';
                    }
                    elseif (substr($date, 5, -3) == 06)
                    {
                      $dates = 'Jun';
                    }elseif (substr($date, 5, -3) == 07)
                    {
                      $dates = 'Jul';
                    }
                    elseif (substr($date, 5, -3) == 08)
                    {
                      $dates = 'Aug';
                    }
                    elseif (substr($date, 5, -3) == 09)
                    {
                      $dates = 'Sep';
                    }
                    elseif (substr($date, 5, -3) == 10)
                    {
                      $dates = 'Oct';
                    }
                    elseif (substr($date, 5, -3) == 11)
                    {
                      $dates = 'Nov';
                    }
                    elseif (substr($date, 5, -3) == 12)
                    {
                      $dates = 'Dec';
                    }
                    echo $dates;

                    ?> 
                    <?php echo substr($date,-2) ?>
                    <?php echo substr($date, 0, -6)?>
                    / <a href='/newsletter' >E-Newsletter Archive</a></i></b></p>
                    <br>
                    <p>
                    <?php echo $body; ?>
                    </p>

                    </div>


       </div>
       <div class="col-md-4" ng-controller="NMSubcribersCtrl">
         <h3>Sign up for weekly inspirational messages from Ilchi Lee</h3>
        <br>
<a target= "_blank"href="http://visitor.r20.constantcontact.com/d.jsp?llr=s8az7rdab&amp;p=oi&amp;m=1103333458802&amp;sit=qr7vytafb&amp;f=5f75bb62-17d2-4454-8f80-744f4b45883f" class="btn m-b-xs butn" style="font-size: 12px !important; color: #fff !important" >Sign-Up</a>


     </div>
 </div>
</div>
</div>
</div>
