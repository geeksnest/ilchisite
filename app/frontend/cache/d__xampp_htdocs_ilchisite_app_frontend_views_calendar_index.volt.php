<?php echo $this->getContent() ?>
    <div ng-controller="CalenCtrl" style="padding-top:20px">

      <div class="wrapper-md bg-light b-b" style="padding-bottom:10px">
        <!-- <input type="button" value="List View" class="pull-right" ng-click="lisview()" style="width:100px;height:35px"> -->
        <!-- <input type="button" value="Calendar View" class="pull-right" ng-click="calview()" style="width:120px;height:35px"> -->

        <h1 class="h1title"><i class="glyphicon  icon-calendar icon"></i>&nbsp;Calendar of Activity</h1>
        <a href="" ng-click="lisview()" class="btn m-b-xs butn1 pull-right"><p style="color:#fff;">List View</p></a>
        <div class="pull-right">&nbsp;&nbsp;</div>
        <a href="" ng-click="calview()" class="btn m-b-xs butn1 pull-right"><p style="color:#fff;">Calendar View</p></a><br/>
      </div>

      <div style="padding-top:50px;" ng-show="calview1">
        <div class="col wrapper-md">      
          <div class="pos-rlt">
            <!-- begin view calendar -->
            <div class="calendar" ng-model="eventSources" calendar="calendar1" config="uiConfig.calendar" ui-calendar="uiConfig.calendar"></div>
            <!-- end view calendar -->
          </div>
        </div>
      </div> 
      <!-- list view -->
      <div style="padding-top:50px;" ng-show="lisview1">
        <br>
        <?php 
        $lisva = $viewcalendar;
        foreach ($lisva as $key => $value) {          
        ?>
          <table width="100%" style="text-align:left;">
            <tr valign="top"><td  style="font-size:14px; width:15%;">
              <?php
              $imgurl_check = $this->config->application->apiURL."/images/featurebanner/".$lisva[$key]->actbanner;
              if(!is_array(@getimagesize($imgurl_check))){
                $imglink = $this->config->application->apiURL."/images/nia.png";
              }else{
                $imglink = $this->config->application->apiURL."/images/featurebanner/".$lisva[$key]->actbanner;
              }
              ?>
              <div class="fillmyimage"> <img src="<?php echo $imglink; ?>"> </div>
              </td><td style="font-size:14px;">
              <b style="font-size:16px; text-transform:capitalize; "><?php echo $lisva[$key]->acttitle; ?></b>
              <p style="color:#5386c1;font-size:12px;" ><?php echo date("F j, Y", strtotime($lisva[$key]->df)).' - '.date("F j, Y", strtotime($lisva[$key]->dt)); ?></p>
              <?php
              $limitlenght = strrpos($lisva[$key]->actdesc, ' ');
              if($limitlenght >= 275){
                $removedimg = strip_tags($lisva[$key]->actdesc, '<img>');
                $limitcontent = substr($removedimg,0,275);
                $limitlenght = strrpos($limitcontent, ' ');
                $limitlenght = strrpos($limitcontent, ' ');
                echo $viewcontent = substr($removedimg,0,$limitlenght).'...';
              }else{
                $removedimg = strip_tags($lisva[$key]->actdesc, '<img>');
                echo $viewcontent = $removedimg.'...';
              }
              ?>
              <br/>
              <a href="<?php echo $this->config->application->baseURL; ?>/calendar/activityinfo/<?php echo $lisva[$key]->actid; ?>" class="actbtn">
                read more
              </a>
            </td></tr>
            <tr><td colspan="2">&nbsp;</td></tr>
          </table>
        <?php } ?>
      </div>

    </div>