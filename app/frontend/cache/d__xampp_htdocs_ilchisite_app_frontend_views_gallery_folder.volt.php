


<link rel="stylesheet" href="/mosaic/css/mosaic.css" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<script type="text/javascript" src="/mosaic/js/mosaic.1.0.1.js"></script>

<script type="text/javascript">  

  jQuery(function($){


    $('.bar22').mosaic({
          animation : 'slide'   //fade or slide
        });
    

    
  });

</script>

<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

<style type="text/css">
  .gallery-image
  { 
    float: left;
    width:100%;
    height:140px;
    position: relative;
    overflow: hidden;
    background-size: cover;
    background-position: center center;
    float:left;
    /*margin: 0px 10px 10px 0px;*/
  }
  .simple-caption {
   position: absolute; 
   margin: auto;
   width:100%; 
   font-family: 'Proxima Nova Alt Regular';
   font-size: 10pt;
   padding: 10px;
   color: #000; 
   padding-top: 25px;
 }

</style>
<style type="text/css">
  * {
    margin: 0;
    padding: 0;
  }

  hr {
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #fff;
    margin: 25px 0;
    clear: both;
  }
  .centered {
    text-align: center;
  }
  .wrapper {
    width: 100%;
    padding: 30px 0;
  }
  .container {
    width: 1200px;
    margin: 0 auto;
  }
  ul.grid-nav {
    list-style: none;
    font-size: .85em;
    font-weight: 200;
    text-align: center;
  }
  ul.grid-nav li {
    display: inline-block;
  }
  ul.grid-nav li a {
    display: inline-block;
    background: #999;
    color: #fff;
    padding: 10px 20px;
    text-decoration: none;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
  }
  ul.grid-nav li a:hover {
    background: #7b0;
  }
  ul.grid-nav li a.active {
    background: #333;
  }
  .grid-container {
    display: none;
  }
  /* ----- Image grids ----- */
  ul.rig {
    list-style: none;
    font-size: 0px;
    margin-left: -2.5%; /* should match li left margin */
  }
  ul.rig li {
    display: inline-block;
    /*padding: 10px;*/
    margin: 0 0 2.5% 2.5%;
    background: #fff;
    border: 1px solid #ddd;
    font-size: 16px;
    font-size: 1rem;
    vertical-align: top;
    box-shadow: 0 0 5px #ddd;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
  }
  ul.rig li img {
    max-width: 100%;
    height: auto;
    min-height: 100%;
    margin: 0 0 10px;
  }
  ul.rig li h3 {
    margin: 0 0 5px;
  }
  ul.rig li p {
    font-size: .9em;
    line-height: 1.5em;
    color: #999;
  }
  /* class for 2 columns */
  ul.rig.columns-2 li {
    width: 47.5%; /* this value + 2.5 should = 50% */
  }
  /* class for 3 columns */
  ul.rig.columns-3 li {
    width: 30.83%; /* this value + 2.5 should = 33% */
  }
  /* class for 4 columns */
  ul.rig.columns-4 li {
    width: 22.5%; /* this value + 2.5 should = 25% */
  }

  @media (max-width: 1199px) {
    .container {
      width: auto;
      padding: 0 10px;
    }
  }

  @media (max-width: 480px) {
    ul.grid-nav li {
      display: block;
      margin: 0 0 5px;
    }
    ul.grid-nav li a {
      display: block;
    }
    ul.rig {
      margin-left: 0;
    }
    ul.rig li {
      width: 100% !important; /* over-ride all li styles */
      margin: 0 0 20px;
    }
  }
</style>
</head>

<body>

  <div class="wrapper">
    <div class="container">
      <ul class="grid-nav">

      </ul>

      <div class="h1title"><?php $getginfo = $folderinfo; echo $getginfo[0]->foldername; ?></div>
      <br>
      <p><?php $getginfo = $folderinfo; echo $getginfo[0]->description; ?></p>

      <hr />

      <div id="two-columns" class="grid-container" style="display:block;">
        <ul class="rig columns-4">
          <?php 
          $getginfo = $folderinfo;
          foreach ($getginfo as $key => $value) {
           ?>
           <li>

            <div class="gallery-image bar22" style="background-image: url('<?php echo $api_url; ?>/images/<?=$getginfo[$key]->foldername?>/<?=$getginfo[$key]->path ?>')">
             <a href="<?php echo $api_url; ?>/images/<?php  echo $getginfo[$key]->foldername ?>/<?php  echo $getginfo[$key]->path ?>" class="mosaic-overlay" data-lightbox="roadtrip">
               <div class="simple-caption details">
                 <strong><?php  echo substr($getginfo[$key]->title, 0,25) ?></strong>
                 <br>
                 <br>
                 <?php  echo $getginfo[$key]->description ?>
               </div> 
             </a>
           </li>
           <?php } ?> 
         </ul>
       </div>

       <hr />


     </div>
     <!--/.container-->
   </div>
   <!--/.wrapper-->

   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $('.grid-nav li a').on('click', function(event){
        event.preventDefault();
        $('.grid-container').fadeOut(500, function(){
          $('#' + gridID).fadeIn(500);
        });
        var gridID = $(this).attr("data-id");

        $('.grid-nav li a').removeClass("active");
        $(this).addClass("active");
      });
    });
  </script>

</body>
</html>