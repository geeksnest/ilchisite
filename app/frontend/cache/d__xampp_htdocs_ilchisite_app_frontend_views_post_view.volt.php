
<?php echo $this->getContent()?>
<script type="text/javascript">
	/* * * CONFIGURATION VARIABLES * * */
	var disqus_shortname = 'ilchilee';

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function () {
		var s = document.createElement('script'); s.async = true;
		s.type = 'text/javascript';
		s.src = '//' + disqus_shortname + '.disqus.com/count.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
	}());
</script>
<style type="text/css">
	.fillmyimage1 {
		height: 300px;
		width: 100%;
		overflow: hidden;
		background-size: cover;
		background-position: center center;
		float:left;
		margin: 0px 10px 10px 0px;
		border: 1px solid #ddd;
	}

	.sidebarimage {
		height: 70px;
		width: 150px;
		overflow: hidden;
		background-size: cover;
		background-position: center center;
		float:left;
		margin: 0px 10px 10px 0px;
		border: 1px solid #ddd;
	}

	/* This gets Google to fall into place */
	.social {
		font-size: 1px;
		float: right;
		width: 280px;
		padding-left: 15px;
		
	}

	/* This gets Facebook to fall into place */
	.social iframe {
		vertical-align: middle;
	}

	/* Set an optional width for your button wrappers */
	.social span {
		display: inline-block;
		width: 90px;
	}

	/* Adjust the widths individually if you like */
	.social .google {
		width: 75px;
	}
	.datebox{
		padding:5px 5px 5px 5px;
		text-align:center;
		background-color:#eee;
		margin-top:2px;
		float: right;
	}
	.titlebox{
		max-width: 450px;
		display: inline-block; 
	}
	@media (max-width: 560px){

		.titlebox{
			max-width: 350px;
			display: inline-block;
		}
		
	}

	@media (max-width: 457px){

		.titlebox{
			max-width: 190px;
			display: inline-block;
		}
		
	}
	.articlediv{
		overflow: hidden;
	}
	.showall{
		margin-top: 30px;
		display: block;
		width: 100%;
		text-transform: uppercase;
		color: #777;
		border-top: 1px solid #e2e2e2;
		box-shadow: none;
		text-align: center;
	}

</style>



<div class="content">
	<div class="container">
		<div>

			<div class="col-md-8" style="margin-bottom:20px;margin-top:15px;">
				<div class="articlediv">
					

					<div >
						<div class="datebox">
							<div> 
								<div style="font-size:35px"><?php echo substr($date,-2) ?></div>
								<?php

								$dates = '';
								if (substr($date, 5, -3) == 01)
								{
									$dates = 'Jan';
								}
								elseif (substr($date, 5, -3) == 02)
								{
									$dates = 'Feb';
								}
								elseif (substr($date, 5, -3) == 03)
								{
									$dates = 'Mar';
								}
								elseif (substr($date, 5, -3) == 04)
								{
									$dates = 'Apr';
								}
								elseif (substr($date, 5, -3) == 05)
								{
									$dates = 'May';
								}
								elseif (substr($date, 5, -3) == 06)
								{
									$dates = 'Jun';
								}elseif (substr($date, 5, -3) == 07)
								{
									$dates = 'Jul';
								}
								elseif (substr($date, 5, -3) == 08)
								{
									$dates = 'Aug';
								}
								elseif (substr($date, 5, -3) == 09)
								{
									$dates = 'Sep';
								}
								elseif (substr($date, 5, -3) == 10)
								{
									$dates = 'Oct';
								}
								elseif (substr($date, 5, -3) == 11)
								{
									$dates = 'Nov';
								}
								elseif (substr($date, 5, -3) == 12)
								{
									$dates = 'Dec';
								}
								echo $dates;

								?> 
								<?php echo substr($date, 0, -6)?></div>
							</div>



							<div>
								<div class="titlebox">
									<h3><?php echo $title ?></h3>
									<i class="icon-user"></i> By <?php echo $author ?>
									<i class="icon-comment"></i> <span class="disqus-comment-count" data-disqus-url="<?php echo $this->config->application->baseURL; ?>/post/view/<?php echo $category ?>/<?php echo $newsslugs ?>"> Comment</span>
								</div>
								<div class="social">
									<span class="Facebook">
										<div class="fb-share-button" data-href="<?php echo $this->config->application->baseURL; ?>/post/view/<?php echo $category ?>/<?php echo $newsslugs ?>" data-layout="button_count"></div>
									</span>
									<span class="twitter">
										<a href="https://twitter.com/share" class="twitter-share-button"{count} data-via="IlchiLee" data-dnt="true">Tweet</a>
										<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
									</span>
									<span class="google">
										<g:plusone size="medium" href="<?php echo $this->config->application->baseURL; ?>/post/view/<?php echo $category ?>/<?php echo $newsslugs ?>"></g:plusone>
									</span>

								</div>
							</div>

							<br>




						</div>
						<div style="margin-top:20px;" class="content2">


							<?php 

							echo $body;

							?>


						</div>
						<br>
						<br>
						<?php 

						$getginfo = $catdata;

						if($getginfo == null)
						{

						}
						else
						{
							echo "Tags:";

							foreach ($getginfo as $key => $value) {
								echo "<a href='/post/tags/"  . $getginfo[$key]->categoryname . "/". $getginfo[$key]->id ."'>".$getginfo[$key]->categoryname. "</a>, ";
							} 
						}


						?>
						<br>
						<br>
						<div class="sepera2r"> </div>

						

						<div id="disqus_thread"></div>
						<script type="text/javascript">
							/* * * CONFIGURATION VARIABLES * * */
							var disqus_shortname = 'ilchilee';
							
							/* * * DON'T EDIT BELOW THIS LINE * * */
							(function() {
								var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
								dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
								(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							})();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>


						<br>
						<br>
						<br>
						<br>
					</div>
				</div>



				<div class="col-md-4" ng-controller="postrecentctrl">
					<div id="parentHorizontalTab">
						<div class="h5title" >
							<ul class="resp-tabs-list hor_1">
								<li><h4 style="margin-left:-15px">Related Posts</h4></li>
								<li><h4 style="margin-left:-15px">Popular Posts</h4></li>
							</ul>
						</div>
						<div class="resp-tabs-container hor_1">
							<div>

								<?php 
								$getginfo = $postinforelated;

								foreach ($getginfo as $key => $value) {
									?>
									<div style="clear:both;margin-bottom:30px;">
										<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
											<div class="sidebarimage" style="background-image: url('<?php echo $api_url; ?>/images/sliderimages/<?php echo $getginfo[$key]->banner ?>');">
											</div>
										</a>
										<div style="height:74px;">
											<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
												<h6 style="line-height:14px">
													<?php

													
													echo $getginfo[$key]->title;
													

													?>
												</h6>
											</a>

											<?php echo $getginfo[$key]->date ?>
										</div>
									</div>
									<?php } ?> 
								</div>
								<div id="listrecent">
									<?php 
									$getginfo = $popularpost;
									foreach ($getginfo as $key => $value) {
										?>
										<div style="clear:both;margin-bottom:30px;">
											<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
												<div class="sidebarimage" style="background-image: url('<?php echo $api_url; ?>/images/sliderimages/<?php echo $getginfo[$key]->banner ?>');">
												</div>
											</a>
											<div style="height:74px;">
												<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
													<h6  style="line-height:14px">
														<?php

														
														echo $getginfo[$key]->title;
														

														?>
													</h6>
												</a>

												<?php echo $getginfo[$key]->date ?>
											</div>
										</div>
										<?php } ?>
										<br>
										<a href="/post/index" class="showall" ng-show="notendresult" >Show all</a>
										<a href="" class="btn btn-default" disabled ng-show="endresult">No more News to Load...</a>
									</div>									
								</div>
							</div>
							<br>
							<br>
							<div>
								<div class="h4title" >Topics</div>
								<div class="topic" style="padding-top:10px">
									<ul >
										<?php 
										$getginfo = $taginfo;

										foreach ($getginfo as $key => $value) {
											?>
											<li><a href="/post/tags/<?php echo $getginfo[$key]->categoryname ?>/<?php echo $getginfo[$key]->id ?>"><?php echo $getginfo[$key]->categoryname ?></a></li>
											<?php } ?> 
										</ul>
									</div>
								</div>

							</div>





						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT -->


				<div id="fb-root"></div>
				<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>

				<script>!function(d,s,id)
					{var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
				</script>

				<script src="https://apis.google.com/js/platform.js" async defer></script>