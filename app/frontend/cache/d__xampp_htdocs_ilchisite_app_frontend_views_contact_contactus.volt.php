<?php echo $this->getContent()?>
<div class="content contact" ng-controller="contacts">
  <div class="container">
    <div class="row">
      <div class="col-md-12">  
       <!-- Contact -->
       <div class="row">
         <div class="col-md-8">
          <p>
           Hi, I’m Michela. I’m the Administrator at Ilchi.com. It’s my pleasure to help you with anything you need.
           <br><br>
           Please use the contact form below to share your thoughts, experiences, and questions with Ilchi Lee, or let me know of any comments or suggestions you may have about the website.
           <br><br>
           You can also use the form for any public relations or speaking engagement requests.
           <br><br>
           Ilchi Lee may not be able to respond personally to every question, however, you will always receive a concerned and sincere response from me. When he can respond, Ilchi Lee may address your question in an email, or he may share your story in a blog post.
           <br><br>
           We always appreciate hearing from you.
         </p>
       </div>
       <div class="col-md-3">
        <img style="width:150px;height:180px;" src="/images/Michela.png">
      </div>
    </div>

    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveMessage(contacts)" name="formNews" id="formcontact">
      <div class="bor"></div>
      <div class="row">
        <div class="col-md-8">
          <div class="cwell">
           <!-- Contact form -->
           <h4 class="title">Contact Form</h4>
           <alert ng-show="showalert">{[{ alertsmsg }]}</alert>
           <div class="form-group">
            <label for="name">Your Name</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Name" ng-model="contacts.name" required >
          </div>                                    
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" ng-model="contacts.email" required>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Content</label>
            <textarea class="form-control" rows="3" ng-model="contacts.message" required></textarea>
          </div>                                      
          <button type="submit" id="submit" name="submit" class="btn btn-default">Send</button>
          <button type="reset" class="btn btn-default" ng-click="reset()">Reset</button>
          <hr />
        </div>
      </div>
    </div>
  </form>


</div>
</div>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
