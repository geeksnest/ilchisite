<script type="text/javascript">
	/* * * CONFIGURATION VARIABLES * * */
	var disqus_shortname = 'ilchilee';

	/* * * DON'T EDIT BELOW THIS LINE * * */
	(function () {
		var s = document.createElement('script'); s.async = true;
		s.type = 'text/javascript';
		s.src = '//' + disqus_shortname + '.disqus.com/count.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
	}());
</script>
<?php echo $this->getContent()?>
<style type="text/css">
	.fillmyimage1 {
		height: auto;
		width: 100%;
		float: left;
		overflow: hidden;
	}
	.fillmyimage1 img {
		width: 100%;
		height: 100%;
		background-size: cover;

	}
	.sidebarimage {
		height: 70px;
		width: 150px;
		overflow: hidden;
		background-size: cover;
		background-position: center center;
		float:left;
		margin: 0px 10px 10px 0px;
		border: 1px solid #ddd;
	}
	.datebox{
		padding:5px 5px 5px 5px;
		text-align:center;
		background-color:#eee;
		margin-top:2px;
		float: right;
	}

	.titlebox{
		max-width: 450px;
		display: inline-block;
	}

	@media (max-width: 560px){

		.titlebox{
		max-width: 350px;
		display: inline-block;
	}
	
	}

	@media (max-width: 457px){

		.titlebox{
		max-width: 190px;
		display: inline-block;
	}
	
	}
	.articlediv{
		overflow: hidden;
	}
	.showmorebutton
	{
		padding: 0px 10px 0px 10px;
		background-color: #FFF;
		border-radius: 5px;
		border: 1px solid #ddd;
		width: 100%;
	}
	.showall{
		margin-top: 30px;
		display: block;
		width: 100%;
		text-transform: uppercase;
		color: #777;
		border-top: 1px solid #e2e2e2;
		box-shadow: none;
		text-align: center;
	}
</style>

<div class="content" >
	<div class="container">
		<div>
			
			<div class="h1title" style="padding-left:-10px;" >Latest Posts</div>
			<br>
			<div class="col-md-8" style="margin-bottom:20px;margin-top:15px;" ng-controller="postlatestctrl" >
				<div class="articlediv" id="latestlist">
					<?php 
					$getginfo = $postinfo;

					foreach ($getginfo as $key => $value) {
						?>
						<div class="articlediv">
							<div class="articlediv">
								<div class="datebox">
									<div style="margin-top:10px;">
										<div style="font-size:35px;"><?php echo substr($getginfo[$key]->date,-2) ?></div>
										<?php

										$dates = '';
										if (substr($getginfo[$key]->date, 5, -3) == 01)
										{
											$dates = 'Jan';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 02)
										{
											$dates = 'Feb';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 03)
										{
											$dates = 'Mar';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 04)
										{
											$dates = 'Apr';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 05)
										{
											$dates = 'May';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 06)
										{
											$dates = 'Jun';
										}elseif (substr($getginfo[$key]->date, 5, -3) == 07)
										{
											$dates = 'Jul';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 08)
										{
											$dates = 'Aug';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 09)
										{
											$dates = 'Sep';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 10)
										{
											$dates = 'Oct';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 11)
										{
											$dates = 'Nov';
										}
										elseif (substr($getginfo[$key]->date, 5, -3) == 12)
										{
											$dates = 'Dec';
										}
										echo $dates;

										?> 
										<?php echo substr($getginfo[$key]->date, 0, -6)?>
									</div>
								</div>


								<div class="titlebox">
									<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
										<h3><?php echo $getginfo[$key]->title ?></h3>
									</a>
									<i class="icon-user"></i> By <?php echo $getginfo[$key]->author ?> 
									<i class="icon-comment"></i> <span class="disqus-comment-count" data-disqus-url="<?php echo $this->config->application->baseURL; ?>/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>"></span>
								</div>
							</div>
							<div style="margin-top:20px;" class="content2">


								<?php 

								echo $getginfo[$key]->body;

								?>


							</div>

							<div class="sepera2r"> </div>
							<br>
							<br>
						</div>

						<?php } ?>  
					</div>

					<center>
						<div class="spinner" ng-show="showloading">
							<div class="bounce1"></div>
							<div class="bounce2"></div>
							<div class="bounce3"></div>
						</div>
						<br>
						<a href="" class="showall" ng-show="notendresult" ng-click="getlatest()" style="width: 100%;" ><center>Load more</center></a>
						<a href="" class="showall" disabled ng-show="endresult" style="width: 100%;"><center>No more Content to Load</center></a>
					</center>
				</div>



				<div class="col-md-4" ng-controller="postrecentctrl" >
					<div id="parentHorizontalTab">
						<div class="h4title" >Popular Posts</div>
						
					
						<br>
						<div class="resp-tabs-container hor_1">
							<div id="listrecent">
								<?php 
								$getginfo = $popularpost;

								foreach ($getginfo as $key => $value) {
									?>
									<div style="clear:both;margin-bottom:30px;">
										<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
											<div class="sidebarimage" style="background-image: url('<?php echo $getginfo[$key]->banner ?>');">
											</div>
										</a>
										<div style="height:74px;">
											<a href="/post/view/<?php echo $getginfo[$key]->category ?>/<?php echo $getginfo[$key]->newsslugs ?>">
												<h6 style="line-height:14px">
													<?php

												
														echo $getginfo[$key]->title;
												

													?>
												</h6>
											</a>

											<?php echo $getginfo[$key]->date ?>
										</div>
									</div>
									<?php } ?> 
									
								</div>
								<br>
										<!-- <a href="/post/index" class="showall" ng-show="notendresult" >Show all</a>
										<a href="" class="btn btn-default" disabled ng-show="endresult">No more News to Load...</a> -->
							</div>
						</div>
						<br>
						<br>
						<div>
							<div class="h4title" >Topics</div>
							<div class="topic" style="padding-top:10px">
								<ul >
									<?php 
									$getginfo = $taginfo;

									foreach ($getginfo as $key => $value) {
										?>
										<li><a href="/post/tags/<?php echo $getginfo[$key]->categoryname ?>/<?php echo $getginfo[$key]->id ?>"><?php echo $getginfo[$key]->categoryname ?></a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>