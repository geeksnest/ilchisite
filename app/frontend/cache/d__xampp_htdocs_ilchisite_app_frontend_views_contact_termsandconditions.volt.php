<div class="content contact" ng-controller="contacts"> 
	<div class="container">
		<div>
			<div class="col-md-12">  
				<h2>PLEASE READ THE FOLLOWING TERMS AND CONDITIONS RELATING TO YOUR USE OF THIS SITE CAREFULLY.</h2>
				<br>
				<p>By using this site, you are deemed to have agreed to these terms and conditions of use. We reserve the right to modify them at any time. You should check these terms and conditions periodically for changes.
				</p>
				<br>
				<br>
				<p>Restrictions on Use of Materials
					The contents of our sites are protected by copyright and trademark laws, and are the property of their owners. Unless we say otherwise, you may access the materials located within the Sites only for your personal use. This means you may download one copy of posted materials on a single computer for personal, noncommercial home use only, so long as you neither change nor delete any author attribution, trademark, legend or copyright notice. When you download copyrighted material you do not obtain any ownership rights in that material. You may not modify, copy, publish, display, transmit, adapt or in any way exploit the content of the Sites.
				</p>
				<br>
				<br>
				<h2>Links</h2>
				<br>
				<p>These terms and conditions apply only to the Site, and not to the sites of any other companies or organizations, including those to which any of the Sites may link. We are not responsible for the availability of any other site to which any of the Sites links. We do not endorse or take responsibility for the contents, advertising, products or other materials made available through any other site. Under no circumstances will we be held responsible or liable, directly or indirectly, for any loss or damage that is caused or alleged to have been caused to you in connection with your use of, or reliance on, any content, goods or services available on any other site.
				</p>
				<br>
				<br>
				<h2>Disclaimers</h2>
				<br>
				<p>THE SERVICES, PRODUCTS AND MATERIALS ON THIS SITE ARE PROVIDED “AS IS” AND WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. WE DISCLAIM ALL ARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. NEITHER WE NOR ANY OF OUR RESPECTIVE LICENSORS OR SUPPLIERS WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE SERVICES, PRODUCTS OR MATERIALS IN THIS SITE IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.</p>
				<br>
				<p>We do not endorse, warrant or guarantee any products or services offered on the Sites. We are not a party to, and do not monitor, any transaction between users and third party providers of products or services.
					Limitation of Liability.</p>
					<br>
					<p>UNDER NO CIRCUMSTANCES, INCLUDING BUT NOT LIMITED TO NEGLIGENCE, WILL WE OR ANY OF OUR LICENSORS OR SUPPLIERS BE LIABLE FOR ANY SPECIAL OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF, OR THE INABILITY TO USE, THE MATERIALS ON THE SITES, OR ANY PRODUCTS OR SERVICES PROVIDED PURSUANT TO THE SITES, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
					</p>
					<br>
					<br>
					<h2>No Personal Advice</h2>
					<br>
					<p>The information contained in or made available through the Sites cannot replace or substitute for the services of trained professionals in any field, including, but not limited to, financial, medical, or legal matters. In particular, you should regularly consult a doctor in all matters relating to physical or mental health. We and our licensors or Suppliers make no representations or warranties concerning any treatment, action, or application of medication or preparation by any person following the information offered or provided within or through the Sites.
					</p>
					<br>
					<br>
					<h2>Jurisdiction and Miscellaneous</h2>
					<br>
					<p>These terms will be governed by and construed in accordance with the laws of the State of Arizona, without regard to any principles of conflicts of 
						law. You agree that any action at law or in equity that arises out of or relates to these terms will be subject to binding arbitration in accordance with the Arizona Arbitration Association and will be arbitrated in Arizona. If any of these terms and conditions are found unlawful, void, or for any reason unenforceable, then that provision will be considered severable from the remaining terms and conditions, and will not affect the validity and enforceability of the remaining provisions. This is the entire agreement between you and us relating to the subject matter it contains.
					</p>
					<br>
					<br>
					<h2>Privacy Policy</h2>
					<br>
					<p>We are committed to protecting your privacy and security. We will not share or sell any of your information with outside companies. Any information that we collect is solely for our own statistics to be able to provide a better experience, for you the user. Information that may be collected is the type of browser, operating system, network provider, etc.
					</p>
					<br>
					<br>
					<h2>Personal Identification Information</h2>
					<br>
					<p>We may request personal identification information from you in connection with your use of, or participation in, membership registration ilchi.com, and in connection with other activities, services or resources we make available on any of our sites. In all of these cases, we will collect personal identification information from you only if you voluntarily submit such information to us, and will use that information for conducting the activities which you agreed on by submitting your personal identification information. We will never intentionally disclose any personal identification information about you as an individual user (such as, for example, your full name, street address, telephone number, credit card number or e-mail address) to any third party without your permission. We may also use cookie technology to improve your experience of these activities, including to remember your preferences, customize the content and advertisements that you see or authenticate your access to your personal information.
					</p>
					<br>
					<br>
					<h2>Non-Personal Identification Information</h2>
					<br>
					<p>We collect non-personal identification information through the use of: “Cookie” technology: A “cookie” is an element of data that a web site can send to your browser, which may then store it on your system. Non-personal identification information might include the browser you use, the type of computer, the Internet service providers utilized, and other similar information.
						Our systems may also automatically gather information about the areas you visit and search terms you utilize on our sites. We use such information to administer the site and, in the aggregate, to understand how our users as a group use the services and resources provided on our sites. This way we know which areas of our sites are favorites of our users, which areas need improvement, and what technologies are being used so that we may continually improve our sites. Most browsers are set to accept cookies. You can set your browser to refuse cookies, or to alert you when cookies are being sent. If you do so, please note that some parts of the Sites, such as message boards, may not function properly.
					</p>
					<br>
					<br>
					<h2>User Supplied Information</h2>
					<br>
					<p>Ilchi.com does not want to receive confidential or proprietary information from you via the Site. You agree that any material, information, or data you transmit to us or post to the Site (each a “Submission” or collectively “Submissions”) will be considered non confidential and non proprietary. For all Submissions, (1) you guarantee to us that you have the legal right to post the Submission and that it will not violate any law or the rights of any person or entity, and (2) you give Ilchi.com the royalty-free, irrevocable, perpetual, worldwide right to use, distribute, display and create derivative works from the Submission, in any and all media, in any manner, in whole or in part, without any restriction or responsibilities to you.
					</p>
					<br>
					<br>
					<h2>Your Acceptance of These Terms</h2>
					<br>
					<p>By using any of the Sites, you signify your acceptance of our Privacy Policy. If you do not agree to this policy, please do not use our sites. Your continued use of the Sites following the posting of changes to these terms will mean that you accept those changes.
					</p>
				</div>
			</div>
		</div>
	</div>