<?php echo $this->getContent()?>
<style type="text/css">
	.minheight
	{
		min-height: 500px;
	}
	.searchtitle a
	{
		font-family: 'MuseoSans_500-webfont';
		font-size: 16px;
		color: #000;
	}
	.searchtitle
	{
		font-family: 'MuseoSans_500-webfont';
		font-size: 16px;
		color: #000;
	}
	.searchresult
	{
		font-family: 'MuseoSans_500-webfont';
		font-size: 16px;
		color: #F00;
	}
	.searchcontent
	{
	  margin-bottom: 10px;
	  border-bottom: 1px solid #999;
	  padding-bottom: 10px;
	}
	.post-info
	{
  	  color: #1cabe3;
  	  font-family: 'MuseoSans_500-webfont';
  	  font-size: 12px;
	}
	
	.pagination1 li a{
		position: relative;
		padding: 8px 12px 8px 12px;
		line-height: 1.428571429;
		text-decoration: none;
		
		font-weight: bold;
		
	}
	.active
	{
		z-index: 2;
		color: #fff;
		cursor: default;
		background: url('/images/number.png');
	}
	.active a
	{
		z-index: 2;
		color: #fff;
		cursor: default;
		background: url('/images/number.png');
	}
	.pagination1{
		float: left;
		margin-left:-50px;

	}
	.pagination1 li{
		display: inline;
		list-style: none;

	}

</style>
<div class="minheight">

	<div class="h1title">Search Result</div>
	<br>
	<?php 
	$getginfo = $searchresult;
	$count = count($searchresult);
	$count1 = count($pageresult);

	$total_pages = ceil($count1 / 10);

	

	
	?>
	<div style="border-bottom: 1px dashed #bdb2a3;"></div>
	<div class="searchtitle"><span class="searchresult" ><?php echo $count1; ?></span> Result for <span class="searchresult" ><i>"<?php echo $keyword;  ?>"</i></span></div>
	<br>
	<?php
	if($getginfo== null)
					{
						
					}
					else
					{
	foreach ($getginfo as $key => $value) {

		$linkeso = '';

		if ($getginfo[$key]->type == 'News')
		{
			$linkeso = '/post/view/News/';
		}
		elseif ($getginfo[$key]->type == 'Journal')
		{
			$linkeso = '/post/view/Journal/';
		}
		elseif ($getginfo[$key]->type == 'Video')
		{
			$linkeso = '/post/view/Video/';
		}
		elseif ($getginfo[$key]->type == 'Pages')
		{
			$linkeso = '/page/index/';
		}
		elseif ($getginfo[$key]->type == 'Newsletter')
		{
			$linkeso = '/newsletter/view/';
		}
		elseif ($getginfo[$key]->type == 'Videos')
		{
			$linkeso = '/video/readabout/';
		}
		elseif ($getginfo[$key]->type == 'Albums')
		{
			$linkeso = '/gallery/folder/';
		}
		elseif ($getginfo[$key]->type == 'Projects')
		{
			$linkeso = '/projects/index/';
		}
		elseif ($getginfo[$key]->type == 'Activity')
		{
			$linkeso = '/calendar/activityinfo/';
		}
	?>
		<div class="searchcontent">
		<div class="searchtitle"><a href="<?php echo $linkeso.$getginfo[$key]->slugs; ?>"><?php echo $getginfo[$key]->title;  ?></a></div>
		<div class="post-info"><?php echo $getginfo[$key]->type; ?></div>
		<p style="font-size:12">
			<?php
			echo substr(strip_tags(html_entity_decode($getginfo[$key]->body)),0,150)."...";
			?>
		</p>
		</div>
	
	<?php } ?>
	
    <ul class="pagination1">
	<?php
	echo "<li ><a href='1'>".'<-'."</a></li>"; // Goto 1st page 

	for ($i=1; $i<=$total_pages; $i++) 
	{ 
				if($offset==$i)
				{
				$activepage = "class='active'";
				}
				else
				{
					$activepage ='';
				}
	            echo "<li><a ". $activepage ." href='".$i."''>".$i."</a></li>"; 
	}; 
	echo "<li><a href='$total_pages'>".'->'."</a></li>"; // Goto last page
	}
	?>
	</ul> 


</div>