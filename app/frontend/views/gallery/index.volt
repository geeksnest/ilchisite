 <div ng-controller="albumctrl">
   <?php echo $this->getContent() ?>
   <style type="text/css">
     .list-image
     { 
      float: left;
      width:303px;
      height:303px;
      position: relative;
      overflow: hidden;
      background-size: cover;
      background-position: center center;
      float:left;
      margin: 0px 10px 10px 0px;
      border: 1px solid #ddd;
    }
    .gallery-warp{
      margin-right: auto;
      margin-left: auto;
      width: 100%;
      padding: 15px;
      height: auto;
      overflow: hidden;
    }

    .simple-caption {
      position: absolute; 
      margin: auto;
      width:100%; 
      font-family: 'Proxima Nova Alt Regular';
      font-size: 12pt;
      padding: 10px;
      color: #555; 
    }
    .showmorebutton
    {
      padding: 0px 10px 0px 10px;
      background-color: #FFF;
      border-radius: 5px;
      border: 1px solid #ddd;
      width: 250px;
    }
  </style>

  <link rel="stylesheet" href="/mosaic/css/mosaic.css" type="text/css" media="screen" />

  

  <div class="h1title">Gallery</div>

  <br>
  <br>

  <div class="gallery-warp" id="albumlist">


   <?php 
   $getginfo = $albuminfo;

   foreach ($getginfo as $key => $value) {
     ?>
     <div class="list-image bar2" style="background-image: url('<?php echo $this->config->application->amazonS3; ?>/<?php  echo $albuminfo[$key]->cats->path ?>')">
      <a href="<?php echo $this->config->application->baseURL; ?>/gallery/folder/<?php echo $getginfo[$key]->cats->folderid ?>" class="mosaic-overlay">
        <div class="simple-caption details">
          <br>
          <br>
          <div><?php echo $getginfo[$key]->album_name ?> </div>
          <p> <i class="icon-calendar"></i> <?php echo $getginfo[$key]->date ?></p>
          <br>
          <div>
            <p>
             <?php 
             if(strlen($getginfo[$key]->description) < 250)
             {
              echo $getginfo[$key]->description;
             }
             else
             {
              echo substr($getginfo[$key]->description,0,250).'...'; 
             }
             ?>
           </p>
         </div>
       </div>
     </a>
   </div>
   <?php } ?> 


 </div>

</div>
