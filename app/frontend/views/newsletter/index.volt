<?php echo $this->getContent()?>
<style type="text/css">
  .showall{
    margin-top: 30px;
    display: block;
    width: 100%;
    text-transform: uppercase;
    font-size: 12px;
    color: #777;
    border-top: 1px solid #e2e2e2;
    box-shadow: none;
    text-align: center;
  }
</style>
<div class="content contact" ng-controller="newsletterload" >
  <div class="container">
    <div class="row">
     <div class="col-md-8">
       <div class="h1title" >E-Newsletter Archive</div>
       <br>
       <br>
       <div id="latestlist">
       <?php 
       $getginfo = $newsletterinfo;
       foreach ($getginfo as $key => $value) {
         ?>

         <table style="margin-top:20px; margin-bottom:30px;width:100%;"><tr><td>
          <span><div class="fillmyimage"><img src="<?php echo $this->config->application->apiURL; ?>/images/featurebanner/<?php echo $getginfo[$key]->banner; ?>" style="width:150px;height:150px;"></div></span>
          <a href='/newsletter/view/<?php echo $getginfo[$key]->newsletterid ?>'><h4><?php echo $getginfo[$key]->title ?></h4></a>
          <p style="color: #999;"><b><i>


          <?php

                   $_dbdate = explode("-", $getginfo[$key]->date);
               
                    $dates = '';
                    if ($_dbdate[1]  == '01'){
                      $dates = 'Jan';
                    }
                    elseif ($_dbdate[1] == '02'){
                      $dates = 'Feb';
                    }
                    elseif ($_dbdate[1] == '03'){
                      $dates = 'Mar';
                    }
                    elseif ($_dbdate[1] == '04'){
                      $dates = 'Apr';
                    }
                    elseif ($_dbdate[1] == '05'){
                      $dates = 'May';
                    }
                    elseif ($_dbdate[1]  == '06'){
                      $dates = 'Jun';
                    }elseif ($_dbdate[1] == '07'){
                      $dates = 'Jul';
                    }
                    elseif ($_dbdate[1] == "08"){
                      $dates = 'Aug';
                    }
                    elseif ($_dbdate[1] == '09'){
                      $dates = 'Sep';
                    }
                    elseif ($_dbdate[1] == '10'){
                      $dates = 'Oct';
                    }
                    elseif ($_dbdate[1] == '11'){
                      $dates = 'Nov';
                    }
                    elseif ($_dbdate[1] == '12'){
                      $dates = 'Dec';
                    }
                    echo $dates;
                    ?> 
                    


                    <?=$_dbdate[2] ?>,
                    <?=$_dbdate[0]?>
                    </i></b></p>
          <br>
        </td></tr></table>
        <?php } ?> 
        </div>
        <center>
            <div class="spinner" ng-show="showloading">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
            <br>
            <a href="" class="showall" ng-show="notendresult" ng-click="getlatest()" style="width: 100%;" ><center>Load more</center></a>
            <a href="" class="showall" disabled ng-show="endresult" style="width: 100%;"><center>No more Content to Load</center></a>
          </center>   


       </div>
       <div class="col-md-4" ng-controller="NMSubcribersCtrl">
        <h3>Sign up for weekly inspirational messages from Ilchi Lee</h3>
        <br>
<a target= "_blank"href="http://visitor.r20.constantcontact.com/d.jsp?llr=s8az7rdab&amp;p=oi&amp;m=1103333458802&amp;sit=qr7vytafb&amp;f=5f75bb62-17d2-4454-8f80-744f4b45883f" class="btn m-b-xs butn" style="font-size: 12px !important; color: #fff !important" >Sign-Up</a>
	<!--
        <span class="formerror" ng-show="emailtaken != ''" style="color: #f80000;">Email is already subscribed.</span>
        <span class="formerror" ng-show="emailtaken2 != ''" style="color: #f80000;">Email already a member.</span>
        <span class="formerror" ng-show="emailregister != ''" style="color: #0C0;">Thank you for subscribing to newsletter.</span>
        <form class="form-inline form-validation ng-pristine ng-invalid ng-invalid-required" name="form" style="padding-bottom:22px;">

         <input type="text" class="form-control ng-pristine ng-invalid-required ng-valid-pattern" id="name" name="name" ng-model="nms.name" placeholder="Name" required>

         <br>
         <br>

         <input type="email" class="form-control ng-pristine ng-invalid-required ng-valid-pattern" id="email" name="email" ng-model="nms.email" placeholder="E-mail" required>

         <br>
         <br>

         <button type="submit" class="btn m-b-xs butn" ng-click="SaveNMS(nms)">Sign-Up</button>

       </form>
	-->
     </div>
 </div>
</div>
</div>

