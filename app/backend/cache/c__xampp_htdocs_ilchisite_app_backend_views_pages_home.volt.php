<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Home </h1>
	<a id="top"></a>
</div>
<script type="text/ng-template" id="updateAlbum.html">
	<div ng-include="'/tpl/updateAlbum.html'"></div>
</script>
<script type="text/ng-template" id="deletealbum.html">
	<div ng-include="'/tpl/deleteAlbum.html'"></div>
</script>
<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="panel panel-default">
		<div class="panel-heading">
			Home Media Title
		</div>
		<div class="table-responsive">
			<input type="hidden">
			<table class="table table-striped b-t b-light">
				<thead>
					<tr>
						<th style="width:70%">Title</th>
						<th style="width:30%">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="data in data">
						<td><span editable-text="data.title" onbeforesave="updatealbumname($index, data.title, datas)" e-pattern="[a-zA-Z0-9\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ data.title }]}</span></td>
						<td>
							<a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"> <span class="label bg-warning">Edit Title</span></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
</div>



<div class="wrapper-md">
	<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
	<div class="panel panel-default">
		<div class="panel-heading">
			About Ilchi
		</div>
		<div class="panel-body">
			<textarea class="ck-editor" ng-model="about"></textarea>
			<footer class="panel-footer text-right bg-light lter">
				<button type="submit" class="btn btn-success" ng-click="saveAbout(about)">Save</button>
			</footer>
		</div>
	</div>
</div>


