<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Static Page</h1>
  <a id="top"></a>
</div>
<div class="hbox hbox-auto-xs hbox-auto-sm" >
  <div class="col w-md bg-light dk b-r bg-auto bg-auto-left">
  <!-- <div class="wrapper b-b bg">
      <button class="btn btn-sm btn-default pull-right visible-sm visible-xs" ui-toggle-class="show" target="#email-menu"><i class="fa fa-bars"></i></button>
      <a ui-sref="app.mail.compose" class="btn btn-sm btn-danger w-xs font-bold">Compose</a>
    </div> -->
    <div class="bg-light lter b-b wrapper-md">
    <h3 class="m-n font-thin h5">Menu</h3>
      <a id="top"></a>
    </div>
    <div class="wrapper hidden-sm hidden-xs" id="email-menu">

      <ul class="nav nav-pills nav-stacked nav-sm">

        <li ui-sref-active="active">
          <a ui-sref="statpage.home">
          <i class="fa fa-file-text-o"></i>
            <span style="margin-left:10px;">Home Page</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="statpage.contact">
          <i class="fa fa-file-text-o"></i>
            <span style="margin-left:10px;">Contact Us</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="statpage.terms">
             <i class="fa fa-file-text-o"></i>
            <span style="margin-left:10px;">Terms and Condition</span>
          </a>
        </li>
        <li ui-sref-active="active">
          <a ui-sref="statpage.medialinks">
             <i class="fa fa-file-text-o"></i>
            <span style="margin-left:10px;">Social Media Url</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="col">
    <div ui-view >
      
    </div>
  </div>
</div>