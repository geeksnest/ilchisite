<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deletebanner.html">
  <div ng-include="'/tpl/deletebanner.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Publication</h1>
  <a id="top"></a>
</div>
<div>
    <form class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="save(info)" name="form" >
        <fieldset ng-disabled="isSaving">
            <div class="wrapper-md">
                <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-heading font-bold">
                                Page Information
                            </div>
                            <div class="panel-body">

                                <label class="col-sm-2 control-label">
                                    <label for="title">Title</label>
                                </label>
                                <div class="col-sm-10">
                                    <!-- ngIf: user.usernametaken == true -->
                                    <input type="text" id="title" name="title" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.title" required="required" >
                                    <br>
                                </div>

                                <br>
                                <div class="line line-dashed b-b line-lg pull-in"></div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Body Content</label>
                                    <div class="col-sm-10">

                                        <textarea class="ck-editor" ng-model="info.content"></textarea>

                                    </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Price</label>
                                    <div class="col-sm-10">
                                       <input type="text" id="title" name="title" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.price" required="required" ng-keypress="praicevalidate(info.price)" ng-pattern="/^[0-9]+(?:\.[0-9]+)?$/"  >
                                   </div>
                               </div>
                               <div class="line line-dashed b-b line-lg pull-in"></div>
                               <div class="form-group">
                                <label class="col-sm-2 control-label">Reference Link</label>
                                <div class="col-sm-10">
                                	<input type="url" id="title" name="title" class="form-control ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.reflink" required="required" placeholder="Example : <?php echo $this->config->application->baseURL ?>" >
                                </div>
                            </div>

                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Category</label>
                                <div class="col-sm-10">
                                    <select name="pagefeatures" id="pagefeatures" class="form-control m-b" ng-model="info.category" required="required">
                                      <option value="1">Books</option>
                                      <option value="2">CDs</option>
                                      <option value="3">Related Products</option>
                                  </select>

                              </div>
                          </div>

                          <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta Title</label>
                                <div class="col-sm-10">
                                    <input type="text" id="metatitle" name="metatitle" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.metatitle" required="required" >
                              </div>
                          </div>
                          <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta Description</label>
                                <div class="col-sm-10">
                                    <input type="text" id="metadescription" name="metadescription" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.metadesc" required="required" >
                              </div>
                          </div>
                           <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta Keyword</label>
                                <div class="col-sm-10">
                                    <input type="text" id="metakeyword" name="metakeyword" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.keyword" required="required" >
                              </div>
                          </div>




                          <div class="line line-dashed b-b line-lg pull-in"></div>

                          <div class="checkbox">
                            <label class="i-checks">
                                <input type="checkbox" value="" ng-model='info.check'>
                                <i></i> Page Active
                            </label>
                        </div>
                    </div>




                </div>
            </div>


            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                    	<div class="widget-content">
                    		<div class="padd">
                    			<span class="btn btn-success fileinput-button">
                    				<i class="glyphicon glyphicon-plus"></i>
                    				<span>Upload Banner</span>
                    				<input id="digitalAssets" type="file" name="files[]" multiple>
                    			</span>
                    			<div id="progress" class="progress">
                    				<div class="progress-bar progress-bar-success"></div>
                    			</div>
                    			<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data">
                    				<div class="gallery digital-assets-gallery">
                    					<div class="clearfix"></div>
                    				</div>
                    			</form>

                    			<div  ng-repeat="info in data">
                    				<div class="data panel-default">
                                        <?php 
                                        $trueimage = $this->config->application->apiURL."/images/pubbanner/{[{ info.imgpath }]}";
                                        $defaultimage = $this->config->application->apiURL."/images/default.png";



                                        ?>
                    					<img style="width:100%;height:250px;position:relative; "ng-src="<?php echo $trueimage; ?>" err-src="<?php echo $defaultimage; ?>">
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="panel-body">
                <footer class="panel-footer text-right bg-light lter">
                   <a ui-sref="dashboard" class="btn btn-default">Cancel</a>
                   <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Submit</button>
               </footer>
           </div>
       </div>
   </div>
</fieldset>
</form>
</div>



