<?php echo $this->getContent(); ?>

<script type="text/ng-template" id="updateAlbum.html">
  <div ng-include="'/tpl/updateAlbum.html'"></div>
</script>
<script type="text/ng-template" id="deletealbum.html">
  <div ng-include="'/tpl/deleteAlbum.html'"></div>
</script>
<script type="text/ng-template" id="mngAlbumDesc.html">
  <div ng-include="'/tpl/mngAlbumDesc.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Album List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">

  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Album
    </div>
    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="SearchAlbum(album)" name="form" >
      <div class="row wrapper">
        <div class="col-sm-3">
          <div class="input-group">
            <input class="input-sm form-control" ng-model="album.query" placeholder="Search" type="text">
            <span class="input-group-btn">
              <!-- <button class="btn btn-sm btn-default" type="button">Go!</button> -->
              <button class="btn m-b-xs btn-sm btn-info btn-addon">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </form>
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="table-responsive">
      <input type="hidden">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            
            </th>
            <th style="width:60%">Album</th>
            <th style="width:40%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data">
            <td><span editable-text="user.album_name" onbeforesave="updatealbumname($data, user.album_id)" e-pattern="[a-zA-Z0-9\s]+" e-oninvalid="setCustomValidity('Please enter Alphabets and Numbers only ')"  e-required e-form="textBtnForm">{[{ user.album_name }]}</span></td>
           
            <td>
              <a href="" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible"> <span class="label bg-warning">Edit Album Name</span></a>
              <a href="" ng-click="description({id:user.album_id,title:user.album_name,desc:user.description})" ng-hide="textBtnForm.$visible"><span class="label bg-primary" >Manage Description</span></a>
              <a href="" ng-click="updatepage(user.album_id )" ng-hide="textBtnForm.$visible"><span class="label bg-info" >Add/Delete photo</span></a>
              <a href="" ng-click="deletepage(user.album_name)" ng-hide="textBtnForm.$visible"> <span class="label bg-danger">Delete Album</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <ul class="pagination">
        <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a></li>
        <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
          <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
        </li>
        <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a></li>
      </ul>   


      
    </footer>
  </div>
</div>