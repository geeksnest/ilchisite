<?php echo $this->getContent(); ?>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Add Subscriber</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="addSubscriber(subscriber)" name="form">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">Subscriber's Info</div>
            <div class="panel-body">
              <input type="hidden">
          
              <label class="col-sm-2 control-label"><label for="NMSname">Name</label> </label>
              <div class="col-sm-10">
                <input type="text" id="NMSname" name="NMSname" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="subscriber.NMSname">
                <br>              
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <label class="col-sm-2 control-label"><label for="NMSemail">Email <i style="font-size:10px">(required)</i></label> </label>
              <div class="col-sm-10">
                <input type="text" id="NMSemail" name="NMSemail" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="subscriber.NMSemail" placeholder="Example: usermail@domain.ext -- ilchi@gmail.com" required="required">
                <br>              
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="panel-body">
               <footer class="panel-footer text-right bg-light lter">
                <button type="submit" class="btn btn-success" ng-disabled="form.$invalid">Submit</button>
                <button type="button" class="btn btn-default" ng-click="reset()">Clear</button>
              </footer>
            </div>
          </div>       
        </div>
      </div>
    </div>
    <div class="row" >
    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
