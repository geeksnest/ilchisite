<?php echo $this->getContent(); ?>
<style type="text/css">
 .label_profile_pic {
   display: block;
   margin:10px 0;
 }
 .create-proj-thumb{
  text-align: center;
}
.create-proj-thumb label{
  font-size: 12px;
}
.create-proj-thumb img{
  max-width: 100%;
}
.propic {
  position: relative;
}
</style>
<script type="text/javascript">  
  function isNumberKey(evt)
  {
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

  return true;
}
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Settings</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md" style="float:;lrft">
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading"><span class="h4">Maintenance</span></div>
        <div class="panel-body">

          <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmaintenance">
            <div ng-if="value1 == 0" >

              <div class="line line-dashed b-b line-lg pull-in"></div>            
              <span class="help-block m-b-none">In order to to turn on maintenance mode you must fill all the fields.</span>          
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Massage</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="on.maintenance_msg" required="required" ng-model="on.maintenance_msg">
                </div><br/><br/>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Time</label>
                <div class="col-sm-4">
                  <div class="input-group bootstrap-touchspin">
                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                    <input ui-jq="TouchSpin" type="text" value="" class="form-control" data-min="0" data-max="100" data-verticalbuttons="true" data-verticalupclass="fa fa-caret-up" data-verticaldownclass="fa fa-caret-down" style="display: block;" required="required" name="on.maintenance_time" ng-model="on.maintenance_time" onkeypress='return isNumberKey(event)'>
                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                  </div>
                </div><br/><br/>
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">Off</spam> 
                <div class="m-b-sm col-sm-2">
                  <label class="i-switch i-switch-lg m-t-xs m-r">
                    <input type="checkbox" value="1" name="maintenance_on" ng-model="maintenance_on" required="required">
                    <i></i>
                  </label>
                </div>
                <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">On</spam> 
              </div>              
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="col-sm-12">
                <button type="submit" class="btn btn-sm  btn-success" ng-click="savesettings(on)" ng-disabled="formmaintenance.$invalid" style="float:right;">Save change</button>
              </div>            
            </div>
          </form>


          <div ng-if="value1 == 1">
            <form class="form-validation ng-pristine ng-invalid ng-invalid-required" name="formmaintenance">
              <div class="line line-dashed b-b line-lg pull-in"></div>            
              <span class="help-block m-b-none">Maintenance mode is turned on.</span>
              <div class="line line-dashed b-b line-lg pull-in"></div>            
              <span class="help-block m-b-none">{[{ value2 }]}</span>          
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <span class="help-block m-b-none">{[{ value3 }]} hours maintenance</span> 
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="form-group">

                <spam class="col-sm-1 control-label" style="font-size:20px;padding-top:5px;">On</spam> 
                <div class="m-b-sm col-sm-2">
                  <label class="i-switch i-switch-lg m-t-xs m-r">
                    <input type="checkbox" value="0" name="maintenance_off" ng-model="maintenance_off" required="required">
                    <i></i>
                  </label>
                </div>              
                <spam class="col-sm-4 control-label" style="font-size:20px;padding-top:5px;margin-left:-30px;">Off</spam> 
              </div>              
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="col-sm-12">
                <button type="submit" class="btn btn-sm btn-primary" id="off"  ng-click="savesettingsoff()" ng-disabled="formmaintenance.$invalid" style="float:right;">Save change</button>
              </div>
            </form>
          </div>




        </div>
      </div>
    </div>

    <!-- to be continue -->
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading"><span class="h4">Change Logo</span></div>
        <div class="panel-body"> 
          <div  >
            <div class="loader" ng-show="imageloader">
              <div class="loadercontainer">
                <div class="spinner">
                  <div class="rect1"></div>
                  <div class="rect2"></div>
                  <div class="rect3"></div>
                  <div class="rect4"></div>
                  <div class="rect5"></div>
                </div>
                Uploading your images please wait...
              </div>
            </div> 

            <div class="panel-body" ng-show="imagecontent">
              <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)">{[{ imgAlerts.msg }]}</alert>
              </div>
              <div class="col-sm-12 create-proj-thumb">
                <img src="{[{info.imgpath}]}">
              </div>
              <div class="col-sm-12 propic create-proj-thumb">
                <br><br>
                <input type="hidden" ng-model="info.imgpath" ng-value="info.imgpath =projImg">
                <div class=" border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" required="required">
                  <a href="">Choose an image from your computer</a><br>
                  <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                  <label>At least 1024x768 pixels</label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="panel-body">
                <footer class="panel-footer text-right bg-light lter">
                  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
                  <button type="submit" ng-click="savelogo(info.imgpath)" class="btn btn-success">Save</button>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- to be continue -->
    <!-- to be continue -->
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading"><span class="h4">Google Analytics</span></div>
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <div class="panel-body">      
          <form class="form-validation ng-pristine ng-invalid ng-invalid-required"  ng-submit="save(info)" name="form" >  
           <div class="row">
            <div class="panel-body">
             <div class="form-group">
              <label class=" control-label">Google Analytics Script</label>
              <pre>
                {[{dbscripts}]}
              </pre>
              <input type="text" id="title" name="title" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="info.data"  required="required" placeholder="Paste Script" >
              <br>

            </div>
            <footer class="panel-footer text-right bg-light lter">
             <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Save</button>
           </footer>


         </div>
       </div> 
     </form>      
   </div>
 </div>
</div>
<!-- to be continue -->


</div>
</div>


