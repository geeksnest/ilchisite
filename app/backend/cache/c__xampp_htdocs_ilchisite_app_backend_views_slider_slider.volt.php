<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="deleteSliderPHoto.html">
  "<div ng-include="'/tpl/deleteSliderPHoto.html'"></div>"
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Slider</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md ">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading font-bold"> Upload Slider Photo</div>

      <div class="panel-body">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-head">
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>  
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
              <div class="padd">
                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="albumtTitle(album)" name="form" >
                 <input id="albumid" type="hidden" ng-model ="folderid" name="uniqueid" ng-value="folderid = genId.folderid" placeholder="">
                 Album Title: <input id="albumtitle" type="text" name="title" ng-model="title">
                 
               </form>

               <div class="loader" ng-show="imageloader">
                <div class="loadercontainer">
                  <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                  </div>
                  Uploading your images please wait...
                </div>
              </div>

              <div ng-show="imagecontent">
                <div class="col-sml-12">
                  <div class="dragdropcenter">
                    <div ngf-drop ngf-select class="drop-box" 
                    ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
                    accept="image/*,application/pdf" ngf-change="upload($files,{title:title})" >
                    Drop images here or click to upload <br>
                  </div>
                </div>
              </div>
              <div class="col-sm-6" ng-repeat="data in data">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="panel-heading">
                      <ul class="nav nav-pills pull-right">
                        <li>
                          <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltimg(dlt)" name="form" enctype="multipart/form-data">
                            <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                            <div class="buttonPanel{[{ data.imgid }]}">
                             <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                           </div>
                         </form>
                       </li>
                     </ul>
                   </div>
                   <div class="line line-dashed b-b line-lg pull-in"></div>
                   <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="imgInfo(info)" name="form" enctype="multipart/form-data">
                     <fieldset ng-disabled="isSaving">
                      <div class="containerimagefill1" >
                        <img style="width:100%;height:300px;" ng-src="{[{ data.imgpath }]}" err-src="">
                      </div>
                      <input type="hidden" id="" name="imgid" ng-init="info.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="info.id">
                      <div class="line line-dashed b-b line-lg pull-in"></div>

                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>  
</div>       
</div>
</div>
