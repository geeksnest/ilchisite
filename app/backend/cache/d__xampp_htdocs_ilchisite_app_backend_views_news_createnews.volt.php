<?php echo $this->getContent(); ?>

<style type="text/css">
    .containerimagefill1{
        height: 190px;
        width: 100%;
        overflow: hidden;
        background-size: cover;
        background-position: center center;
        float:left;
        margin: 0px 10px 10px 0px;
    }

</style>


<script type="text/ng-template" id="editMemberModal.html">
    <form class="bs-example form-horizontal" name="form" novalidate>
        <div class="modal-header">
            <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
        </div>
        <div class="modal-body" ng-show="process==true">
            <p ng-show="success==false">Updating user profile record.</p>
            <p ng-show="success==true">Success!</p>
        </div>
        <div class="modal-body" ng-show="process==false">
            ARE YOU SURE YOU WANT TO DELETE?
        </div>
        <div class="modal-footer" ng-hide="process==true">
            <button class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
        </div>
    </form>
</script>
<script type="text/ng-template" id="addcategory.html">
 <div ng-include="'/tpl/addcategory.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Create News</h1>
    <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="saveNews(news)" name="formNews" id="formNews">
    <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            News Information
                        </div>
                        <div class="panel-body">

                            <label class="col-sm-2 control-label">
                                <label for="title">News Title</label>
                            </label>
                            <div class="col-sm-10">
                                <!-- ngIf: user.usernametaken == true -->
                                <input type="text" id="title" name="title" class="form-control" ng-model="news.title" name="news.title" required="required" ng-keyup="onnewstitle(news.title)">
                                <input type="hidden" ng-model="news.slugs">
                            </div>
                            <br>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <label class="col-sm-2 control-label">
                                <label for="title">Author</label>
                            </label>
                            <div class="col-sm-10">
                                <!-- ngIf: user.usernametaken == true -->
                                <input type="text" class="form-control" ng-model="news.author" name="news.author" required="required" >
                            </div>
                            <br>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <label class="col-sm-2 control-label"><label for="title">Date</label> </label>
                            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
                              <div class="input-group w-md">
                                <span class="input-group-btn">
                                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="news.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                </span>
                            </div>
                        </div>

                        <br>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        <label class="col-sm-2 control-label">
                            <label for="title">Tags/Keywords</label>
                        </label>
                        <div class="col-sm-10" id="chosen">
                            <div>

                                <select multiple chosen id="autoship_option" class="js-example-basic-multiple" multiple="multiple" style="width: 100%" ng-model="news.tags"  id="tagoption" ng-options="cat.id as cat.categoryname for cat in category ">
                                </select>
                            </div>
                            <br>
                            <button type="button" class="btn m-b-xs btn-sm btn-primary btn-addon" ng-click="addtags()"><i class="fa fa-plus" style="width=100%;"></i>Add New Tags</button>
                        </div>

                        <br>
                        <div class="line line-dashed b-b line-lg pull-in"></div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">News Content</label>
                            <div class="col-sm-10">

                                <textarea class="ck-editor" ng-model="news.body"></textarea>

                            </div>
                        </div>

                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">News Category</label>
                            <div class="col-sm-10">
                             <select name="pagefeatures" id="pagefeatures" class="form-control m-b" ng-model="news.category" required="required">
                              <option value="{[{ data.categoryname }]}" ng-repeat="data in datalist"> {[{ data.categoryname }]} </option>
                          </select>

                      </div>
                  </div>

                    <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta Title</label>
                                <div class="col-sm-10">
                                    <input type="text" id="metatitle" name="metatitle" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metatitle" required="required" >
                              </div>
                          </div>
                          <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta Description</label>
                                <div class="col-sm-10">
                                    <input type="text" id="metadescription" name="metadescription" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.metadesc" required="required" >
                              </div>
                          </div>
                           <div class="line line-dashed b-b line-lg pull-in"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta Keyword</label>
                                <div class="col-sm-10">
                                    <input type="text" id="metakeyword" name="metakeyword" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="news.keyword" required="required" >
                              </div>
                          </div>


                  <div class="line line-dashed b-b line-lg pull-in"></div>

                  <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" value="" ng-model='news.check'>
                        <i></i> Post Active
                    </label>
                </div>
            </div>




        </div>
    </div>

    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading font-bold">
                News Baner
            </div>
            <div class="panel-body">
                <?php 
                  $trueimage = $this->config->application->apiURL."/images/sliderimages/{[{ news.banner }]}";
                  $defaultimage = $this->config->application->apiURL."/images/default.png";
                 
              
                 
                ?>
               
                <img ng-show="news.banner" ng-src="<?php echo $trueimage; ?>" err-src="<?php echo $defaultimage; ?>" style="width: 100%">
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <input type="text" id="banner" name="banner" class="form-control" ng-model="news.banner"  placeholder="Paste Banner link here..." required="required">
                <div class="line line-dashed b-b line-lg pull-in"></div>
            </div>
        </div>
    </div>

</div>


<div class="row">
    <div class="panel-body">
        <footer class="panel-footer text-right bg-light lter">
            <button type="button" class="btn btn-default" ng-click="reset()">Clear</button>
            <button type="submit" id="submit" name="submit" class="btn btn-success" ng-disabled="formNews.$invalid">Submit</button>
        </footer>
    </div>
</div>
</fieldset>
</form>

<div class="wrapper-md ">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading font-bold"> Upload New Photo</div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <div class="widget">
                        <div class="widget-content">
                            <div class="padd">
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Add files...</span>
                                    <input id="digitalAssets" type="file" name="files[]" multiple>
                                </span>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data">
                                    <div class="gallery digital-assets-gallery">
                                        <div class="clearfix"></div>
                                    </div>
                                </form>

                                 <div class="col-sm-4" ng-repeat="data in data">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="panel-heading">
                                                <ul class="nav nav-pills pull-right">
                                                    <li>
                                                        <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltnewsimageClick(dlt)" name="form" enctype="multipart/form-data">
                                                            <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid" class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                                                            <div class="buttonPanel{[{ data.imgid }]}">
                                                                <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                                <br>
                                                <br>
                                            </div>
                                             <div class="input-group m-b">
                                              <span class="input-group-btn">
                                                  <button class="btn btn-default" type="button">Banner</button>
                                              </span>
                                                <input type="text" class="form-control" ng-model="data.vanner" ng-value="data.vanner = data.imgpath">
                                          </div>
                                            <div class="input-group m-b">
                                              <span class="input-group-btn">
                                                  <button class="btn btn-default" type="button">URL</button>
                                              </span>
                                                <input type="text" class="form-control" ng-model="data.img" ng-value="data.img ='<?php echo $this->config->application->apiURL; ?>/images/sliderimages/'+data.imgpath ">
                                          </div>

                                          <div class="line line-dashed b-b line-lg pull-in"></div>
                                          <div class="containerimagefill1" style="background-image: url('{[{ data.img }]}');">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
</div>


