<?php echo $this->getContent(); ?>
<script type="text/ng-template" id="editMemberModal.html">
  <form class="bs-example form-horizontal" name="form" novalidate>
    <div class="modal-header">
      <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>
    <div class="modal-body" ng-show="process==true">
      <p ng-show="success==false">Updating user profile record.</p>
      <p ng-show="success==true">Success!</p>
    </div>
    <div class="modal-body" ng-show="process==false">
      ARE YOU SURE YOU WANT TO DELETE?
    </div>
    <div class="modal-footer" ng-hide="process==true">                  
      <button class="btn btn-default" ng-click="cancel()">Cancel</button>
      <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>
  </form>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Slider</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md ng-scope">

  <div class="row">

    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading font-bold"> Album Iiformation</div>
        <div class="panel-body">
          <form role="form" class="ng-pristine ng-valid">
            <input id="albumid" type="hidden" ng-model ="folderid" name="uniqueid" ng-value="folderid = genId.folderid" placeholder="">
            <div class="form-group">
              <label>Album Title:</label>
              <input type="text" id="albumtitle" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="title" >
            </div>
            <div class="form-group">
              <label> Short Description</label>
              <input type="text" id="description" name="desc" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="desc">
            </div>
            <div class="form-group">
              <label>Date:</label>
              <div class="input-group w-md" ng-controller="DatepickerDemoCtrl">
                <span class="input-group-btn">
                  <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="createdate" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col-sm-12">
      <div class="panel panel-default col-sm-12">
        <div class="panel-heading font-bold">
          Uploaded Images
        </div>
        <div class="loader" ng-show="imageloader">
          <div class="loadercontainer">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
            Uploading your images please wait...
          </div>
        </div>
        <div ng-show="imagecontent">
          <div class="col-sml-12">
            <div class="dragdropcenter">
              <div ngf-drop ngf-select class="drop-box" 
              ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
              accept="image/*,application/pdf" ngf-change="upload($files,{folderid:folderid, title:title, desc:desc, date:createdate})" >
              Drop images here or click to upload <br>
            </div>
          </div>
        </div>

        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
        <input id="idcenter" type="hidden" id="centerid" ng-value="idcenter= centerid" ng-model="idcenter">
        <div>
          <div ng-show="noimage"><center>IMAGE GALLERY IS EMPTY</div>
          <div class="line line-dashed b-b line-lg"></div>

          <div class="col-sm-3" ng-repeat="data in data">
            <div class="panel panel-default">
              <ul class="nav nav-pills pull-right">
                <li>
                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltimg(dlt, folderid)" name="form" enctype="multipart/form-data">
                    <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                    <div class="buttonPanel{[{ data.imgid }]}">
                     <button style="padding:2px;border: none;background: none;margin-top:-5px;"><i class="glyphicon glyphicon-remove"></i></button>
                   </div>
                 </form>
               </li>
             </ul>
             <div class="panel-body">
              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="imgInfo(info)" name="form" enctype="multipart/form-data">
                <input type="hidden" id="" name="imgid" ng-init="info.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="info.id">
                <div class="containerimagefill1" >
                  <img style="width:100%;height:300px;" ng-src="{[{ data.imgpath }]}" err-src="">
                </div>
                <!-- Input for Image Title and Description -->
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                 <label>Title: </label>
                 <input type="text" onclick="this.focus();this.select()" id="" name="imgtitle" value="{[{ data.imgtitle }]}" class="form-control" ng-model="info.title" ng-init="info.title=data.imgtitle">
               </div>
               <div class="line line-dashed b-b line-lg pull-in"></div>
               <div class="form-group">
                <label>Description: </label>
                <input type="text" id="" name="imgdescription" value="" class="form-control" ng-init="info.description=data.description"  ng-model="info.description">
              </div>
              <div class="line line-dashed b-b line-lg pull-in"></div>
              <div class="buttonPanel{[{ data.imgid }]}">
                <button class="btn m-b-xs btn-sm btn-primary btn-addon"><i class="fa fa-plus" style='width=100%;'></i>Update Information</button>
                <span ng-show="saveboxmessage == data.imgid"> <i class="fa fa-check"></i> Saved!</span>
              </div>

            </form>
          </div>
        </div>
      </div>








    </div>
  </div>
</div>
</div>
</div>
</div>