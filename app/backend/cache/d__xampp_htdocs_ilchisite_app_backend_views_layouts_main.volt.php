<!DOCTYPE html>
<?php include "../app/config/config.php"; ?>
<html lang="en" data-ng-app="app">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <!-- Title and other stuffs -->
    <?php echo $this->tag->getTitle(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable = yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Stylesheets -->
    <?php echo $this->tag->stylesheetLink('css/bootstrap.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/animate.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/font-awesome.min.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/simple-line-icons.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/font.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/app.css'); ?>

    <!-- jquery fileuploader -->
    <?php echo $this->tag->stylesheetLink('css/jquery.fileupload.css'); ?>
    <?php echo $this->tag->stylesheetLink('js/tokeninput/styles/token-input.css'); ?>
    <?php echo $this->tag->stylesheetLink('js/select2/dist/css/select2.min.css'); ?>
    <?php echo $this->tag->stylesheetLink('js/jquery/chosen/chosen.css'); ?>
    <?php echo $this->tag->stylesheetLink('js/jquery/chosen/chosen-spinner.css'); ?>

    <!-- video -->

    <?php echo $this->tag->stylesheetLink('css/video/projekktor.style.css'); ?>
    <?php echo $this->tag->stylesheetLink('js/angular-xeditable/css/xeditable.css'); ?>

   

    <link rel="shortcut icon" href="/images/faviconblue.ico" type='image/x-icon'/>
</head>

<body ng-controller="AppCtrl">

<?php echo $this->getContent(); ?>
<div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}">
    <div class="app-header navbar">
        <!-- navbar header -->
        <div class="navbar-header bg-info dker">
            <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
                <i class="glyphicon glyphicon-cog"></i>
            </button>
            <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
                <i class="glyphicon glyphicon-align-justify"></i>
            </button>
            <!-- brand -->
            <a href="#/" class="navbar-brand text-lt">
                <img src="/images/headerlogo_backend.png" alt=".">
            </a>
            <!-- / brand -->
        </div>
        <!-- / navbar header -->

        <!-- navbar collapse -->
        <div class="collapse navbar-collapse box-shadow bg-info dker">
            <!-- buttons -->
            <div class="nav navbar-nav m-l-sm hidden-xs">
                <a href class="btn no-shadow navbar-btn" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
                    <i class="fa {[{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}]} fa-fw"></i>
                </a>
                <a href class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
                    <i class="icon-user fa-fw"></i>
                </a>
            </div>
            <!-- / buttons -->

            <!-- link and dropdown -->
            <ul class="nav navbar-nav hidden-sm">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
                        <span translate="header.navbar.new.NEW">New</span> <span class="caret"></span>
                    </a>
                   <ul class="dropdown-menu" role="menu">
                            <li>
                                <a ui-sref="createnews">
                                    <span translate="header.navbar.new.NEWS">Task</span>
                                </a>
                            </li>
                          
                            <li>
                                 <a ui-sref="createpage">
                                    <span translate="header.navbar.new.PAGE">Email</span>
                                </a>
                            </li>
                          
                        </ul>
                </li>
            </ul>
            <!-- / link and dropdown -->

            <!-- search form -->
         
            <!-- / search form -->

            <!-- nabar right -->
            <ul class="nav navbar-nav navbar-right" >
                <li class="hidden-xs">
                    <a ui-fullscreen></a>
                </li>
                <li class="dropdown">
                    <form  name="form">
                        <input id="setid" type="hidden"  value="<?php echo $userid; ?>">
                    </form>
                    <a href class="dropdown-toggle clear" data-toggle="dropdown">
                        <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">                          
                            <?php
                                // $imgurl_check = $this->config->application->apiURL."/images/profile_picture/thumbnail/".$imgg;
                                // if(!is_array(@getimagesize($imgurl_check))){
                                //   $imglink = $this->config->application->apiURL."/images/profile_picture/thumbnail/default-page-thumb.png";
                                // }else{
                                //   $imglink = $imgurl_check;
                                // }
                            ?>
                            <img style="height:40px" src="<?php echo $this->config->application->apiURL."/images/profile_picture/thumbnail/".$imgg; ?>" alt="...">
                            <i class="on md b-white bottom"></i>
                        </span>
                        <span class="hidden-sm hidden-md"><?php echo $fullname; ?></span> <b class="caret"></b>
                    </a>
                    <!-- dropdown -->
                    <ul class="dropdown-menu animated fadeInRight w">
                        <li class="wrapper b-b m-b-sm bg-light m-t-n-xs">
                            <div>
                                <p>300mb of 500mb used</p>
                            </div>
                            <progressbar value="60" class="progress-xs m-b-none bg-white"></progressbar>
                        </li>

                        <li>
                            <a ui-sref="managesettings">
                                <span class="badge bg-danger pull-right">30%</span>
                                <span translate="aside.nav.your_stuff.SETTINGS">Settings</span>
                            </a>
                        </li>
                        
                        <li>
                            <a ui-sref="editprofile({userid:<?php echo $userid; ?>})">
                                <span translate="aside.nav.profile.EDIT_PROFILE">Edit Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->config->application->baseURL . '/user_manual/User Manual (Ilchi site final).pdf'; ?>" target="_blank"> <!-- ui-sref="app.docs" -->
                                <span class="label bg-info pull-right">new</span> Help
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo $this->config->application->baseURL . '/ilchiadmin/index/logout' ?>" name="logout">Logout
                                <!-- <form  method="post" name="form" action="<?php //echo $this->config->application->baseURL . '/ecoadmin' ?>" enctype="multipart/form-data">
                                <input type="submit" name="logout" value="Logout">
                                </form> -->
                            </a>
                        </li>
                    </ul>
                    <!-- / dropdown -->
                </li>
            </ul>
            <!-- / navbar right -->

        </div>
        <!-- / navbar collapse -->
    </div>

    <!-- menu -->
    <div class="app-aside hidden-xs bg-light dker">
        <div class="aside-wrap">
            <div class="navi-wrap">
                <!-- user -->
                <div class="clearfix hidden-xs text-center hide" id="aside-user">
                    <div class="dropdown wrapper">
                        <a ui-sref="app.page.profile">
                        <span class="thumb-lg w-auto-folded avatar m-t-sm">
                            <img src="/img/a0.jpg" class="img-full" alt="...">
                        </span>
                        </a>
                        <a href class="dropdown-toggle hidden-folded">
                        <span class="clear">
                            <span class="block m-t-sm">
                              <strong class="font-bold text-lt">Efren Bautista Jr.</strong> 
                              <b class="caret"></b>
                          </span>
                          <span class="text-muted text-xs block">Software / Web Developer</span>
                      </span>
                        </a>
                        <!-- dropdown -->
                        <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                            <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
                                <span class="arrow top hidden-folded arrow-info"></span>
                                <div>
                                    <p>300mb of 500mb used</p>
                                </div>
                                <progressbar value="60" type="white" class="progress-xs m-b-none dker"></progressbar>
                            </li>
                            <li>
                                <a href>Settings</a>
                            </li>
                            <li>
                                <a ui-sref="app.page.profile">Profile</a>
                            </li>
                            <li>
                                <a href>
                                    <span class="badge bg-danger pull-right">3</span> Notifications
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a ui-sref="access.signin">Logout</a>
                            </li>
                        </ul>
                        <!-- / dropdown -->
                    </div>
                    <div class="line dk hidden-folded"></div>
                </div>
                <!-- / user -->

                <!-- nav -->
                <nav ui-nav class="navi">
                    <!-- first -->
                    <ul class="nav">
                        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                            <span translate="aside.nav.HEADER">Navigation</span>
                        </li>

                        <li>
                            <a ui-sref="dashboard">
                                <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                                <span class="font-bold" translate="aside.nav.DASHBOARD">Dashboard</span>
                            </a>
                        </li>
                        <?php if(in_array('imagegalleryrole', $roles) || in_array('videogalleryrole', $roles)){ ?>
                        <li ng-class="{active:$state.includes('app.gallery')}">
                            <a href class="auto">
                        <span class="pull-right text-muted">
                            <i class="fa fa-fw fa-angle-right text"></i>
                            <i class="fa fa-fw fa-angle-down text-active"></i>
                        </span>

                                <i class="glyphicon  glyphicon-picture icon"></i>
                                <span class="font-bold" translate="aside.nav.gallery.GALLERY">Users</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <?php
                                        if(in_array('videogalleryrole', $roles)) { ?>
                                <li ui-sref-active="active">
                                    <a ui-sref="video">
                                        <span translate="aside.nav.gallery.VIDEO">Video</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="video_list">
                                        <span translate="aside.nav.gallery.VIDEO_LIST">Video LIST</span>
                                    </a>
                                </li>
                                <?php } if(in_array('imagegalleryrole', $roles)) {  ?>
                                <li ui-sref-active="active">
                                    <a ui-sref="image">
                                        <span translate="aside.nav.gallery.IMAGE">Image</span>
                                    </a>
                                    <li ui-sref-active="active">
                                        <a ui-sref="manage_album">
                                            <span translate="aside.nav.gallery.MANAGE_ALBUM">Manage Album</span>
                                        </a>
                                    </li>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } if(in_array('slidergalleryrole', $roles)){ ?>
                        <li ng-class="{active:$state.includes('app.sliderimage')}">
                            <a href class="auto">
                        <span class="pull-right text-muted">
                            <i class="fa fa-fw fa-angle-right text"></i>
                            <i class="fa fa-fw fa-angle-down text-active"></i>
                        </span>

                                <i class="glyphicon glyphicon-sound-stereo"></i>
                                <span class="font-bold" translate="aside.nav.sliderimage.SLIDER">SLIDER</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="sliderimage">
                                        <span translate="aside.nav.sliderimage.CREATE_SLIDER">Video</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="slider_list">
                                        <span translate="aside.nav.sliderimage.MANAGE_SLIDER">Video LIST</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } if(in_array('publicationrole', $roles)){ ?>
                        <li ng-class="{active:$state.includes('app.publication')}">
                            <a href class="auto">
                        <span class="pull-right text-muted">
                            <i class="fa fa-fw fa-angle-right text"></i>
                            <i class="fa fa-fw fa-angle-down text-active"></i>
                        </span>

                                <i class="glyphicon glyphicon-book"></i>
                                <span class="font-bold" translate="aside.nav.publication.PUBLICATION">Publication</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="createpublication">
                                        <span translate="aside.nav.publication.CREATE_PUBLICATION">Video</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="managepublication">
                                        <span translate="aside.nav.publication.MANAGE_PUBLICATION">Video LIST</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } if(in_array('calendarrole', $roles)){ ?>

                        <li ng-class="{active:$state.includes('app.calendar')}">
                            <a href class="auto">
            <span class="pull-right text-muted">
                <i class="fa fa-fw fa-angle-right text"></i>
                <i class="fa fa-fw fa-angle-down text-active"></i>
            </span>
                                <i class="glyphicon glyphicon-calendar icon text-info-dker"></i>
                                <span class="font-bold" translate="aside.nav.calendar.CALENDAR">Calendar</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="addcalendar">
                                        <span translate="aside.nav.calendar.Add_Calendar">Add Activity</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="viewcalendar">
                                        <span translate="aside.nav.calendar.View_Calendar">View Activity</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } if(in_array('newsrole', $roles)){ ?>

                        <!-- news rainier -->
                        <li ng-class="{active:$state.includes('app.news')}">
                            <a href class="auto">
      <span class="pull-right text-muted">

        <i class="fa fa-fw fa-angle-right text"></i>
        <i class="fa fa-fw fa-angle-down text-active"></i>
    </span>
                                <i class="glyphicon glyphicon-edit icon text-info-lter"></i>
                                <span class="font-bold" translate="aside.nav.news.NEWS">News</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="createnews">
                                        <span translate="aside.nav.news.Create_News">Create News</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="managenews">
                                        <span translate="aside.nav.news.Manage_News">Manage News</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="tags">
                                        <span translate="aside.nav.news.Tags">News Tags</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="category">
                                        <span translate="aside.nav.news.Category">News Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } if(in_array('proposalesrole', $roles)){ ?>
                        <li>
                            <a ui-sref="#">
                                <i class="glyphicon  glyphicon-phone-alt icon"></i>
                                <span class="font-bold" translate="aside.nav.contacts">Contacts</span>
                            </a>
                        </li>
                        <?php } if(in_array('contactsrole', $roles)){ ?>
                        
                        <li ng-class="{active:$state.includes('app.contacts')}">
                            <a href class="auto">
                              <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                            </span>
                            <i class="glyphicon  glyphicon-phone-alt icon"></i>
                            <span class="font-bold" translate="aside.nav.contacts.CONTACTS">Proposals</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li ui-sref-active="active">

                                <a ui-sref="managecontacts">
                                    <span translate="aside.nav.contacts.Manage_Contacts">Manage Proposals</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                        <?php } if(in_array('pagesrole', $roles)){ ?>


                        <!-- pages rainier -->
                        <li ng-class="{active:$state.includes('app.pages')}">
                            <a href class="auto">

        <span class="pull-right text-muted">
            <i class="fa fa-fw fa-angle-right text"></i>
            <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>

                                <i class="glyphicon glyphicon-th-large icon text-success"></i>
                                <span class="font-bold" translate="aside.nav.pages.PAGES">Pages</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="createpage">
                                        <span translate="aside.nav.pages.Create_Page">Create Page</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="managepages">
                                        <span translate="aside.nav.pages.Manage_Page">Manage Page</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- <li ui-sref-active="active">
                            <a ui-sref="menucreator"> -->
                        <!-- <b class="badge bg-success dk pull-right">16</b> -->
                        <!--         <i class="glyphicon  glyphicon-list icon"></i>
                                <span class="font-bold" translate="aside.nav.others.MENUCREATOR">Menu Creator</span>
                            </a>
                        </li> -->

                        <?php } if(in_array('usersrole', $roles)){ ?>

                        <li ng-class="{active:$state.includes('app.users')}">
                            <a href class="auto">
        <span class="pull-right text-muted">
            <i class="fa fa-fw fa-angle-right text"></i>
            <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>


                                <i class="glyphicon glyphicon-user icon"></i>
                                <span class="font-bold" translate="aside.nav.users.USERS">Users</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="userlist">
                                        <span translate="aside.nav.users.USER_LIST">User List</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="userscreate">
                                        <span translate="aside.nav.users.CREATE_USER">Create User</span>
                                    </a>
                                </li>
                                <!-- <li ui-sref-active="active">
                                    <a ui-sref="members">
                                        <span translate="aside.nav.users.MEMBERS">Members</span>
                                    </a>
                                </li> -->
                            </ul>
                        </li>

                        <!-- <li ui-sref-active="active">
                            <a ui-sref="donationlist">
                              <i class="glyphicon glyphicon-usd icon text-info-dker"></i>
                              <span class="font-bold" translate="aside.nav.DONATIONS">Donations</span>
                          </a>
                        </li> -->
                        <!-- <li ng-class="{active:$state.includes('app.peacemarks')}">
                            <a href class="auto">
                              <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                            </span>
                            <i class="glyphicon glyphicon-map-marker icon text-info-dker"></i>
                            <span class="font-bold" translate="aside.nav.map.MAPS">Peace Maps</span>
                        </a>
                        <ul class="nav nav-sub dk">
                          <li ui-sref-active="active">
                            <a ui-sref="peacemarks({pageid: page.id})">
                              <span translate="aside.nav.map.Create_Peacemap">Create Peacemap</span>
                          </a>
                        </li>
                        <li ui-sref-active="active">
                            <a ui-sref="managepeacemarks">
                              <span translate="aside.nav.map.Manage_Peacemap">Manage Peacemaps</span>
                          </a>
                        </li>
                        </ul>
                        </li> -->


                        <!-- <li ng-class="{active:$state.includes('app.calendar')}">
                            <a href class="auto">
                                <span class="pull-right text-muted">
                                    <i class="fa fa-fw fa-angle-right text"></i>
                                    <i class="fa fa-fw fa-angle-down text-active"></i>
                                </span>
                                <i class="glyphicon glyphicon-calendar icon text-info-dker"></i>
                                <span class="font-bold" translate="aside.nav.calendar.CALENDAR">Calendar</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="addcalendar">
                                      <span translate="aside.nav.calendar.Add_Calendar">Add Activity</span>
                                  </a>
                              </li>
                          </ul>
                          <ul class="nav nav-sub dk">
                            <li ui-sref-active="active">
                                <a ui-sref="viewcalendar">
                                    <span translate="aside.nav.calendar.View_Calendar">View Activity</span>
                                </a>
                            </li>
                        </ul>
                        </li> -->
                        <?php } if(in_array('projectsrole', $roles)){ ?>
                        <!-- pages jimmy -->
                        <li ng-class="{active:$state.includes('app.feature')}">
                            <a href class="auto">


        <span class="pull-right text-muted">
            <i class="fa fa-fw fa-angle-right text"></i>
            <i class="fa fa-fw fa-angle-down text-active"></i>
        </span>
                                <i class="glyphicon fa fa-suitcase icon"></i>
                                <span class="font-bold" translate="aside.nav.feature.FEATURE">Project</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="createfeaturedproject">
                                        <span translate="aside.nav.feature.Create_Feature">Create Project</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="managefeaturedproject">
                                        <span translate="aside.nav.feature.Manage_Feature">Manage Project</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <?php 
                        } if(in_array('newslettersrole', $roles) || in_array('subscriberrole', $roles)){ ?>

                        <li ng-class="{active:$state.includes('app.newsletter')}">
                            <a href class="auto">
                                  <span class="pull-right text-muted">
                                    <i class="fa fa-fw fa-angle-right text"></i>
                                    <i class="fa fa-fw fa-angle-down text-active"></i>
                                </span>

                                <i class="glyphicon glyphicon-envelope icon"></i>
                                <span class="font-bold" translate="aside.nav.newsletter.NEWSLETTER">News</span>
                            </a>
                            <?php
                                if(in_array('subscriberrole', $roles)){
                            ?>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="addsubscriber">
                                        <span translate="aside.nav.subscribers.Add_Subscriber">Add Subscribers</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="subscriberslist">
                                        <span translate="aside.nav.subscribers.Subscribers_List">Subscribers List</span>
                                    </a>
                                </li>
                            </ul>
                            <?php 
                                } if(in_array('newslettersrole', $roles)){
                            ?>
                            <ul class="nav nav-sub dk">
                                <li ui-sref-active="active">
                                    <a ui-sref="createnewsletter">
                                        <span translate="aside.nav.newsletter.Create_Newsletter">Create News</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="managenewsletter">
                                        <span translate="aside.nav.newsletter.Manage_Newsletter">Manage News</span>
                                    </a>
                                </li>
                            </ul>
                            <?php
                                }
                            ?>
                        </li>
                        <?php } ?>
                        <!-- /////////////////////////////////////////////////////////////////////////////////////////////// -->
                        <!-- /////////////////////////////////////////////////////////////////////////////////////////////// -->
                        <!-- /////////////////////////////////////////////////////////////////////////////////////////////// -->

                        <!-- <li ng-class="{active:$state.includes('app.sliders')}">
                            <a href class="auto">
                              <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                            </span>
                            <i class="glyphicon glyphicon-sound-stereo icon"></i>
                            <span class="font-bold" translate="aside.nav.others.sliders.SLIDERS">Sliders</span>
                        </a>
                        <ul class="nav nav-sub dk">
                            <li ui-sref-active="active">
                                <a ui-sref="create_album">
                                    <span translate="aside.nav.others.sliders.CREATE_ALBUM">Create Album</span>
                                </a>
                            </li>
                            <li ui-sref-active="active">
                                <a ui-sref="manage_album">
                                    <span translate="aside.nav.others.sliders.MANAGE_ALBUM">Manage Album</span>
                                </a>
                            </li>
                        </ul>
                        </li> -->

                        <li class="line dk hidden-folded"></li>

                        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                            <span translate="aside.nav.your_stuff.OTHERS">Your Stuff</span>
                        </li>
                        <!-- <li>
                            <a ui-sref="app.page.profile">
                                <i class="icon-user icon text-success-lter"></i>
                                <b class="badge bg-success dk pull-right">30%</b>
                                <span translate="aside.nav.your_stuff.PROFILE">Profile</span>
                            </a>
                        </li> -->
                        <!-- <li>
                            <a ui-sref="app.docs">
                                <i class="glyphicon glyphicon-tasks icon"></i>
                                <span translate="aside.nav.your_stuff.LOGS">Documents</span>
                            </a>
                        </li> -->
                        <!-- SETTINGS uson -->
                        <li ui-sref-actives="active">
                            <a ui-sref="managesettings">
                                <i class="glyphicon glyphicon-cog icon"></i>
                                <span class="font-bold" translate="aside.nav.your_stuff.SETTINGS">SETTINGS</span>
                            </a>
                        </li>
                    </ul>
                    <!-- / third -->

                </nav>
                <!-- nav -->

                <!-- aside footer -->
                <div class="wrapper m-t">
                    <div class="text-center-folded">
                        <span class=" pull-none-folded"> &copy; 2015 Copyright. <br> POWERED BY GEEKSNEST</span>
                    </div>
                </div>
                <!-- / aside footer -->
            </div>
        </div>

    </div>
    <!-- / menu -->
    <!-- content -->
    <div class="app-content">
        <div ui-butterbar></div>
        <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside"></a>
        <div class="app-content-body fade-in-up" ui-view></div>
    </div>
    <!-- /content -->
    <!-- aside right -->
    <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
        <div class="vbox">
            <div class="wrapper b-b b-light m-b">
                <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a> Chat
            </div>
            <div class="row-row">
                <div class="cell">
                    <div class="cell-inner padder">
                        <!-- chat list -->
                        <div class="m-b">
                            <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="...">
                            </a>
                            <div class="clear">
                                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                    <span class="arrow left pull-up"></span>
                                    <p class="m-b-none">Hi John, What's up...</p>
                                </div>
                                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
                            </div>
                        </div>
                        <div class="m-b">
                            <a href class="pull-right thumb-xs avatar"><img src="/img/a3.jpg" class="img-circle" alt="...">
                            </a>
                            <div class="clear">
                                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                                    <span class="arrow right pull-up arrow-light"></span>
                                    <p class="m-b-none">Lorem ipsum dolor :)</p>
                                </div>
                                <small class="text-muted">1 minutes ago</small>
                            </div>
                        </div>
                        <div class="m-b">
                            <a href class="pull-left thumb-xs avatar"><img src="/img/a2.jpg" alt="...">
                            </a>
                            <div class="clear">
                                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                    <span class="arrow left pull-up"></span>
                                    <p class="m-b-none">Great!</p>
                                </div>
                                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
                            </div>
                        </div>
                        <!-- / chat list -->
                    </div>
                </div>
            </div>
            <div class="wrapper m-t b-t b-light">
                <form class="m-b-none">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Say something">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">SEND</button>
                  </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- / aside right -->

    <!-- footer -->
   <!--  <div class="app-footer wrapper b-t bg-light">
        <span class="pull-right">{[{app.version}]} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span> &copy; 2015 Copyright.
    </div> -->
    <!-- / footer -->

</div>
<script>
    var API_URL = '<?php echo $this->config->application->apiURL; ?>';
</script>
<!-- JS -->
<?php echo $this->tag->javascriptInclude('js/jquery/jquery.min.js'); ?>
<!-- angular -->
<?php echo $this->tag->javascriptInclude('js/angular/angular.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-cookies.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-animate.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-ui-router.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-translate.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ngStorage.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-load.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-jq.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-validate.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/ui-bootstrap-tpls.min.js'); ?>
<!-- added -->
<?php echo $this->tag->javascriptInclude('js/angular/angular-route.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-route.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/angular-sanitize.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular/checklist-model.js'); ?>


<!-- APP -->
<?php echo $this->tag->javascriptInclude('js/app.js'); ?>
<?php echo $this->tag->javascriptInclude('js/services.js'); ?>
<?php echo $this->tag->javascriptInclude('js/controllers.js'); ?>
<?php echo $this->tag->javascriptInclude('js/filters.js'); ?>
<?php echo $this->tag->javascriptInclude('js/directives.js'); ?>
<?php echo $this->tag->javascriptInclude('js/ckeditor/ckeditor.js'); ?>
<?php echo $this->tag->javascriptInclude('js/ckeditor/styles.js'); ?>
<?php echo $this->tag->javascriptInclude('js/ckeditor/adapters/jquery.js'); ?>


<!-- SLIDER IMAGE -->
<?php echo $this->tag->javascriptInclude('js/sliderimage/angular-file-upload-shim.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/angular-file-upload.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/upload.js'); ?>
<?php echo $this->tag->javascriptInclude('js/jquery/chosen/chosen.js'); ?>
<?php echo $this->tag->javascriptInclude('js/jquery/chosen/chosen.jquery.js'); ?>


<!-- jquery fileuploader -->
<?php echo $this->tag->javascriptInclude('js/sliderimage/vendor/jquery.ui.widget.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/load-image.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/canvas-to-blob.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.iframe-transport.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-process.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-image.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-audio.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-video.js'); ?>
<?php echo $this->tag->javascriptInclude('js/sliderimage/jquery.fileupload-validate.js'); ?>

<!-- tags -->
<?php echo $this->tag->javascriptInclude('js/tokeninput/src/jquery.tokeninput.js'); ?>
<?php echo $this->tag->javascriptInclude('js/select2/dist/js/select2.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/materialize.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/select2/dist/js/select2.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/angular-xeditable/js/xeditable.js'); ?>

<!-- video-->

<?php echo $this->tag->javascriptInclude('js/video/projekktor-1.3.09.min.js'); ?>





</body>

</html>
