<?php echo $this->getContent(); ?>
<style type="text/css">
  .label_profile_pic {
   display: block;
   margin:10px 0;
 }
 .create-proj-thumb{
  text-align: center;
}
.create-proj-thumb label{
  font-size: 12px;
}
.create-proj-thumb img{
  max-width: 100%;
}
.propic {
  position: relative;
}
.modal.modal-wide .modal-dialog {
  width: 90%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}
</style>
<style type="text/css">
  .containerimagefill1{
    height: 190px;
    width: 100%;
    overflow: hidden;
    background-size: cover;
    background-position: center center;
    float:left;
    margin: 0px 10px 10px 0px;
  }
</style>

<script type="text/ng-template" id="mediagallery.html">
 <div ng-include="'/tpl/mediagallery.html'"></div>
</script>
<script type="text/ng-template" id="imgurl.html">
  <form class="bs-example form-horizontal"  name="form" novalidate>
    <div class="modal-header">
      Image Url
    </div>
    <div class="col wrapper">
      <small class="text-muted">Image URL </small>
      <input type="text" class="form-control" ng-model="imgpath">
    </div>
    <div class="modal-footer"  ng-hide="process==true">
      <button class="btn btn-default" ng-click="cancel()">Close</button>
    </div>
  </form>
</script>

<script type="text/ng-template" id="previewletter.html">
 <div ng-include="'/tpl/previewletter.html'"></div>
</script>
<script type="text/ng-template" id="editMemberModal.html">
    <form class="bs-example form-horizontal" name="form" novalidate>
        <div class="modal-header">
            <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
        </div>
        <div class="modal-body" ng-show="process==true">
            <p ng-show="success==false">Updating user profile record.</p>
            <p ng-show="success==true">Success!</p>
        </div>

        <div class="modal-body" ng-show="process==false">
          ARE YOU SURE YOU WANT TO DELETE?
      </div>

      <div class="modal-footer" ng-hide="process==true">                  
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>

</form>

</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Edit News Letter</h1>
    <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updatenewsletter(newsletter)" name="form">
    <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            News Letter Information
                        </div>
                        <div class="panel-body">
                            <label class="col-sm-2 control-label">
                                Letter Title
                            </label>
                            <div class="col-sm-10">
                                <!-- ngIf: user.usernametaken == true -->
                                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="newsletter.title" required="required">
                            </div>
                            <br>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <label class="col-sm-2 control-label"><label for="title">Date</label> </label>
                            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
                              <div class="input-group w-md">
                                <span class="input-group-btn">
                                    <input type="hidden" ng-model='newsletter.date2'>
                                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newsletter.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">News Content</label>
                        <div class="col-sm-10">

                            <textarea class="ck-editor" ng-model="newsletter.body">
                               



                            </textarea>
                            <br>

                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <a class="btn btn-default btn-block" ng-click = "previewletter(newsletter.body)"><i class="fa fa-bars pull-left"></i> Preview News Letter</a>

                        </div>
                    </div>

                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
            </div>

             <div class="col-sm-4">
              <div class="panel ">
                <div class="panel-heading">
                  <a class="btn btn-default btn-xs pull-right" ng-click="mediaGallery()"><i class="fa  fa-folder-open"></i> Select from Recent Upload</a>
                  <span class="h4">News Baner</span>
                </div>
                <div class="loader" ng-show="imageloader">
                  <div class="loadercontainer">
                    <div class="spinner">
                      <div class="rect1"></div>
                      <div class="rect2"></div>
                      <div class="rect3"></div>
                      <div class="rect4"></div>
                      <div class="rect5"></div>
                    </div>
                    Uploading your images please wait...
                  </div>
                </div> 

                <div class="panel-body" ng-show="imagecontent">
                  <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
                    <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)">{[{ imgAlerts.msg }]}</alert>
                  </div>
                  <div class="col-sm-12 create-proj-thumb">
                    <img src="{[{newsletter.banner}]}">
                  </div>
                  <div class="col-sm-12 propic create-proj-thumb">
                    <br><br>
                    <input type="hidden" ng-model="newsletter.banner" ng-value="newsletter.banner =projImg">
                    <div class=" border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false">
                      <a href="">Choose an image from your computer</a><br>
                      <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
                      <label>At least 1024x768 pixels</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="panel-body">
        <footer class="panel-footer text-right bg-light lter">
            <button type="button" class="btn btn-default" ui-sref="managenewsletter">Cancel</button>
            <button type="submit" class="btn btn-success">Save</button>
        </footer>
    </div>
</div>

</div>
</fieldset>
</form>

