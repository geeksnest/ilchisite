<?php echo $this->getContent(); ?>
<style type="text/css">
    .containerimagefill1{
        height: 190px;
        width: 100%;
        overflow: hidden;
        background-size: cover;
        background-position: center center;
        float:left;
        margin: 0px 10px 10px 0px;
    }

</style>
<script type="text/ng-template" id="editMemberModal.html">
    <form class="bs-example form-horizontal" name="form" novalidate>
        <div class="modal-header">
            <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
        </div>
        <div class="modal-body" ng-show="process==true">
            <p ng-show="success==false">Updating user profile record.</p>
            <p ng-show="success==true">Success!</p>
        </div>
        <div class="modal-body" ng-show="process==false">
          ARE YOU SURE YOU WANT TO DELETE?
      </div>
      <div class="modal-footer" ng-hide="process==true">                  
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
        <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>
</form>
</script>
<script type="text/ng-template" id="previewletter.html">
 <div ng-include="'/tpl/previewletter.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">Create News Letter</h1>
    <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-scope ng-invalid ng-invalid-required" ng-submit="saveNewsLetter(newsletter)" name="formnl">
    <fieldset ng-disabled="isSaving">
        <div class="wrapper-md">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading font-bold">
                            News Letter Information
                        </div>
                        <div class="panel-body">
                            <label class="col-sm-2 control-label">
                                Letter Title
                            </label>
                            <div class="col-sm-10">
                                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="newsletter.title" required="required">
                            </div>
                            <br>
                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <label class="col-sm-2 control-label"><label for="title">Date</label> </label>
                            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
                              <div class="input-group w-md">
                                <span class="input-group-btn">
                                    <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newsletter.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                                    <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">News Content</label>
                        <div class="col-sm-10">

                            <textarea class="ck-editor" ng-model="newsletter.body">
                               



                            </textarea>
                            <br>

                            <div class="line line-dashed b-b line-lg pull-in"></div>
                            <a class="btn btn-default btn-block" ng-click = "previewletter(newsletter.body)"><i class="fa fa-bars pull-left"></i> Preview News Letter</a>

                        </div>
                    </div>

                    <div class="line line-dashed b-b line-lg pull-in"></div>
                </div>
            </div>
            



            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading font-bold">
                        News Baner
                    </div>
                    <div class="panel-body">
                        <img ng-show="newsletter.banner" src="<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ newsletter.banner }]}" style="width: 100%">
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                        <input type="text" id="banner" name="banner" class="form-control" ng-model="newsletter.banner"  placeholder="Paste Banner link here..." required="required">
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    </div>
                </div>
            </div>


        </div>
    </div>

</div>


<div class="row">
    <div class="panel-body">
        <footer class="panel-footer text-right bg-light lter">
            <button type="button" class="btn btn-default" ng-click="reset()">Clear</button>
            <button type="submit" class="btn btn-success">Submit</button>
        </footer>
    </div>
</div>

</div>
</fieldset>
</form>

<div class="wrapper-md ">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading font-bold"> Upload New Photo</div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <div class="widget">
                        <div class="widget-content">
                            <div class="padd">
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Add photo...</span>
                                    <input id="digitalAssets" type="file" name="files[]" multiple>
                                </span>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data">
                                    <div class="gallery digital-assets-gallery">
                                        <div class="clearfix"></div>
                                    </div>
                                </form>

                                <div class="col-sm-4" ng-repeat="data in data">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="panel-heading">
                                                <ul class="nav nav-pills pull-right">
                                                    <li>
                                                        <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dletphoto(dlt)" name="form" enctype="multipart/form-data">
                                                            <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.id" class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
                                                            <div class="buttonPanel{[{ data.id }]}">
                                                                <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                                                            </div>
                                                        </form>
                                                    </li>
                                                </ul>
                                                <br>
                                                <br>
                                            </div>
                                           <!--  <input type="text" class="form-control" ng-model="data.imgpath"> -->

                                            <div class="input-group m-b">
                                              <span class="input-group-btn">
                                                  <button class="btn btn-default" type="button">Banner</button>
                                              </span>
                                              <input type="text" class="form-control" ng-model="data.vanner" ng-value="data.vanner = data.imgpath">
                                          </div>
                                          <div class="input-group m-b">
                                              <span class="input-group-btn">
                                                  <button class="btn btn-default" type="button">URL</button>
                                              </span>
                                              <input type="text" class="form-control" ng-model="data.img" ng-value="data.img ='<?php echo $this->config->application->apiURL; ?>/images/featurebanner/'+data.imgpath ">
                                          </div>

                                          
                                          <div class="line line-dashed b-b line-lg pull-in"></div>
                                          <div class="containerimagefill1" style="background-image: url('<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ data.imgpath }]}')">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
</div>