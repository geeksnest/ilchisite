<?php
$disqus = new DisqusAPI('ofwPV1tvlOUVw5nbiXxg901NAV6KWGuByESwKTZDL7hjjxjBiKBkmc3hJXc7Jmic');

//print_r($disqus->forums->listThreads(array('forum'=>'ilchilee')));


$getginfo = $disqus->forums->listThreads(array('forum'=>'ilchilee'));
$total = 0;

foreach ($getginfo as $key => $value) {

 $total = $total + $getginfo[$key]->posts;
}

?>
<div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <h1 class="m-n font-thin h3 text-black">Dashboard</h1>
          <small class="text-muted">ILCHI Content Management System</small>
        </div>
      </div>
    </div>
    <!-- / main header -->
    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
      <!-- stats -->
      <div class="row">
        <div class="col-md-5">
          <div class="row row-sm text-center">
            <div class="col-xs-6">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">{[{newscount}]}</div>
                <span class="text-muted text-xs">Total News</span>
                <span class="bottom text-right">
                  <i class="fa fa-map-marker text-muted m-r-sm"></i>
                </span>
              </div>
            </div>
            <div class="col-xs-6">
              <a href class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block"><?php echo  $total; ?></span>
                <span class="text-muted text-xs">Total News Comment</span>
                <span class="bottom text-right">
                  <i class="fa fa-user text-muted m-r-sm"></i>
                </span>
              </a>
            </div>
            <div class="col-xs-6">
              <a href class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block">{[{videocount}]}</span>
                <span class="text-muted text-xs">Total Video's</span>
                <span class="bottom text-right">
                  <i class="fa fa-user text-muted m-r-sm"></i>
                </span>
              </a>
            </div>
              <div class="col-xs-6">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">{[{imgcount}]}</div>
                <span class="text-muted text-xs">Total Uploaded Photo's ({[{imgsize}]} mb)</span>
                <span class="bottom text-right">
                  <i class="fa fa-map-marker text-muted m-r-sm"></i>
                </span>
              </div>
            </div>
            <div class="col-xs-12 m-b-md">
              <div class="r bg-light dker item hbox no-border">
                <div class="col w-xs v-middle hidden-md">
                  <div ng-init="data1=[60,40]" ui-jq="sparkline" ui-options="{[{data1}]}, {type:'pie', height:40, sliceColors:['{[{app.color.warning}]}','#fff']}" class="sparkline inline"></div>
                </div>
                <div class="col dk padder-v r-r">
                  <div class="text-primary-dk font-thin h1"><span>{[{visitcount}]}</span></div>
                  <span class="text-muted text-xs">Ilchi Site User's Visit</span>
                </div>
              </div>
            </div>

          </div>
        </div>
      <div class="col-md-7">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">Ilchi Site Monthly User's Visit</div>
          <div class="panel-body">
            <div ui-jq="plot" ui-options="
              [
                { data: {[{d}]}, points: { show: true, radius: 6}, splines: { show: true, tension: 0.45, lineWidth: 5, fill: 0 } }
              ], 
              {
                colors: ['{[{app.color.info}]}'],
                series: { shadowSize: 3 },
                xaxis:{ 
                  font: { color: '#ccc' },
                  position: 'bottom',
                  ticks: {[{month}]}
                },
                yaxis:{ font: { color: '#ccc' } },
                grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
                tooltip: true,
                tooltipOpts: { content: '%x.1 is %y.4',  defaultTheme: false, shifts: { x: 0, y: 20 } }
              }
            " style="height:240px" >
            </div>
          </div>
        </div>
      </div>

      </div>
      <!-- / stats -->

     
      <!-- New service -->
      <div class="panel hbox hbox-auto-xs no-border">
        <div class="col wrapper">
          <div class="panel wrapper">
            <label class="i-switch bg-warning pull-right" ng-init="showSpline=true">
              <input type="checkbox" ng-model="showSpline">
              <i></i>
            </label>
            <h4 class="font-thin m-t-none m-b-none text-primary-lt">System Performance</h4>
            <span class="m-b block text-sm text-muted">Service report of this year (updated 1 hour ago)</span>
            <div ui-jq="plot" ui-refresh="showSpline" ui-options="
            [
            { data: {[{d0_1}]}, label:'Memmory', points: { show: true, radius: 1}, splines: { show: showSpline, tension: 0.4, lineWidth: 1, fill: 0.8 } },
            { data: {[{d0_2}]}, label:'CPU', points: { show: true, radius: 1}, splines: { show: showSpline, tension: 0.4, lineWidth: 1, fill: 0.8 } }
            ], 
            {
            colors: ['{[{app.color.info}]}', '{[{app.color.primary}]}'],
            series: { shadowSize: 3 },
            xaxis:{ font: { color: '#a1a7ac' } },
            yaxis:{ font: { color: '#a1a7ac' } },
            grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#dce5ec' },
            tooltip: true,
            tooltipOpts: { content: '%s of %x.1 is %y.4',  defaultTheme: false, shifts: { x: 10, y: -25 } }
          }
          " style="height:246px" >
        </div>
      </div>
    </div>
     <style type="text/css">
     td{
      color: #444;
     }
    </style>
     <div class="col wrapper-lg w-lg bg-light dk r-r">
          <h4 class="font-thin m-t-none m-b">Reports</h4>
          <div class="">
           
            <div class="text-center-folded">
              <span class="pull-right text-info">{[{permem}]}%</span>
              <span class="hidden-folded">Server Memory Usage</span>
            </div>
           <!--   <progressbar id="" value="" class="progress-xs m-t-sm bg-white" animate="true" type="info"></progressbar> -->
<div class="progress-xs m-t-sm bg-white progress " id="progbar" type="info" >
  <div class="progress-bar progress-bar-info"     style="width: {[{permem}]}%"></div>
</div>
             <div class="text-center-folded">
              <span class="pull-right text-primary">{[{percpu}]}%</span>
              <span class="hidden-folded">Server Cpu Usage</span>
            </div>
          <!--   <progressbar value="60" class="progress-xs m-t-sm bg-white" animate="true" type="primary"></progressbar> -->
<div class="progress-xs m-t-sm bg-white progress " type="info">
  <div class="progress-bar progress-bar-info"    style="width: {[{percpu}]}%;"></div>
</div>
           
          </div>
        </div>
  </div>
      <!-- / Newservice -->
    </div>
  </div>
  <!-- / main -->
  
</div>