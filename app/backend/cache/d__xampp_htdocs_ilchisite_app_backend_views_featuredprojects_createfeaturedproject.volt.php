<?php echo $this->getContent(); ?>
<style type="text/css">
  .containerimagefill1{
    height: 190px;
    width: 100%;
    overflow: hidden;
    background-size: cover;
    background-position: center center;
    float:left;
    margin: 0px 10px 10px 0px;
  }

</style>

<script type="text/ng-template" id="editMemberModal.html">

  <form class="bs-example form-horizontal" name="form" novalidate>

    <div class="modal-header">
      <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>

    <div class="modal-body" ng-show="process==true">
      <p ng-show="success==false">Updating user profile record.</p>
      <p ng-show="success==true">Success!</p>
    </div>

    <div class="modal-body" ng-show="process==false">
    	ARE YOU SURE YOU WANT TO DELETE?
    </div>

    <div class="modal-footer" ng-hide="process==true">                  
      <button class="btn btn-default" ng-click="cancel()">Cancel</button>
      <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>

  </form>

</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Featured Project</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="form">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Featured Project Information
            </div>
            <div class="panel-body">

              <!-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->

              <label class="col-sm-2 control-label"><label for="title">Title</label> </label>
              <div class="col-sm-10">
                <!-- ngIf: user.usernametaken == true -->
                <input type="text" id="title" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.title" required="required" ng-keyup = "onpagetitle(page.title)">
                <br>
                <!-- <b>Page Slugs: </b><input type="hidden" ng-model="page.slugs" ><span ng-bind="page.slugs"></span> -->
              </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>


              <div class="form-group">
                <label class="col-sm-2 control-label">Body Content</label>
                <div class="col-sm-10">

                 <textarea class="ck-editor" ng-model="page.body" required></textarea>
                 <br>
               </div>
             </div>


             <div class="line line-dashed b-b line-lg pull-in"></div>

             <div class="form-group">
              <label class="col-sm-2 control-label">Meta Title</label>
              <div class="col-sm-10">
                <input type="text" id="metatitle" name="metatitle" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metatitle" required="required" >
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Meta Description</label>
              <div class="col-sm-10">
                <input type="text" id="metadescription" name="metadescription" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.metadesc" required="required" >
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Meta Keyword</label>
              <div class="col-sm-10">
                <input type="text" id="metakeyword" name="metakeyword" class="form-control  ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.keyword" required="required" >
              </div>
            </div>


            <!-- testtttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt -->          

            <!-- testtttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt -->

            <br>

            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="checkbox">
              <label class="i-checks">
                <input type="checkbox" value="" ng-model='page.check'>
                <i></i>
                Active Project
              </label>
            </div>

            <div class="checkbox">
              <label class="i-checks">
                <input type="checkbox" value="" ng-model='page.check2'>
                <i></i>
                Publish Project
              </label>
            </div>

          </div>       
        </div>
      </div>

      <div class="col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading font-bold">
            Featured Project Cover Photo
          </div>
          <div class="panel-body">
            <?php 
            $trueimage = $this->config->application->apiURL."/images/featurebanner/{[{ page.banner }]}";
            $defaultimage = $this->config->application->apiURL."/images/default.png";
            
            
            
            ?>
            <img ng-show="page.banner" ng-src="<?php echo $trueimage; ?>" err-src="<?php echo $defaultimage; ?>" style="width: 100%">
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <input type="text" id="banner" name="banner" class="form-control" ng-model= "page.banner" ng-change = "banners(page.banner)" placeholder="Paste cover photo link here..." required="required">
            <div class="line line-dashed b-b line-lg pull-in"></div>

          </div>
        </div>
      </div>

    </div>

    <div class="row" >
     <div class="panel-body">
       <footer class="panel-footer text-right bg-light lter">
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-default" ng-click="reset()">Clear</button>
      </footer>
    </div>

  </form>

  <div class="wrapper-md ">
    <div class="row">
      <div class="panel panel-default">
        <div class="panel-heading font-bold"> Upload Cover Photo</div>

        <div class="panel-body">
          <div class="col-lg-12">

            <div class="widget">
              <div class="widget-head">
                <div class="widget-icons pull-right">
                  <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                  <a href="#" class="wclose"><i class="icon-remove"></i></a>
                </div>  
                <div class="clearfix"></div>
              </div>


              <div class="widget-content">
                <div class="padd">
                  <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add Photo...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input  id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                  </span>
                  <!-- The global progress bar -->

                  <div class="line line-dashed b-b line-lg pull-in"></div>

                  <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                  </div>
                  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data"> 
                   <div class="gallery digital-assets-gallery"> 

                    <div class="clearfix"></div>
                  </div>
                </form>

                <div class="col-sm-4" ng-repeat="data in data">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="panel-heading">
                        <ul class="nav nav-pills pull-right">
                          <li>
                            <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="huhuClick(dlt)" name="form" enctype="multipart/form-data">
                              <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.id"class="form-control" placeholder="{[{ data.id }]}" ng-model="dlt.id">
                              <div class="buttonPanel{[{ data.id }]}">
                               <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                             </div>
                           </form>
                         </li>
                       </ul>
                       <br>
                       <br>
                       <input type="text" class="form-control"  ng-model="data.imgpath">
                       <div class="line line-dashed b-b line-lg pull-in"></div>
                       <div class="containerimagefill1" style="background-image: url('<?php echo $this->config->application->apiURL; ?>/images/featurebanner/{[{ data.imgpath }]}');">
                       </div>
                     </div>



                   </div>
                 </div>
               </div>
             </div>
             <div class="widget-foot">
             </div>
           </div>
         </div>  
       </div>       
     </div>
   </div>
 </div>
</div>
</div>

</div>

</div>
</fieldset>
