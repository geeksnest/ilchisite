<?php echo $this->getContent(); ?>
<div id="Scrollup"></div>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Terms & Conditions</h1>
  <a id="top"></a>
</div>
<div class="panel-body">
    <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <textarea class="ck-editor" ng-model="term"></textarea>
    <footer class="panel-footer text-right bg-light lter">
    <button type="submit" class="btn btn-success" ng-click="save(term)">Save</button>
  </footer>
</div>
