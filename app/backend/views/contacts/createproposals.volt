{{ content() }}
<script type="text/ng-template" id="editMemberModal.html">
  <form class="bs-example form-horizontal" name="form" novalidate>
    <div class="modal-header">
      <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>
    <div class="modal-body" ng-show="process==true">
      <p ng-show="success==false">Updating user profile record.</p>
      <p ng-show="success==true">Success!</p>
    </div>
    <div class="modal-body" ng-show="process==false">
      ARE YOU SURE YOU WANT TO DELETE?
    </div>
    <div class="modal-footer" ng-hide="process==true">                  
      <button class="btn btn-default" ng-click="cancel()">Cancel</button>
      <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>
  </form>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create Proposals</h1>
  <a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="form" enctype="multipart/form-data">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Proposal Information
            </div>
            <div class="panel-body">

              <label class="col-sm-2 control-label"><label for="title">Name</label> </label>
              <div class="col-sm-10">

                <input type="text" id="name" name="name" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.name" required="required" placeholder="Enter your Name">
                
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <label class="col-sm-2 control-label"><label for="title">Email Address</label> </label>
              <div class="col-sm-10">

                <input type="email" id="email" name="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-email" placeholder="Enter your Email Address" ng-model="page.email" required="">

              </div>

              <br>
              <div class="line line-dashed b-b line-lg pull-in"></div>

              <label class="col-sm-2 control-label"><label for="title">Company</label> </label>
              <div class="col-sm-10">

                <input type="text" id="company" name="company" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="page.company" required="required" placeholder="Enter your Company">
                
              </div>

              <div class="line line-dashed b-b line-lg pull-in"></div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Body Content</label>
                <div class="col-sm-10">

                 <textarea id="proposalbody" name="proposalbody" class="ck-editor" ng-model="page.body"></textarea>

               </div>
             </div>

             <div class="line line-dashed b-b line-lg pull-in"></div>


             <div class="wrapper-md ">
              <div class="row">
                <div class="panel panel-default">
                  <div class="panel-heading font-bold"> Attach Files <label class="control-label">(images/PDF/.doc/.xls/.ppt)</label></div>
                  <div class="panel-body">
                    <div class="col-lg-12">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                          </div>  
                          <div class="clearfix"></div>
                        </div>


                        <div class="widget-content">
                          <div class="padd">           
                            <input  ui-jq="filestyle" class="form-control" id="digitalAssets" type="file" name="files[]" accept="application/pdf,image/*,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.presentationml.presentation">

                            <!-- The global progress bar -->

                            <div class="line line-dashed b-b line-lg pull-in"></div>

                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>


                          </div>
                          <div class="widget-foot">
                          </div>
                        </div>
                      </div>  
                    </div>       
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
    <div class="row" >
     <div class="panel-body">
       <footer class="panel-footer text-right bg-light lter">
        <button type="button" class="btn btn-default" ng-click="reset()">Cancel</button>
        <button type="submit"   class="btn btn-success">Submit</button>
      </footer>
    </div>

  </form>
</div>
</div>
</fieldset>
// <script type="text/javascript">
                   /* File Uploader */
var BASE_URL = 'http://ilchiapi/';
/* File Uploader */
$(function () {
    'use strict';
    var newArray = new Array();
    $('#digitalAssets').fileupload({
        url: BASE_URL + 'server/php/index.php',
        dataType: 'json',
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        limitMultiFileUploads: 4,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $.get( BASE_URL + 'proposals/ajaxfileuploader/'+ file.name + '/Sample description' ).done(function( data ) {
                  $('.digital-assets-gallery').prepend(data);
                  console.log(data);
                });
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
 </script>