{{ content() }}

<script type="text/ng-template" id="deletepeacemap.html">
  <div ng-include="'/tpl/deletePeacemapModal.html'"></div>
</script>
<script type="text/ng-template" id="editpeacemap.html">
  <div ng-include="'/tpl/editpeacemapsModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Peacemark List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Peacemarks
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
         <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
        </div>     
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              <label class="i-checks m-b-none">
                <input type="checkbox"><i></i>
              </label>
            </th>
            <th style="width:25%">Title</th>
            <th style="width:40%">Description</th>
            <th style="width:25%">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="pm in data.data">
            <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
            <td>{[{ pm.title }]}</td>
            <td>{[{ pm.description }]}</td>
            <td>
              <a href="" ng-click="editpeacemap(pm.id)"><span class="label bg-warning" >Edit</span></a>
              <a href="" ng-click="deletepeacemap(pm.id, pm.title)"> <span class="label bg-danger">Delete</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a></li>
          </ul>   

    </footer>
  </div>
</div>