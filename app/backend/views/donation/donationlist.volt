{{ content() }}
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">List of Donations</h1> <div tester ng-click="openModal()">clickme</div>
  <a id="top"></a>
</div>
<script type="text/ng-template" id="addDonationModal.html">
            <div ng-include="'/tpl/addDonationModal.html'"></div>
          </script>
<div class="wrapper-md">
<div class="row text-center">
	<div class="col-xs-4">
              <div class="panel padder-v item">
                <div class="h1 text-info font-thin h1">${{amounts}}</div>
                <span class="text-muted text-xs">Total Donations</span>
                <span class="bottom text-right">
                  <i class="fa fa-map-marker text-muted m-r-sm"></i>
                </span>
              </div>
            </div>
	<div class="col-xs-4">
              <a href="" class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block">{{usersdonated}}</span>
                <span class="text-muted text-xs">User Donated</span>
                <span class="bottom text-right">
                  <i class="fa fa-user text-muted m-r-sm"></i>
                </span>
              </a>
            </div>
	<div class="col-xs-4">
<form name="updateamounts" novalidate>

		<input type="text" ng-model="donate.amount" name="amount" ng-value="{{donamounts}}" placeholder="Amount Donated: {{donamounts}}"> <br/>
		<input type="text" ng-model="donate.users" name="users" ng-value="{{dontotaltrans}}" placeholder="Users Donated: {{dontotaltrans}}"> <br/>
		<input type="button" value="Update" ng-click="updateAmounts(donate)">
</form>
            </div>
</div>
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Members
    </div>
    <div class="table-responsive">
      <input type="hidden">
     <table ui-jq="dataTable" ui-options='{
          data: {{ data }},
          aoColumns: [
            { mData: "transactionId" },
            { mData: "firstname" },
            { mData: "lastname" },
            { mData: "email" },
            { mData: "amount" },
            { mData: "datetimestamp" }
          ],
	  order: [[ 5, "desc" ]],
          "drawCallback": blablah()
        }' class="table table-striped m-b-none">
        <thead>
          <tr>
            <th  style="width:25%">TransactionID</th>
            <th  style="width:20%">Firstname</th>
            <th  style="width:25%">Lastname</th>
            <th  style="width:25%">Email</th>
            <th  style="width:25%">Amount</th>
            <th  style="width:25%">Timestamp (UTC)</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
