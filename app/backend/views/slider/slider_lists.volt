{{ content() }}

<script type="text/ng-template" id="updateAlbum.html">
  <div ng-include="'/tpl/updateAlbum.html'"></div>
</script>
<script type="text/ng-template" id="deleteSliderPHoto.html">
  <div ng-include="'/tpl/deleteSliderPHoto.html'"></div>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Slider Album </h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Slider Album
    </div>
    <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="SearchAlbum(album)" name="form" >
      <div class="row wrapper">
        <div class="col-sm-3">
          <div class="input-group">
            <input class="input-sm form-control" ng-model="album.query" placeholder="Search" type="text">
            <span class="input-group-btn">
              <!-- <button class="btn btn-sm btn-default" type="button">Go!</button> -->
              <button class="btn m-b-xs btn-sm btn-info btn-addon">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </form>
    <div class="table-responsive">
      <input type="hidden" ng-init='userdata = ""'>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
          
            <th>Album</th>
            <th>Album ID</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data">
            <td>{[{ user.album_name}]} </td>
            <td>{[{ user.album_id }]}</td>
            <td>
              <a href="" ng-click="updatepage(user.album_id )"><span class="label bg-warning" >Edit</span></a>
              <a href="" ng-click="deletepage(user.album_name)"> <span class="label bg-danger">Delete</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <ul class="pagination">
        <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a></li>
        <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
          <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
        </li>
        <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a></li>
      </ul>   


      
    </footer>
  </div>
</div>