{{ content() }}
<script type="text/ng-template" id="deleteSliderPHoto.html">
  <div ng-include="'/tpl/deleteSliderPHoto.html'"></div>
</script>
<script type="text/ng-template" id="setasslider.html">
  <div ng-include="'/tpl/setasslider.html'"></div>
</script>

<script type="text/ng-template" id="note.html">
  <div ng-include="'/tpl/note.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
 <button ng-show="setmebtn == true" class="btn btn-default pull-right" type="submit" ng-click="set()" >Set as Slider</button>
 <button ng-show="setmebtn1 == true" class="btn btn-default pull-right" type="submit" ng-click="set()" disabled="disabled">Current Active Slider</button>
 <h1 class="m-n font-thin h3">Update Album</h1>
 <a id="top"></a>
</div>

<div class="wrapper-md ">
  <div class="row">
    <div class="panel panel-default">

      <div class="panel-body">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-head">
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>  
              <div class="clearfix"></div>
            </div>


            <div class="widget-content">

              <div class="padd">
                <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="albumtTitle(album)" name="form" >

                 <label>Album Name</label>
                 <form class="m-b-none m-l-xl ng-pristine ng-valid">
                  <div class="input-group">
                    <input type="hidden" ng-model="name.album_name">
                    <input id="name" type="text" class="form-control"  value="{[{album_info.album_name}]}" >
                    <span ng-show="editbtn == true" class="input-group-btn">
                      <button class="btn btn-default" type="button" ng-click="edit(album_info.album_name)">Change Album Name</button>
                    </span>
                    <span ng-show="savebtn == true" class="input-group-btn">
                      <button class="btn btn-default" type="submit" ng-click="savename()" >Update</button>
                    </span>
                  </div>
                </form>
              </form>

              <div class="loader" ng-show="imageloader">
                <div class="loadercontainer">
                  <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>
                  </div>
                  Uploading your images please wait...
                </div>
              </div>

              <div ng-show="imagecontent">
                <div class="col-sml-12">
                  <div class="dragdropcenter">
                    <div ngf-drop ngf-select class="drop-box" 
                    ngf-drag-over-class="dragover" ngf-multiple="false" ngf-allow-dir="true"
                    accept="image/*,application/pdf" ngf-change="upload($files,{folderid:folderid, title:title})" >
                    Drop images here or click to upload <br>
                  </div>
                </div>
              </div>
              <alert ng-repeat="alert in alert" type="{[{ alert.type }]}" close="closeAlert1($index)">{[{ alert.msg }]}</alert>
              <div class="col-sm-6" ng-repeat="data in data">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="panel-heading">

                      <ul class="nav nav-pills pull-right">
                        <li>
                          <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltimg(dlt)" name="form" enctype="multipart/form-data">
                            <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                            <div class="buttonPanel{[{ data.imgid }]}">
                             <button class="btn m-b-xs btn-sm btn-info btn-addon"><i class="fa fa-trash-o"></i>Delete Photo</button>
                           </div>
                         </form>
                       </li>
                     </ul>
                     <div class="line line-dashed b-b line-lg pull-in"></div> 
                   </div>
                   <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="imgInfo(info)" name="form" enctype="multipart/form-data">
                     <fieldset ng-disabled="isSaving">
                       <img  style="width:100%;height:250px;position:relative; " src="{[{ data.imgpath }]}">
                       <input type="hidden" id="" name="imgid" ng-init="info.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="info.id">
                       <div class="line line-dashed b-b line-lg pull-in"></div>
                       <input type="hidden" id="" name="imgid" ng-init="info.albumid=albumid"class="form-control" placeholder="{[{ albumid }]}" ng-model="info.albumid">
                       <alert  ng-repeat="alert in alerterror" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

                       <div class="line line-dashed b-b line-lg pull-in"></div>
                       <div class="form-group">
                        <label class="col-sm-2 control-label">Ordered_No.</label>
                        <div class="col-sm-10">
                          <input type="text" id="" name="imgdescription" value="" class="form-control" ng-init="info.sort=data.sort"  ng-model="info.sort"  ng-pattern="/^[0-9]+(?:\.[0-9]+)?$/">
                        </div>
                      </div>
                      <div class="line line-dashed b-b line-lg pull-in"></div>
                      <div class="buttonPanel{[{ data.imgid }]}">
                        <button class="btn m-b-xs btn-sm btn-primary btn-addon"><i class="fa fa-plus" style='width=100%;'></i>Update Information</button>
                        <span ng-show="saveboxmessage == data.imgid"> <i class="fa fa-check"></i> Saved!</span>
                        <span ng-show="notification== data.imgid" style="color:red"> <i class="icon-close"></i> Number Already Exist</span>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>  
</div>       
</div>
</div>

