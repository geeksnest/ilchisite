

<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Menu Creator</h1>
	<a id="top"></a>
</div>
<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="savePage(page)" name="form">
	<fieldset ng-disabled="isSaving">
		<div class="wrapper-md">
			<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
			<div class="row">
				<div class="col-sm-4">
					<form method="POST" name="form" novalidate class="ng-pristine ng-valid">
						<div class="panel panel-default">
							<div class="panel-heading font-bold">
								Create Menu
							</div>
							<div class="panel-body">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Menu Name</label>
										<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required" name="menuName" ng-model="menu.name" required="">
									</div>
								</div>
								<div class="col-sm-12">
									<label>Select Page</label>
									<select name="menuPage" aria-controls="DataTables_Table_0" class="form-control m-b input-sm ng-invalid-required ng-pristine" ng-model="menu.page">
										<option value="{[{ data.pageid}]}" ng-repeat="data in pageinfo" label="">{[{ data.title }]}</option>
									</select>
								</div>
								<div class="col-sm-12" style="margin-top:15px;">
									<label>Menu Type</label>
									<script type="text/javascript">
										 $("#mainMenus").hide();
										function changeFunc() {
											var selectBox = document.getElementById("selectBox");
											var selectedValue = selectBox.options[selectBox.selectedIndex].value;
											if(selectedValue==1){
												$("#mainMenus").hide();
											}else{
												$("#mainMenus").show();
											};
										}
										var val=1;
										
									</script>
									<!-- <input type="text"  value="{[{inputCounter}]}"> -->
									<input type="text"  value="{[{idcont}]}">

									<select id="selectBox" onchange="changeFunc();" name="menuType" aria-controls="DataTables_Table_0" class="form-control input-sm ng-invalid-required ng-pristine" ng-model="menu.type" style="width:50%" >
										<option></option>
										<option value="1">Main Menu</option>
										<option value="0">Sub-Menu</option> 
									</select>
								</div>



								<div class="col-sm-12" id="mainMenus" style="margin-top:20px;">
									<div add-input class="add-input">
										<label>Main Menu</label>
										<select type="text" ng-model="menu.id" name="{[{subs.id}]}"  class="form-control input-sm ng-pristine" ng-change="addNewChoice(menu.name)" style="width:50%" >
												<option value="{[{ data.menu_id}]}" ng-repeat="data in menulist" >{[{ data.menu_name }]}</option>
										</select> 
									</div>
								</div>

								
								<div class="col-sm-4" style="margin-top:20px;">
									<input type="button" name="" id="abouteco" value="Submit"  class="btn btn-sm btn-primary" ng-click="save(menu)" ng-disabled="form.$invalid" />
								</div>
							</div>
							
						</div>
						
					</form>


				</div>
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading">
						Menu List
						</div>
						
						<div class="table-responsive">
							<table class="table table-striped b-t b-light">
								<thead>
									<tr>
										
										<th>Main Menu</th>
										<th>Main Sub-Menu</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="info in menulist">
										<td style="font-size:17px;color:#528ac2;">{[{ info.menu_name}]} </td>
										<td>
											<table>
											<tr ng-repeat="subinfo in submenu" >
													<td ng-show="subinfo.sub_id === info.menu_id" style="font-size:16px;color:#008a17;">
														{[{subinfo.menu_name}]}
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</tbody>
							</table>


						<footer class="panel-footer">
							
						</footer>
					</div>
				</div>
			</div>
		</div>
	</fieldset>
</form>

