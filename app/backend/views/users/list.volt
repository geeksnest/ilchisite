{{ content() }}
<script type="text/ng-template" id="deleteuser.html">
  <div ng-include="'/tpl/userdelete.html'"></div>
</script>
<script type="text/ng-template" id="updateuser.html">
  <div ng-include="'/tpl/userupdate.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">User List</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Users
    </div>
    <div class="row wrapper">
      <div class="col-sm-5 m-b-xs">
         <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
     <!--  <input type="hidden" ng-init='pagedata = {{ data }}'> -->
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="user in data.data">
            <td>{[{ user.firstname }]} {[{ user.lastname }]}</td>
            <td>{[{ user.email }]}</td>
            <td>{[{ user.username }]}</td>
            <td>
              <span ng-show="{[{ user.userLevel==1 }]}">Super Admin</span>
              <span ng-show="{[{ user.userLevel==2 }]}">Enabled</span>
              <span ng-show="{[{ user.userLevel==3 }]}">Disabled</span>
            <td>
              <?php if($ulevel == 1){ ?>
              <a href="" ng-click="updateuser(user.userid)" ng-hide="{[{ user.userLevel==3 }]}"><span class="label bg-warning">Edit</span></a>
              <a href="" ng-click="deleteuser(user.userid)" ng-hide="{[{ user.userLevel==1 }]}"><span class="label bg-danger">Delete</span></a>
              <a href="" ng-click="status(user.userid,user.userLevel)" ng-show="{[{ user.userLevel==3 }]}"><span class="label bg-success">Enable</span></a>
              <a href="" ng-click="status(user.userid,user.userLevel)" ng-show="{[{ user.userLevel==2 }]}"><span class="label bg-primary">Disable</span></a>
              <?php }else{ ?>
              <a href="" ng-click="updateuser(user.userid)" ng-hide="{[{ user.userLevel==1 }]}"><span class="label bg-warning">Edit</span></a>
              <a href="" ng-click="deleteuser(user.userid)" ng-hide="{[{ user.userLevel==1 }]}"><span class="label bg-danger">Delete</span></a>
              <a href="" ng-click="status(user.userid,user.userLevel)" ng-show="{[{ user.userLevel==3 }]}"><span class="label bg-success">Enable</span></a>
              <a href="" ng-click="status(user.userid,user.userLevel)" ng-show="{[{ user.userLevel==2 }]}"><span class="label bg-primary">Disable</span></a>
              <?php } ?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a></li>
          </ul>
    </footer>
     </div>
  </div>
</div>