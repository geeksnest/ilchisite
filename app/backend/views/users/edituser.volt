{{ content() }}
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Edit User</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="updateuser(user, 'ajaxSubmitResult1')" name="form">
<fieldset ng-disabled="isSaving">
  <div class="wrapper-md" ng-controller="FormDemoCtrl">
  <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        Account Information
      </div>
      <div class="panel-body">
          
          <div class="form-group">
            <div class="padd">
              <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Change Profile Picture</span>
              <input id="digitalAssets" type="file" name="files[]" multiple>
              </span>
              <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
              </div>
              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="profilephoto(propic)" name="form" enctype="multipart/form-data">
                <div class="gallery digital-assets-gallery">
                  <div class="clearfix"></div>
                </div>
              </form>

              <div ng-if="trueval == true">
                <input type="hidden" class="form-control" ng-model="user.img">
                <img style="width:110px;height:100px;" src="<?php echo $this->config->application->apiURL."/images/profile_picture/thumbnail/{[{ user.imgg }]}"; ?>">
              </div>

              <div ng-if="upload == true" ng-repeat="user in data">
                <div class="data panel-default">
                  <input type="hidden" class="form-control" ng-model="user.img">
                  <img style="width:110px;height:100px;" src="<?php echo $this->config->application->apiURL."/images/profile_picture/thumbnail/{[{ user.img }]}"; ?>">
                </div>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('username') }} </label>
            <div class="col-sm-10">
              <span class="label bg-danger" ng-if="user.usernametaken == true">Username already taken. <br/></span>
              {{ form.render('username') }} <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('emailaddress') }}</label>
            <div class="col-sm-10">
              <span class="label bg-danger" ng-if="user.emailtaken == true">Email Address already taken.</span>
              {{ form.render('emailaddress') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Enter old password to change password</label>
            <div class="col-sm-10">
              <input type="password" id="oldpword" name="oldpword" class="form-control" ng-model="user.oldpword">
              <input type="hidden" id="oldpwordtemp" name="oldpwordtemp" ng-model="user.oldpwordtemp">
              <input type="hidden" id="oldpwordtemp2" name="oldpwordtemp2" ng-model="user.oldpwordtemp2">
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>

          <div ng-if="user.oldpword">
          <div class="form-group">
            <label class="col-sm-2 control-label">New {{ form.label('password') }}</label>
            <div class="col-sm-10">
              <input type="password" id="password" name="password" class="form-control" ng-model="user.password" required>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('confirm_password') }}</label>
            <div class="col-sm-10">
                    <input type="password" id="confirm_password" name="confirm_password" class="form-control" ng-model="user.confirm_password" required>
                    <span ng-show='form.confirm_password.$error.validator'>Passwords do not match!</span>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>  
        </div>

      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading font-bold">
        User Profile
      </div>
      <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('firstname') }}</label>
            <div class="col-sm-10">
              {{ form.render('firstname') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('lastname') }}</label>
            <div class="col-sm-10">
              {{ form.render('lastname') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('birthday') }}</label>
            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
              <div class="input-group w-md">
                {{ form.render('birthday')  }}
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                  <input type="hidden" id="birthday2" name="birthday2" ng-model="user.birthday2">
                  <input type="hidden" id="bdaytemp" name="bdaytemp" ng-model="user.bdaytemp">
                  <input type="hidden" id="bdaytemp2" name="bdaytemp2" ng-model="user.bdaytemp2">
                </span>
              </div>
            </div>        </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Gender</label>
            <div class="col-sm-10">
              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                  <i></i>
                  Male
                </label>
              </div>
              <div class="radio">
                <label class="i-checks">
                  <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                  <i></i>
                  Female
                </label>
              </div>
            </div>
          </div> 
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">State</label>
            <div class="col-lg-10">
          <div class="col-sm-10">
              <select ui-jq="chosen" class="w-md" name="state" ng-model="user.state" required="required">
                  <optgroup label="Alaskan/Hawaiian Time Zone">
                      <option value="AK">Alaska</option>
                      <option value="HI">Hawaii</option>
                  </optgroup>
                  <optgroup label="Pacific Time Zone">
                      <option value="CA">California</option>
                      <option value="NV">Nevada</option>
                      <option value="OR">Oregon</option>
                      <option value="WA">Washington</option>
                  </optgroup>
                  <optgroup label="Mountain Time Zone">
                      <option value="AZ">Arizona</option>
                      <option value="CO">Colorado</option>
                      <option value="ID">Idaho</option>
                      <option value="MT">Montana</option><option value="NE">Nebraska</option>
                      <option value="NM">New Mexico</option>
                      <option value="ND">North Dakota</option>
                      <option value="UT">Utah</option>
                      <option value="WY">Wyoming</option>
                  </optgroup>
                  <optgroup label="Central Time Zone">
                      <option value="AL">Alabama</option>
                      <option value="AR">Arkansas</option>
                      <option value="IL">Illinois</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="OK">Oklahoma</option>
                      <option value="SD">South Dakota</option>
                      <option value="TX">Texas</option>
                      <option value="TN">Tennessee</option>
                      <option value="WI">Wisconsin</option>
                  </optgroup>
                  <optgroup label="Eastern Time Zone">
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="IN">Indiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="NH">New Hampshire</option><option value="NJ">New Jersey</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="OH">Ohio</option>
                      <option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>
                      <option value="VT">Vermont</option><option value="VA">Virginia</option>
                      <option value="WV">West Virginia</option>
                  </optgroup>
              </select>
            </div>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-lg-2 control-label">Country</label>
            <div class="col-lg-10">
              <p class="form-control-static">Soon, Now only in USA</p>
            </div>
          </div>
          
          <?php if($ulevel == 1){ ?>
          <div ng-if="user.userLevel==2">
          <div class="line line-dashed b-b line-lg pull-in"></div>
            <label class="col-lg-2 control-label">Roles</label>
            <div class="col-lg-10">
          <!-- $phql = 'SELECT Tblroles.rolePage,Tblroles.roleDescription, Tblroles.roleCode, Tblroles.roleGroup FROM Modules\Backend\Models\Userroles as Tbluserroles' .
          ' INNER JOIN Modules\Backend\Models\Roles as Tblroles ON Tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = 2'; -->
              {% for role in tblroles %}
                <label class="checkbox-inline">
                  {{ check_field(role.roleCode, 'value': role.roleCode, 'ng-model':'user.' ~ role.roleCode, 'id': role.roleCode) }}
                  {{ role.roleDescription }}
                </label> <br/>
              {% endfor  %}
              
            </div>
        </div>
        <?php } ?>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
              <button type="button" class="btn btn-default" ui-sref="userlist">Cancel</button>
              <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Update</button>
            </div>
          </div>
        
      </div>

  </div>
  </fieldset>
  </form>