{{ content() }}
<style type="text/css">
 .label_profile_pic {
   display: block;
   margin:10px 0;
 }
 .create-proj-thumb{
  text-align: center;
}
.create-proj-thumb label{
  font-size: 12px;
}
.create-proj-thumb img{
  max-width: 100%;
}
.propic {
  position: relative;
}
</style>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Create User</h1>
  <a id="top"></a>
</div>

<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="submitData(user)" name="form">
  <fieldset ng-disabled="isSaving">
    <div class="wrapper-md" ng-controller="FormDemoCtrl">
      <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>

      <div class="row">
        <div class="col-sm-8">
          <div class="panel panel-default">
            <div class="panel-heading font-bold">
              Account Information
            </div>
            <div class="panel-body">



          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('username') }} </label>
            <div class="col-sm-10">
              <span class="label bg-danger" ng-if="user.usernametaken == true">Username already taken. <br/></span>
              {{ form.render('username') }} <em class="text-muted">(allow 'a-zA-Z0-9', 4-10 length)</em>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('emailaddress') }}</label>
            <div class="col-sm-10">
              <span class="label bg-danger" ng-if="user.emailtaken == true">Email Address already taken.</span>
              {{ form.render('emailaddress') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('password') }}</label>
            <div class="col-sm-10">
              {{ form.render('password') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('confirm_password') }}</label>
            <div class="col-sm-10">
              {{ form.render('confirm_password') }}
              <span ng-show='form.confirm_password.$error.validator'>Passwords do not match!</span>
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div> 
        </div>
      </div>
    </div>

   <!--  <div class="col-sm-4">
      <div class="panel ">
        <div class="panel-heading">
          <span class="h4">Image</span>
        </div>
        <div class="loader" ng-show="imageloader">
          <div class="loadercontainer">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
            Uploading your images please wait...
          </div>
        </div> 

        <div class="panel-body" ng-show="imagecontent">
          <div class="col-sm-12 create-proj-thumb" ng-if="imageselected == false">
            <alert ng-repeat="imgAlerts in imgAlerts" type="{[{imgAlerts.type }]}" close="closeAlert($index)">{[{ imgAlerts.msg }]}</alert>
          </div>
          <div class="col-sm-12 create-proj-thumb">
            <img src="{[{user.img}]}">
          </div>
          <div class="col-sm-12 propic create-proj-thumb">
            <br><br>
            <input type="hidden" ng-model="user.img" ng-value="user.img =projImg">
            <div class=" border-dash browse-img-wrap" id="change-picture" accept='image/*' ngf-change="prepare(files)" ngf-select ng-model="files" ngf-multiple="false" >
              <a href="">Choose an image from your computer</a><br>
              <label>JPG, PNG, GIF or BMP | Maximum size of 2MB</label><br>
              <label>At least 1024x768 pixels</label>
            </div>
          </div>
        </div>
      </div>
    </div> -->
  </div>





  <div class="row">
    <div class="col-sm-8">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">
          User Profile
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('firstname') }}</label>
            <div class="col-sm-10">
              {{ form.render('firstname') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('lastname') }}</label>
            <div class="col-sm-10">
              {{ form.render('lastname') }}
            </div>
          </div>
          <div class="line line-dashed b-b line-lg pull-in"></div>
          <div class="form-group">
            <label class="col-sm-2 control-label">{{ form.label('birthday') }}</label>
            <div class="col-sm-10" ng-controller="DatepickerDemoCtrl">
              <div class="input-group w-md">
                {{ form.render('birthday')  }}
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                </span>
              </div>
            </div>        </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Gender</label>
              <div class="col-sm-10">
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Male" ng-model="user.gender" required="required">
                    <i></i>
                    Male
                  </label>
                </div>
                <div class="radio">
                  <label class="i-checks">
                    <input type="radio" name="gender" value="Female" ng-model="user.gender" required="required">
                    <i></i>
                    Female
                  </label>
                </div>
              </div>
            </div> 
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label">State</label>
              <div class="col-lg-10">
                <div class="col-sm-10">
                  <select ui-jq="chosen" class="w-md" name="state" ng-model="user.state" required="required">
                    <optgroup label="Alaskan/Hawaiian Time Zone">
                      <option value="AK">Alaska</option>
                      <option value="HI">Hawaii</option>
                    </optgroup>
                    <optgroup label="Pacific Time Zone">
                      <option value="CA">California</option>
                      <option value="NV">Nevada</option>
                      <option value="OR">Oregon</option>
                      <option value="WA">Washington</option>
                    </optgroup>
                    <optgroup label="Mountain Time Zone">
                      <option value="AZ">Arizona</option>
                      <option value="CO">Colorado</option>
                      <option value="ID">Idaho</option>
                      <option value="MT">Montana</option><option value="NE">Nebraska</option>
                      <option value="NM">New Mexico</option>
                      <option value="ND">North Dakota</option>
                      <option value="UT">Utah</option>
                      <option value="WY">Wyoming</option>
                    </optgroup>
                    <optgroup label="Central Time Zone">
                      <option value="AL">Alabama</option>
                      <option value="AR">Arkansas</option>
                      <option value="IL">Illinois</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="OK">Oklahoma</option>
                      <option value="SD">South Dakota</option>
                      <option value="TX">Texas</option>
                      <option value="TN">Tennessee</option>
                      <option value="WI">Wisconsin</option>
                    </optgroup>
                    <optgroup label="Eastern Time Zone">
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="IN">Indiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="NH">New Hampshire</option><option value="NJ">New Jersey</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="OH">Ohio</option>
                      <option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>
                      <option value="VT">Vermont</option><option value="VA">Virginia</option>
                      <option value="WV">West Virginia</option>
                    </optgroup>
                  </select>
                </div>
              </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Country</label>
              <div class="col-lg-10">
                <p class="form-control-static">Soon, Now only in USA</p>
              </div>
            </div>   
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <label class="col-lg-2 control-label">Roles</label>
            <div class="col-lg-10">
              {% for role in tblroles %}
              <label class="checkbox-inline">
                {{ check_field(role.roleCode, 'value':role.roleCode, 'ng-model':'user.' ~ role.roleCode) }}
                {{ role.roleDescription }}
              </label> <br/>
              {% endfor  %}
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>
            <footer class="panel-footer text-right bg-light lter">
                <button type="button" class="btn btn-default" ng-click="reset()">Cancel</button>
                <button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Submit</button>
            </footer>
          </div>
        </div>
      </div>
    </div>


  </fieldset>
</form>