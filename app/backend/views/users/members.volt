{{ content() }}
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">List of Registered Users</h1>
  <a id="top"></a>
</div>
<script type="text/ng-template" id="editMemberModal.html">
            <div ng-include="'/tpl/editMemberModal.html'"></div>
          </script>
<script type="text/ng-template" id="deleteMemberModal.html">
            <div ng-include="'/tpl/deleteMemberModal.html'"></div>
          </script>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Members
    </div>
<div class="row wrapper">
      <div class="col-xs-5 m-b-xs">
        <select class="input-xs form-control w-xs inline v-middle">
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="20">20</option>
          <option value="30">30</option>
        </select>           
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-3">
        <div class="input-group">
          <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <input type="hidden">
     <table class="table table-striped m-b-none">
        <thead>
          <tr>
            <th></th>
            <th  style="width:25%">Username</th>
            <th  style="width:20%">Firstname</th>
            <th  style="width:25%">Lastname</th>
            <th  style="width:25%">Birhtday</th>
            <th  style="width:25%">Email</th>
            <th  style="width:25%">Location</th>
            <th  style="width:25%">Zip Code</th>
            <th  style="width:25%"></th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="mem in data.data">
            <td><label class="i-checks m-b-none"><input name="post[]" type="checkbox"><i></i></label></td>
            <td>{[{ mem.username }]}</td>
            <td>{[{ mem.firstname }]} </td>
            <td>{[{ mem.lastname }]}</td>
            <td>{[{ mem.birthday }]}</td>
            <td>{[{ mem.email }]}</td>
            <td>{[{ mem.location }]}</td>
            <td>{[{ mem.zipcode }]}</td>
            <td>
              <a href=""><span class="label bg-warning" ng-click="edituser(mem.userid)" >Edit</span></a> <a href="" ng-click="deleteuser(mem.userid, mem.firstname)"> <span class="label bg-danger">Delete</span></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-6 text-left">
          <small class="text-muted inline m-t-sm m-b-sm">Total of {[{data.total_items}]} items</small>
        </div>
        <div class="col-sm-6 text-right text-center-xs">                
          <ul class="pagination">
            <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a></li>
            <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
              <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
            </li>
            <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a></li>
          </ul>
        </div>
     </div>
    </footer>
  </div>
</div>
