{{ content() }}
<script type="text/ng-template" id="deleteVideo.html">
  <div ng-include="'/tpl/deleteVideo.html'"></div>
</script>
<script type="text/ng-template" id="updatevideo.html">
  <div ng-include="'/tpl/updatevideo.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Video Gallery List</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="panel-heading">
      Manage Video's
    </div>
    <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="SearchAlbum(search)" name="form" >
      <div class="row wrapper">
        <div class="col-sm-3">
          <div class="input-group">
            <input class="input-sm form-control" ng-model="search.query" placeholder="Search" type="text">
            <span class="input-group-btn">
              <!-- <button class="btn btn-sm btn-default" type="button">Go!</button> -->
              <button class="btn m-b-xs btn-sm btn-info btn-addon">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </form>
    <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
</div>
</div> 

<style type="text/css">
  iframe{
    width:100%;
    height: 250px;
  }
</style>

<div class="col-lg-4" ng-repeat="video in data.data" style="max-height:430px;">
  <div class="panel panel-default">
 <!--    <div class="panel-heading font-bold">{[{ video.album}]}</div> -->
    <div class="panel-body">

  
        <button id="set{[{video.id}]}" ng-show="video.featvid == 0 " class="btn  btn-success" style="float:rigth:width:230px;" ng-click="video.featvid = 1; setasfeat(video.id)">
          Set As Featured Video
        </button>
        <button id="remove{[{video.id}]}"  ng-show="video.featvid == 1" class="btn  btn-danger" style="float:rigth:width:230px;" ng-click="video.featvid = 0; removefeat(video.id)">
          Remove from Featured Video List
        </button>




   <!--    <button id="set{[{video.id}]}"  class="btn  btn-success" style="float:rigth:width:230px;" ng-click="setasfeat(video.id)">
        Set As Featured Video
      </button>
      <button id="remove{[{video.id}]}"  class="btn  btn-danger" style="float:rigth:width:230px;" ng-click="removefeat(video.id)">
        Remove from Featured Video List
      </button> -->

      <div ng-bind-html="video.path"></div>
      <div class="m-b-md">
        <p class="h3 font-thin">{[{ video.title}]}</p>
        <!-- <div style="padding:10px;" ng-bind-html="video.description"></div> -->
      </div>
      <div class="row m-b">
        <div class="col-xs-6">
          <button class="btn  btn-sm btn-info" ng-click="update(video.id)"><i class="icon-note"></i> Edit</button>
          <button class="btn  btn-sm btn-danger" ng-click="deletevid(video.id)"><i class="icon-close"></i> Delete</button>

        </div>
      </div>
    </div>
  </div>
</div>





<div class="wrapper-md">
  <div class="panel panel-default col-sm-12 ">

    <footer class="panel-footer">
      <ul class="pagination">
        <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous"><a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a></li>
        <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
          <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
        </li>
        <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next"><a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a></li>
      </ul>   


      
    </footer>
  </div>
</div>
