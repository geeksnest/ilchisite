{{ content() }}

<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">Video Gallery</h1>
	<a id="top"></a>
</div>

<div class="wrapper-md ">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading font-bold"> Upload New Video's</div>
			<form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="save(info)" name="form">

				<div class="panel-body">
					<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
					<div class="col-lg-6">
						<input type="hidden" class="form-control" placeholder="" ng-model="info.id" required>
						<div class="form-group">
							Video Title: <input type="text" class="form-control" placeholder="" ng-model="info.title" required ng-keyup="onvideotitle(info.title)">
						<input type="hidden" ng-model="info.slugs">
						</div>
						<div class="form-group">
							Album Select : 
							<select class="form-control" ng-model="info.album" value="Featured Video" >
								<option value="1">Public Talks</option>
								<option value="2">Short Clips</option>
								<option value="3">Video Journal</option>
							</select>


						</div>


						<div class="form-group">
							<div class="col-lg-6">
								Location: <input type="text" class="form-control" placeholder="" ng-model="info.location" required>
							</div>
							<div class="col-lg-6">
								Duration: <input type="text" class="form-control" placeholder="" ng-model="info.duration" required>
							</div>
							<div class="col-lg-6" style="padding:10px;">
								Date: (month-day-year)<br> <!-- <input type="date" class="form-control" placeholder="" ng-model="video.date" required> -->

					
								<select ng-model="info.month" class="input-sm form-control w-sm inline v-middle ng-pristine ng-valid" style="width:105px">
									<?php $formonths = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'); 
									foreach ($formonths as $index => $formonth) {
									    if($index>=10){
									    	$addzero="";
									    }else{
									    	$addzero=0;
									    } 
										echo "<option value='".$addzero."".$index."'>".$formonth."</option>";
									}
									?>
								</select>

								<select ng-model="info.day" class="input-sm form-control w-sm inline v-middle ng-pristine ng-valid" style="width:80px">
									
									
									<?php for ($day=1; $day < 32; $day++) { if($day <= 9){ $days = '0'.$day; }else{ $days = $day; }
									echo "<option value='".$days."'>".$days."</option>";
								} ?>
								video
							</select>

							<select ng-model="info.year" class="input-sm form-control w-sm inline v-middle ng-pristine ng-valid" style="width:80px">
								<?php 
								$curyear="20".substr(date("y-m-d"),0,2);
								echo "<option ></option>";
								for ($year=1960; $year <= $curyear; $year++) { 
									echo "<option value='".$year."'>".$year."</option>";
								}?>
							</select>


						</div>
						<div class="col-lg-6">
							Language: <input type="text" class="form-control" placeholder="" ng-model="info.language" required>
						</div>
					</div>

				</div>
				<div class="col-lg-6">
					<div class="form-group">
						Select Upload Categories : 
						<select class="form-control" ng-model="info.option" ng-change="option(info.option)" >
							<option value="0">Embed Video</option>
							<option value="1">Upload VIdeo </option>
						</select>
					</div>
					<div ng-show="embed == true" class="form-group"  >
						Embed Video
						<input class="form-control"  type='text' ng-model='info.embed' ng-change='paste(video.embed)'>

					</div>
					<div ng-show="upload == true" class="form-group">
						<span class="btn  fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Upload Video...</span>
							<!-- The file input field used as target for the file upload widget -->
							<input  id="digitalAssets" type="file" name="files[]" multiple>
						</span>
						<!-- The global progress bar -->
						<div id="progress" class="progress">
							<div class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<input type="hidden" style="text-align:center;" ng-model="info.path"  value="" >
					<style type="text/css">
						iframe{
							width:100%;
							height: 315px;
						}
					</style>
					<div style="padding:10px;" ng-bind-html="vidpath"></div>
					<div>
						{[{ upVid.path}]}
					</div>

				</div>
				<div class="col-lg-12">
					<div class="form-group">
						Description
						<textarea class="ck-editor" ng-model="info.description" rows="2" required></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12 ">
						<a ui-sref="video_list" class="btn btn-default">Cancel</a>
						<button disabled="disabled" type="submit" class="btn btn-success" ng-disabled="form.$invalid">Update</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
