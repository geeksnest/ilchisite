
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  {{ stylesheet_link('css/bootstrap.css') }}
  {{ stylesheet_link('css/animate.css') }}
  {{ stylesheet_link('css/font-awesome.min.css') }}
  {{ stylesheet_link('css/simple-line-icons.css') }}
  <style type="text/css">
    @font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:300;src:local('Source Sans Pro Light'),local('SourceSansPro-Light'),url(../fonts/sourcesanspro/sourcesanspro-light.woff) format('woff')}@font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:400;src:local('Source Sans Pro'),local('SourceSansPro-Regular'),url(../fonts/sourcesanspro/sourcesanspro.woff) format('woff')}@font-face{font-family:'Source Sans Pro';font-style:normal;font-weight:700;src:local('Source Sans Pro Bold'),local('SourceSansPro-Bold'),url(../fonts/sourcesanspro/sourcesanspro-bold.woff) format('woff')}
  </style>
  {{ stylesheet_link('css/app.css') }}
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->


</head>

<body>
  <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController">
  <div class="m-b-lg">
  <a href class="navbar-brand block m-t"></a>
    <div class="wrapper text-center">
      <img src='/img/ilchilogo.png' style="width:300px;height:178px;">
    </div>
    <form name="form" class="form-validation" method="post" action="<?php echo $base_url.'/ilchiadmin';?>">
      <div class="text-danger wrapper text-center" ng-show="authError">
          {{ content() }}
      </div>
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          {{ text_field('username', 'size': "30", 'class': "form-control no-border", 'id': "inputUsername", 'placeholder': 'Username', 'ng-model': 'user.password', 'required':'required') }}
        </div>
        <div class="list-group-item">
          {{ password_field('password', 'size': "30", 'class': "form-control no-border", 'id': "inputPassword", 'placeholder': 'Password', 'ng-model': 'user.password', 'required': 'required') }}
        </div>
      </div>
      <button type="submit" class="btn btn-lg btn-primary btn-block btn-success" ng-click="login()" ng-disabled='form.$invalid'>Log in</button>
      <div class="text-center m-t m-b"><a href="/ilchiadmin/forgotpassword">Forgot password?</a></div>
      <div class="line line-dashed"></div>
      <p class="text-center"><small>Only the SuperAgent can access beyond this point.</small></p>
    </form>
  </div>
  <div class="text-center" ng-include="'tpl/blocks/page_footer.html'"></div>
</div>
<!-- JS -->
{{ javascript_include('js/jquery/jquery.min.js') }}
<!-- angular -->
{{ javascript_include('js/angular/angular.min.js') }}
{{ javascript_include('js/angular/angular-cookies.min.js') }}
{{ javascript_include('js/angular/angular-animate.min.js') }}
{{ javascript_include('js/angular/angular-ui-router.min.js') }}
{{ javascript_include('js/angular/angular-translate.js') }}
{{ javascript_include('js/angular/ngStorage.min.js') }}
{{ javascript_include('js/angular/ui-load.js') }}
{{ javascript_include('js/angular/ui-jq.js') }}
{{ javascript_include('js/angular/ui-validate.js') }}
{{ javascript_include('js/angular/checklist-model.js') }}
{{ javascript_include('js/angular/ui-bootstrap-tpls.min.js') }}

<!-- APP -->
  <script src="/js/app.js"></script>
  <script src="/js/services.js"></script>
  <script src="/js/controllers.js"></script>
  <script src="/js/filters.js"></script>
  <script src="/js/directives.js"></script>
</body>
</html>