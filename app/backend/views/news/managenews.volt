{{ content() }}

<script type="text/ng-template" id="deletenews.html">
    <div ng-include="'/tpl/deleteNewsModal.html'"></div>
</script>
<script type="text/ng-template" id="updatenews.html">
    <div ng-include="'/tpl/updatenewsModal.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3">News List</h1>
    <a id="top"></a>
</div>
<div class="wrapper-md">
<alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
    <div class="panel panel-default">
        <div class="panel-heading">
            Manage News
        </div>
        <div class="row wrapper">
            <div class="col-sm-5 m-b-xs">
                <div class="input-group">
                    <input class="input-sm form-control" placeholder="Search" type="text" ng-model="searchtext">
                    <span class="input-group-btn">
                    <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
                    </span>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t b-light">
                <thead>
                    <tr>
                        
                        <th style="width:50%">Title</th>
                        <th style="width:25%">Status</th>
                        <th style="width:25%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="mem in data.data">
                       
                        <td>{[{ mem.title }]}</td>
                        <td  ng-if="mem.status == 1"><span ng-class=""><span class="label bg-info m-l-xs" >Active</span></a></span>
                        </td>
                        <td  ng-if="mem.status == 0"><span ng-class=""><span class="label bg-danger">InActive</span></span>
                        </td>
                        <td>
                            <a href="" ng-click="updatenews(mem.newsid)"><span class="label bg-warning" >Edit</span></a>
                            <a href="" ng-click="deletenews(mem.newsid)"> <span class="label bg-danger">Delete</span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <ul class="pagination">
                <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous">
                    <a class="fa fa-chevron-left" href="" ng-click="paging(data.before)"></a>
                </li>
                <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
                    <a ng-click="paging(page.num)"> {[{ page.num }]}</a>
                </li>
                <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next">
                    <a href="" class="fa fa-chevron-right" ng-click="paging(data.next)"> </a>
                </li>
            </ul>

    </div>
</div>
</footer>
</div>
</div>
