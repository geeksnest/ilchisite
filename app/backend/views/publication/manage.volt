{{ content() }}
<script type="text/ng-template" id="deletePublication.html">
  <div ng-include="'/tpl/deletePublication.html'"></div>
</script>
<script type="text/ng-template" id="updatePub.html">
  <div ng-include="'/tpl/updatePub.html'"></div>
</script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Manage Publication</h1>
  <a id="top"></a>
</div>
<div class="wrapper-md">
    <div class="panel panel-default">
        <div class="panel-heading">
            Manage Publication
        </div>        
        <div class="row wrapper">
          <div class="col-sm-4">
            <div class="input-group">
              <input class="input-sm form-control" placeholder="Search" type="text" name="searchtext" ng-model="searchtext">
              <span class="input-group-btn">
                <button class="btn btn-sm btn-default" type="button" ng-click="search(searchtext)">Go!</button>
              </span>
            </div>
             <alert ng-repeat="alert in alerts" type="{[{ alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="col-sm-3">
            </div>
          </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t b-light">
                <thead>
                    <tr>
                        <th style="width:0%"></th>
                        <th style="width:60%">Title</th>
                        <th style="width:40%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="data in data.data">
                        <td></td>
                        <td>{[{ data.title }]}</td>
                        <td>
                            <a href="" ng-click="update(data.id)"><span class="label bg-warning" >Edit</span></a>
                            <a href="" ng-click="delete(data.id)"> <span class="label bg-danger">Delete</span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <ul class="pagination">
                <li id="DataTables_Table_0_previous" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == 1  }" class="paginate_button previous">
                    <a class="fa fa-chevron-left" href="" ng-click="numpages(data.before)"></a>
                </li>
                <li ng-repeat="page in data.pages" ng-class="{ 'active' : data.index == page.num  }" class="paginate_button">
                    <a ng-click="numpages(page.num)"> {[{ page.num }]}</a>
                </li>
                <li id="DataTables_Table_0_next" tabindex="0" aria-controls="DataTables_Table_0" ng-class="{ 'disabled' : data.index == data.last  }" class="paginate_button next">
                    <a href="" class="fa fa-chevron-right" ng-click="numpages(data.next)"> </a>
                </li>
            </ul>

    </div>
</div>
</footer>
</div>
</div>

