{{ content() }}
<script type="text/ng-template" id="editMemberModal.html">
  <form class="bs-example form-horizontal" name="form" novalidate>
    <div class="modal-header">
      <h3 class="modal-title">Are You sure you want to delete this Photo?</h3>
    </div>
    <div class="modal-body" ng-show="process==true">
      <p ng-show="success==false">Updating user profile record.</p>
      <p ng-show="success==true">Success!</p>
    </div>
    <div class="modal-body" ng-show="process==false">
      ARE YOU SURE YOU WANT TO DELETE?
    </div>
    <div class="modal-footer" ng-hide="process==true">                  
      <button class="btn btn-default" ng-click="cancel()">Cancel</button>
      <button class="btn btn-primary" ng-click="ok(imageid)" ng-disabled="form.user.$invalid">Delete</button>
    </div>
  </form>
</script>
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Image Album</h1>
  <a id="top"></a>
</div>

<div class="wrapper-md ">
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-heading font-bold"> Upload New Photo</div>

      <div class="panel-body">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-head">
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>  
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
              <div class="padd">
                <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="albumtTitle(album)" name="form" >
                  <input id="albumid" type="hidden" ngmodel = "folderid" name="uniqueid" value="{[{ genId.folderid }]}" placeholder="">
                  Album Title: <input type="text" id="albumtitle" name="title" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="album.title" >

                  <br>
                  Short Description: <input type="text" id="description" name="desc" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-pattern" ng-model="album.desc" >
                  <br>

                  Date:
                  <div class="input-group w-md" ng-controller="DatepickerDemoCtrl">
                    <span class="input-group-btn">
                      <input id="date" name="date" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="album.date" is-open="opened" datepicker-options="dateOptions"  ng-required="true" close-text="Close" type="text" disabled>
                      <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </div>
                  <br>
                  <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input  id="digitalAssets" type="file" name="files[]" multiple>
                  </span>
                  <!-- The global progress bar -->
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                  <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                  </div>
                </form>


              <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="dltPhoto(dlt)" name="form" enctype="multipart/form-data"> 
                 <div class="gallery digital-assets-gallery"> 
                  <div class="clearfix"></div>
                </div>
              </form>





              <div class="col-sm-4" ng-repeat="data in data">
                <div class="panel panel-default">
                  <ul class="nav nav-pills pull-right">
                    <li>
                      <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="huhuClick(dlt)" name="form" enctype="multipart/form-data">
                        <input type="hidden" id="" name="imgid" ng-init="dlt.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="dlt.id">
                        <div class="buttonPanel{[{ data.imgid }]}">
                         <button style="padding:2px;border: none;background: none;margin-top:-5px;"><i class="glyphicon glyphicon-remove"></i></button>
                       </div>
                     </form>
                   </li>
                 </ul>
                 <div class="panel-body">
                  <div class="panel-heading">

                  </div>
                  <form class="form-validation ng-pristine ng-invalid ng-invalid-required" ng-submit="imgInfo(info)" name="form" enctype="multipart/form-data">
                    <div class="containerimagefill1" >

                     <?php 
                     $trueimage = $this->config->application->apiURL."/images/{[{ data.foldername }]}/{[{ data.imgpath }]}";
                     $defaultimage = $this->config->application->apiURL."/images/default.png";
                     ?>
                     <img style="width:100%;height:300px;" ng-src="<?php echo $trueimage; ?>" err-src="<?php echo $defaultimage; ?>">
                   </div>
                   <!-- Input for Image Title and Description -->
                   <input type="hidden" id="" name="imgid" ng-init="info.id=data.imgid"class="form-control" placeholder="{[{ data.imgid }]}" ng-model="info.id">
                   <div class="line line-dashed b-b line-lg pull-in"></div>

                   <div class="form-group">

                    <div class="col-sm-10">
                      Title: <input type="text" onclick="this.focus();this.select()" id="" name="imgtitle" value="{[{ data.imgtitle }]}" class="form-control" ng-model="info.title" ng-init="info.title=data.imgtitle">
                    </div>
                  </div>
                  <div class="line line-dashed b-b line-lg pull-in"></div>
                  <div class="form-group">
                    <div class="col-sm-10">
                     Description:<input type="text" id="" name="imgdescription" value="" class="form-control" ng-init="info.description=data.description"  ng-model="info.description">
                   </div>
                 </div>
                 <div class="line line-dashed b-b line-lg pull-in"></div>
                 <div class="buttonPanel{[{ data.imgid }]}">
                  <button class="btn m-b-xs btn-sm btn-primary btn-addon"><i class="fa fa-plus" style='width=100%;'></i>Update Information</button>
                  <span ng-show="saveboxmessage == data.imgid"> <i class="fa fa-check"></i> Saved!</span>
                </div>

              </form>
            </div>
          </div>
        </div>


      </div>
      <div class="widget-foot">
      </div>
    </div> 
  </div>
  <div class="widget-foot">
  </div>
</div>
</div>  
</div>       
</div>
</div>


