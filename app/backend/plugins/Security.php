<?php
namespace Modules\Backend\Plugins;

use Phalcon\Events\Event,
    Phalcon\Mvc\User\Plugin,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Acl;
use Phalcon\Acl\Adapter\Memory as Mem;
/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Security extends Plugin
{

    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }

    public function getAcl($resources)
    {
        //if (!isset($this->persistent->acl)) {

        $acl = new Mem();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        //Register roles
        $roles = array(
            'superadmin' => new \Phalcon\Acl\Role('SuperAdmin'),
            'guests' => new \Phalcon\Acl\Role('Guests')
        );

        foreach ($roles as $role) {
            $acl->addRole($role);
        }
        $resources['admin'] = array('dashboard','index','boom','editprofile');
        $resources['settings'] = array('managesettings','maintenance');
        $resources['menucreator'] = array('createMenu');
        //Private area resources
        $privateResources = $resources;

        foreach ($privateResources as $resource => $actions) {
            $acl->addResource(new \Phalcon\Acl\Resource($resource), $actions);
        }

        //Public area resources
        $publicResources = array(
            'index' => array('index','logout'),
            'forgotpassword'=>array('index','changepassword')
        );
        foreach ($publicResources as $resource => $actions) {
            $acl->addResource(new \Phalcon\Acl\Resource($resource), $actions);
        }

        //Grant access to public areas to both users and guests
        foreach ($roles as $role) {
            foreach ($publicResources as $resource => $actions) {
                $acl->allow($role->getName(), $resource, $actions);
            }
        }

        //Grant acess to private area to role Users
        foreach ($privateResources as $resource => $actions) {
            foreach ($actions as $action){
                $acl->allow('SuperAdmin', $resource, $action);
            }
        }

        //The acl is stored in session, APC would be useful here too
        $this->persistent->acl = $acl;
        //}

        return $this->persistent->acl;
    }

    /**
     * This action is executed before execute any action in the application
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {

        $auth = $this->session->get('SuperAdmin');
        $roles = $this->session->get('roles');
        if (!$auth){

            $role = 'Guests';
        } else {
            $role = 'SuperAdmin';
        }

        

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl($roles['page']);

        $allowed = $acl->isAllowed($role, $controller, $action);

        if ($allowed != Acl::ALLOW) {
            $dispatcher->forward(
                array(
                    'controller' => 'index',
                    'action' => 'index'
                )
            );
            return false;
        }
    }
}

