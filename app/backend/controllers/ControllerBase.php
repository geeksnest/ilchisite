<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;


class ControllerBase extends \Phalcon\Mvc\Controller
{


   public function curl($url){
    $service_url = $this->config->application->apiURL.'/'.$url;
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additional info: ' . var_export($info));
    }
    curl_close($curl);
    return $decoded = json_decode($curl_response);
  } 



protected function initialize()
{
    if(@$this->session->get('userid')){
    
      $this->view->fullname = $this->session->get('auth')['user_fullname'];
      $this->view->imgg = @$this->session->get('img')   ;     
  }

  $this->tag->setTitle("ILCHI Content Management System");
  $this->view->username = $this->session->get('auth');
  $this->view->userid = $this->session->get('userid');
  $this->view->base_url = $this->config->application->baseURL;
  $this->view->api_url = $this->config->application->apiURL;
}
}