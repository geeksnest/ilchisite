<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class NewsletterController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }


    public function createnewsletterAction()
    {
        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

    public function managenewsletterAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

     public function editnewsletterAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

     public function sendnewsletterAction()
    {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }


}

