<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;

class VideoController extends ControllerBase
{

    public function intialize(){

    }
    public function videosAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function video_listsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
       public function editAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
 
}

