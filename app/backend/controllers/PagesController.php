<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class PagesController extends \Phalcon\Mvc\Controller{

    public function indexAction(){
    }
    
    public function managepagesAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function createpageAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editpageAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function statlistAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function homeAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function contactAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function termsAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function medialinksAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    

}

