<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;

class NewsController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }


    public function createnewsAction()
    {
        $form = new CreateuserForm();
        $this->view->form = $form;
        $this->view->userfullname = $this->session->get('auth')['user_fullname'];
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

     public function managenewsAction()
    {
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
 
		
    }

    public function editnewsAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }

     public function tagsAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }
    public function categoryAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }



}

