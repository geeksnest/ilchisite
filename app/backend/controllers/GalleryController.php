<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class GalleryController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }


    public function imageAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }
    public function manage_albumAction()
    {
   
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
    }
    public function edit_albumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


}

