<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;

class MenucreatorController extends ControllerBase
{

    public function intialize(){

    }
    public function createMenuAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        
        $service_url = $this->config->application->apiURL . '/utility/testimonial/0';
        
        $curl = curl_init($service_url);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);
        
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->view->msginfo = $decoded;
    }
   
}

