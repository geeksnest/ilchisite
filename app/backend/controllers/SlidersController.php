<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;
use Modules\Backend\Models\Album as Album;

class SliderController extends ControllerBase
{

    public function intialize(){

    }
    public function create_albumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function manage_albumAction()
    {
    
      
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);


    }
     public function edit_albumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

