<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class CalendarController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }
        
    public function addcalendarAction()
    {           
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW); 	
    }

    public function viewcalendarAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editcalendarAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }
 
}