<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class FeaturedprojectsController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }
        
    public function createfeaturedprojectAction()
    {           
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW); 	
    }

    public function managefeaturedprojectAction()
    {
        // $numberPage = $this->request->getQuery("page", "int");

        // $builder = $this->modelsManager->createBuilder()
        //     ->columns('feat_id, feat_title , feat_content, feat_status')
        //     ->from('Modules\Backend\Models\Featuredprojects');

        // $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
        //   "builder" => $builder,
        //   "limit"=> 10,
        //   "page" => $numberPage
        //   ));
        // // Get the paginated results
        // $page = $paginator->getPaginate();
        // $data = array();
        // $list = array();
        // foreach($page->items as $item){
        //     $list[] = array(
        //         'feat_id' => $item->feat_id,
        //         'feat_title' => $item->feat_title,
        //         'feat_content' => $item->feat_content,
        //         'feat_status' => $item->feat_status
        //     );
        // }
        // $data['pagesdata'] = $list;

        // $p = array();
        // for($x = 1; $x <= $page->total_pages; $x++){
        //     $p[] = array('num' => $x, 'link' => 'page');
        // }
        // $data['featuredprojects'] = $p;
        // $data['index'] = $page->current;
        // $this->view->data = json_encode($data);

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function editfeaturedprojectAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);   
    }
 
}