<?php
namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;


class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }
    public function logoutAction(){
        session_destroy();
        $this->flash->warning('You have successfully logout.');
        $this->view->pick("index/index");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function indexAction($logout=1){
        // $auth = $this->session->get('auth');
        // if ($auth){
        //     $this->response->redirect('ilchiadmin/admin');
        // }

        // $this->view->error = null;

        if ($this->request->isPost()) {




            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');


            $_login = $this->curl('user/login/'.$username.'/'.$password);


            $array =  (array) $_login->role->page;


            if($_login->login){
                $this->_registerSession($_login->user, $_login->role, $array);
                $this->response->redirect('ilchiadmin/admin');
            }
            $this->flash->warning('Wrong Username/Password');
        }
        
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    private function _registerSession($user, $rlsrole, $rlspage )
    {
        $this->session->set('auth', array(
            'user_id' => $user->userid,
            'user_fullname' => $user->firstname .' '.$user->lastname,
            'user_username' => $user->username
        ));

   


        $roles = array('roles' => $rlsrole, 'page' =>  $rlspage);

        $this->session->set('roles', $roles);

        //Set SuperAdmin
        $this->session->set('SuperAdmin', true );
        $this->session->set('ulevel', $user->userLevel);
        $this->session->set('userid', $user->userid);
        $this->session->set('img', $user->img);
        if($user->userLevel==1){
            $this->session->set('MegaAdmin', true );            
        }else{
            $this->session->set('MegaAdmin', false );
        }
    }
}

