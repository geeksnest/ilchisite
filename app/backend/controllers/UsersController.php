<?php

namespace Modules\Backend\Controllers;
use Modules\Backend\Forms\CreateuserForm;
use Phalcon\Mvc\View;

class UsersController extends ControllerBase {

    public function indexAction() {

    }

    public function createAction(){
        // $dlttemp = Profimg::find();
        // if ($dlttemp) {
        //     if($dlttemp->delete()){
              
        //     }
        // }

        $form = new CreateuserForm();
        $this->view->form = $form;

        $this->view->tblroles = $this->curl('user/formrole');
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


    public function listAction() {
        $this->view->ulevel = $this->session->get('ulevel');
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

   
    public function edituserAction() {
  
        $this->view->form = new CreateuserForm();
        $this->view->tblroles = $this->curl('user/formrole');
        $this->view->ulevel = $this->session->get('ulevel');
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }



    public function testAction() {
        echo 'test';
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
