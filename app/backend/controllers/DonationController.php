<?php

namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Modules\Backend\Forms\CreateuserForm;
use Modules\Backend\Models\Users as Users;
use Modules\Backend\Models\Members as Members;
use Modules\Backend\Models\Donationlog as Donationlog;
use Modules\Backend\Models\Donation as Donation;

class DonationController extends ControllerBase
{

    public function indexAction(){

    }
    public function donationlistAction() {
	// Instantiate the Query
	$query = new \Phalcon\Mvc\Model\Query("SELECT m.email, m.firstname, m.lastname, dl.amount, dl.transactionId, dl.datetimestamp FROM Modules\Backend\Models\Donationlog as dl INNER JOIN Modules\Backend\Models\Members as m ON dl.useremail=m.email ORDER BY dl.datetimestamp DESC ", $this->getDI());
	$dl = $query->execute();
	$data = array();
        foreach ($dl as $q) {
            $data[] = array(
                'transactionId' => $q->transactionId,
                'email' => $q->email,
                'firstname' => $q->firstname,
                'lastname' => $q->lastname,
                'amount' => $q->amount,
                'datetimestamp' => $q->datetimestamp
            );
        }
		$this->view->usersdonated = Donationlog::count(array("distinct" => "useremail"));
		$amount = Donationlog::sum(array("column" => "amount"));
		$this->view->amounts = number_format($amount, 2, '.', '');	
		$this->view->totaltrans = count($data);


	   	$don = Donation::findFirst("id=1");
			$this->view->donamounts = $don->amount;
			$this->view->dontotaltrans = $don->usercount;
	
	// Execute the query returning a result if any
        $this->view->data = str_replace("\\/", "/", json_encode($data));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}

