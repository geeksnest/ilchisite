<?php

namespace Modules\Backend\Controllers;
use Phalcon\Mvc\View;

class SliderController extends ControllerBase
{

    public function intialize(){

    }
    public function sliderAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

     public function slider_listsAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    public function editalbumAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
   
 
}

